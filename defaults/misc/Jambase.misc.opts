# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License ONLY.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# VAR = [ RemoveOptWild regexp-options-to-remove : options-list ] ;
# remove options from list with egrep-like regexps
rule RemoveOptWild oname : str {
  local res = ;
  for local _f in $(str) {
    # don't change this to '~=', 'cause oname can contain more that one regexp
    if ! ( [ Match $(oname) : $(_f) ] ) { res += $(_f) ; }
  }
  return $(res) ;
}


# remove-opt-flags $(var)
# remove optimization flags from compiler options
rule remove-opt-flags {
  return [ RemoveOptWild
    '^-O[0-9s]$'
    '^-march='
    '^-mtune='
    '^-mfpmath='
    '^-fwrapv$'
    '^-f[^n][^o]' : $(<) ] ;
}


rule remove-opt-flags-for-all-compilers {
  OPTIM = [ remove-opt-flags $(OPTIM) ] ;
  C++OPTIM = [ remove-opt-flags $(C++OPTIM) ] ;
  OBJCOPTIM = [ remove-opt-flags $(OBJCOPTIM) ] ;
  OPTIM.all = [ remove-opt-flags $(OPTIM.all) ] ;
  CFLAGS.all = [ remove-opt-flags $(CFLAGS.all) ] ;
}


rule gcc-suggest-attrs {
  if $(SUGGEST) {
    CFLAGS.all += -Wsuggest-attribute=const ;
    CFLAGS.all += -Wsuggest-attribute=pure ;
    CFLAGS.all += -Wsuggest-attribute=noreturn ;
  }
}
