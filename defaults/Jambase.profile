# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License ONLY.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# nehalem is more-or-less ok
# westmere is the last without AVX

###############################################################################
# special values
###############################################################################
DIET_BIN ?= diet ;

GCC_NATIVE_ARCH ?= 'native' ;

GDC_INLINE_FLAGS ?= -finline-small-functions -finline-functions ;

# k8: moved to ~/.jam.rc
#  -mstackrealign
#  -fms-extensions

OPTIM_COMMON_FLAGS ?=
  -fwrapv
  -fno-aggressive-loop-optimizations
  -fno-delete-null-pointer-checks
  -fno-strict-aliasing
  -fno-strict-overflow
  $(OPTIM_USER_COMMON_FLAGS)
;

OPTIM_ARCH_TUNE ?= -march=$(GCC_NATIVE_ARCH) -mtune=$(GCC_NATIVE_ARCH) ;

OPTIM_SPEED ?= -O3 $(OPTIM_ARCH_TUNE) ;
LINKFLAGS_SPEED ?= -s ;

OPTIM_SIZE ?= -Os $(OPTIM_ARCH_TUNE)  ;
LINKFLAGS_SIZE ?= -s ;

OPTIM_STANDARD ?= -O2 $(OPTIM_ARCH_TUNE)  ;
LINKFLAGS_STANDARD ?= -s ;

OPTIM_NOOPT ?= -O0 ;
LINKFLAGS_NOOPT ?= -s ;

OPTIM_DEBUG ?= -O0 -g ;
LINKFLAGS_DEBUG ?= -g ;

OPTIM_NOALIAS ?= -fno-strict-aliasing ;


# current profile will be saved to OPT_PROFILE
# empty OPT_PROFILE means 'default'


## K8JAM-KNOWN-PROFILES =
##   none default  # don't change
##   empty         # remove optimisation flags
##   speed         # optimise for speed
##   size          # optimise for size
##   debug         # don't optimize, add debug info
##   standard      # -O2
## ;


rule --k8jam-set-standards-- {
  if ! $(CC.standard) { CC.standard = -std=gnu11 ; }
  if ! $(C++.standard) { C++.standard = -std=gnu++14 ; }
}


rule --k8jam-profile-default-- {
  OPT_PROFILE = ;
}


rule --k8jam-profile-empty-- {
  LINKFLAGS.all += -s ;
  --k8jam-set-standards-- ;
}


rule --k8jam-profile-noopt-- {
  CFLAGS.all += $(OPTIM_NOOPT) $(OPTIM_COMMON_FLAGS) $(OPTIM_USER_NOOPT) ;
  GDCFLAGS.all += $(OPTIM_NOOPT) ;
  LINKFLAGS.all += $(LINKFLAGS_NOOPT) ;
  --k8jam-set-standards-- ;
}


rule --k8jam-profile-speed-- {
  CFLAGS.all += $(OPTIM_SPEED) $(OPTIM_COMMON_FLAGS) $(OPTIM_USER_SPEED) ;
  GDCFLAGS.all += $(OPTIM_SPEED) ;
  LINKFLAGS.all += $(LINKFLAGS_SPEED) ;
  --k8jam-set-standards-- ;
}

rule --k8jam-profile-size-- {
  CFLAGS.all += $(OPTIM_SIZE) $(OPTIM_COMMON_FLAGS) $(OPTIM_USER_SIZE) ;
  GDCFLAGS.all += $(OPTIM_SIZE) ;
  LINKFLAGS.all += $(LINKFLAGS_SIZE) ;
  --k8jam-set-standards-- ;
}

rule --k8jam-profile-debug-- {
  CFLAGS.all += $(OPTIM_DEBUG) $(OPTIM_COMMON_FLAGS) $(OPTIM_USER_DEBUG) ;
  GDCFLAGS.all += $(OPTIM_DEBUG) ;
  LINKFLAGS.all += $(LINKFLAGS_DEBUG) ;
  --k8jam-set-standards-- ;
}

rule --k8jam-profile-standard-- {
  CFLAGS.all += $(OPTIM_STANDARD) $(OPTIM_COMMON_FLAGS) $(OPTIM_USER_STANDARD) ;
  if $(OPTIM_NOALIAS) { CFLAGS.all += $(OPTIM_NOALIAS); }
  GDCFLAGS.all += $(OPTIM_STANDARD) ;
  if $(OPTIM_NOALIAS) { GDCFLAGS.all += $(OPTIM_NOALIAS); }
  LINKFLAGS.all += $(LINKFLAGS_STANDARD) ;
  --k8jam-set-standards-- ;
}


rule --k8jam-profile-standard-with-aliasing-- {
  CFLAGS.all += $(OPTIM_STANDARD) $(OPTIM_COMMON_FLAGS) $(OPTIM_USER_STANDARD) ;
  GDCFLAGS.all += $(OPTIM_STANDARD) ;
  LINKFLAGS.all += $(LINKFLAGS_STANDARD) ;
  --k8jam-set-standards-- ;
}


# profile 'name' ;
# set compile flags for profile; works only for gcc/g++
# available profiles:
#  none, default: don't change
#  empty: remove optimisation flags
#  speed: optimise for speed and pIII
#  size: optimise for size
#  debug: don't optimize, add debug info
#  standard: -O2
rule profile {
  local pnn = $(1) ;
  if ! $(pnn) { pnn = 'standard' ; }
  if [ HasRule --k8jam-profile-$(pnn)-- ] {
    # --fuck-idiotic-caret-- ;
    remove-opt-flags-for-all-compilers ;
    LINKFLAGS -= '-g' '-s' ;
    C++LINKFLAGS -= '-g' '-s' ;
    OBJCLINKFLAGS -= '-g' '-s' ;
    LINKFLAGS.all -= '-g' '-s' ;
    if ! $(JAM_OPTION_MAKE_UPDATES_SILENT) {
      Echo "MSG: '$(pnn)' profile" ;
    }
    OPT_PROFILE = $(pnn) ;
    --k8jam-profile-$(OPT_PROFILE)-- ;
  } else {
    Exit 'FATAL: invalid profile:' "$(pnn)" ;
  }
}


# process various profile targets:
#   speed
#   size
#   debug
rule profile-targets {
  local prf = ;
  local rel = ;
  local ut = ;
  local ntarg = ;
  for local targ in $(JAM_TARGETS) {
    if $(targ) = 'speed' || $(targ) = 'size' || $(targ) = 'debug' {
      if ! $(PROFILE) {
        if $(prf) { Exit "FATAL: duplicate profile target; " $(prf) "and" $(targ) ; }
        prf = $(targ) ;
      }
    } else if $(targ) = 'release' {
      if $(rel) { Exit "FATAL: duplicate profile target; " $(prf) "and" $(targ) ; }
      rel = tan ;
    } else if $(targ) = 'unittest' {
      if $(ut) { Exit "FATAL: duplicate profile target; " $(prf) "and" $(targ) ; }
      ut = tan ;
    } else {
      ntarg += $(targ) ;
    }
  }
  if $(prf) { PROFILE = $(prf) ; }
  if $(rel) { RELEASE = tan ; }
  if $(ut) { UNITTEST = tan ; }
  if ! $(ntarg) { ntarg = 'all' ; }
  JAM_TARGETS = $(ntarg) ;
}


# selects profile
# if $(flags) contains 'no-targets' -- disallow 'profile targets'
rule set-profile flags {
  local profname ndb dbg ;

  if ! 'no-targets' in $(flags) { profile-targets ; }

  dbg = ;
  profname = $(PROFILE) ;
  if ! $(profname) {
    if $(DEBUG) { profname = 'debug' ; } else { profname = 'standard' ; }
  } else {
    if $(profname) != 'debug' && $(DEBUG) { dbg = 'tan' ; }
  }

  if $(profname) = 'debug' && ! $(DEBUG) { DEBUG = tan ; }

  if $(USE_CLANG) {
    CC = clang ;
    LINK = clang ;
    C++ = clang++ ;
    C++LINK = clang++ ;
  }

  if $(DIET) && $(DIET_BIN) {
    CC = $(DIET_BIN) $(CC) ;
    LINK = $(DIET_BIN) $(LINK) ;
    C++ = $(DIET_BIN) $(C++) ;
    C++LINK = $(DIET_BIN) $(C++LINK) ;
  }

  if $(DBG) || $(DEBUG) { ndb = ; } else { ndb = NDEBUG ; }

  profile $(profname) ;

  if $(VALGRIND) || $(dbg) || $(DEBUG) {
    CFLAGS.all -= '-g' '-s' ;
    GDCFLAGS.all -= '-g' '-s' ;
    LINKFLAGS.all -= '-g' '-s' ;
    if $(VALGRIND) {
      CFLAGS.all += -g ;
      GDCFLAGS.all += -g ;
      LINKFLAGS.all += -g ;
    } else {
      CFLAGS.all += -g '-mfpmath=387' ;
      GDCFLAGS.all += -g '-mfpmath=387' ;
      LINKFLAGS.all += -g '-mfpmath=387' ;
      NO_NATIVE = tan ;
    }
  }

  if ! $(NO_WARNINGS) { CFLAGS.all += -Wall ; GDCFLAGS.all += -Wall ; }
  if $(NO_NATIVE) { CFLAGS.all -= '-march=native' '-mtune=native' ; GDCFLAGS.all -= '-march=native' '-mtune=native' ; }
  if $(NO_FWRAP) { CFLAGS.all -= '-fwrapv' ; GDCFLAGS.all -= '-fwrapv' ; }

  if $(NO_STACK_REALIGN) { CFLAGS.all -= '-mstackrealign' ; }

  if $(ndb) { DEFINES += $(ndb) ; }

  if $(MAEMO47) { CFLAGS.all += -fgnu89-inline ; }
  if $(MAEMO47) && $(USE_THUMB) { CFLAGS.all += -mthumb ; }
  if $(MAEMO47) && $(THUMB_WRELOC) { CFLAGS.all += -mword-relocations ; }
  #if $(MAEMO47) && $(Fpic) { CFLAGS.all += -fpic ; }
  #if $(MAEMO47) && $(FPIC) { CFLAGS.all += -fPIC ; }
  if $(Fpic) || $(fpic) { CFLAGS.all += -fpic ; }
  if $(FPIC) || $(fPIC) { CFLAGS.all += -fPIC ; }

  if ( ! [ IsGCC ] ) && ( ! [ IsG++ ] ) {
    CFLAGS.all -= '-Wall'
                  '-march=native'
                  '-mtune=native'
                  '-fno-aggressive-loop-optimizations'
                  '-fno-delete-null-pointer-checks'
                  '-fno-strict-aliasing'
                  '-fno-strict-overflow'
                  '-mstackrealign'
                  '-fno-diagnostics-show-caret' ;
  }

  # d
  if $(UNITTEST) {
    GDCFLAGS.all += -funittest ;
  }
  if $(RELEASE) {
    GDCFLAGS.all += -frelease ;
    GDCFLAGS.all += -fno-assert ;
    GDCFLAGS.all += -fno-debug ;
    GDCFLAGS.all += -fno-in ;
    GDCFLAGS.all += -fno-out ;
    GDCFLAGS.all += -fno-invariants ;
    if $(D_NO_BOUNDS) { GDCFLAGS.all += -fno-bounds-check ; }
  } else {
    if ! $(D_MAXSPEED) {
      #if ! $(NODEBUG) && ! $(NO_DEBUG) && ! $(D_NO_DEBUG) { GDCFLAGS.all += -fdebug ; }
      if ! $(D_NO_ASSERT) { GDCFLAGS.all += -fassert ; } else { GDCFLAGS.all += -fno-assert ; }
      if ! $(D_NO_IN) { GDCFLAGS.all += -fin ; } else { GDCFLAGS.all += -fno-in ; }
      if ! $(D_NO_OUT) { GDCFLAGS.all += -fout ; } else { GDCFLAGS.all += -fno-out ; }
      if ! $(D_NO_INVARIANTS) { GDCFLAGS.all += -finvariants ; } else { GDCFLAGS.all += -fno-invariants ; }
      if ! $(D_NO_BOUNDS) { GDCFLAGS.all += -fbounds-check ; } else { GDCFLAGS.all += -fno-bounds-check ; }
    } else {
      GDCFLAGS.all += -fno-assert ;
      GDCFLAGS.all += -fno-debug ;
      GDCFLAGS.all += -fno-in ;
      GDCFLAGS.all += -fno-out ;
      GDCFLAGS.all += -fno-invariants ;
      GDCFLAGS.all += -fno-bounds-check ;
    }
  }

  if ! $(USE_DMD) && $(PHOBOS_STATIC) {
    #GDCFLAGS.all += -static-libphobos ;
    GDCLINKFLAGS.all += -static-libphobos ;
  } else {
    GDCFLAGS.all += -fPIC ;
    #LINKFLAGS.all += -fPIC ;
  }

  gcc-suggest-attrs ;
}
