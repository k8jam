# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License ONLY.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# /Link  image : objects ;
#
# Links _image_ from _objects_ and sets permissions on _image_ to
# $(EXEMODE). _image_ must be an actual filename; suffix is not
# supplied.
#
# Called by @Main, shouldn't be called by most people
#
rule Link {
  MODE on $(<) = $(EXEMODE) ;
  Chmod $(<) ;
}


# /LinkFlagsOn  mains : flags ;
#
# this rule is used to add compiler flags to the compilation of
# specific C sources files.
#
rule LinkFlagsOn {
  LINKFLAGS on [ FGristFiles $(<) ] += $(>) ;
}


actions Link bind NEEDLIBS {
  $(LINK) $(LINKFLAGS.all) $(LINKFLAGS) -o $(<) $(UNDEFS) $(>) $(NEEDLIBS) $(LINKLIBS.all) $(LINKLIBS)
}
