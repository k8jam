# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License ONLY.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# /Main image : sources [ : targets ] ;
#
# Compiles _sources_ and links them into _image_. Calls @Objects and
# @MainFromObjects.
#
# _image_ may be supplied without suffix.
#
rule C-Main {
  local _tgts = [ --MainNormalizeTargets-- $(1) : $(3) ] ;
  local _xl = [ --MainFromObjects-- $(<) : $(>:S=$(SUFOBJ)) : $(_tgts) : Link : $(>) ] ;
  Objects $(_xl) : $(3) : $(1) ;
}
