# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License ONLY.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# /Library  library : sources ;
#
#  Compiles _sources_ and archives them into _library_. The intermediate
#  objects are deleted. Calles @Object and @LibraryFromObjects
#
#  If @Library is invoked with no suffix on _library_, the $(SUFLIB)
#  suffix is used
#
rule Library {
  local _xl = [ LibraryFromObjects $(<) : $(>:S=$(SUFOBJ)) : $(>) ] ;
  Objects $(_xl) : : $(1) ;
}


# /LibraryFromObjects library : objects : origsrc ;
#
# Archives _objects_ into _library_. The _objects_ are then deleted
#
# If _library_ has no suffix, the $(SUFLIB) suffix is used
#
# Called by @Library rule. Most people should never call this rule
# directly.
#
rule LibraryFromObjects {
  local objlist tgt have_attrs ;

  # check if we have attributes
  have_attrs = [ --main-preprocess-attrs-- $(>) ] ;
  objlist = $(have_attrs[2-]) ;
  have_attrs = $(have_attrs[1]) ;

  tgt = $(<:S=$(SUFLIB)) ;

  # library depends on its member objects
  if $(KEEPOBJS) {
    Depends obj : $(objlist) ;
  } else {
    Depends lib : $(tgt) ;
  }

  # Set LOCATE for the library and its contents.  The bound
  # value shows up as $(NEEDLIBS) on the Link actions.
  # For compatibility, we only do this if the library doesn't
  # already have a path.
  if ! $(tgt:D) {
    MakeLocate $(tgt) "$(tgt)($(objlist:BS))" : $(LOCATE_LIB) ;
  }

  if $(NOARSCAN) {
    # If we can't scan the library to timestamp its contents,
    # we have to just make the library depend directly on the
    # on-disk object files.
    Depends $(tgt) : $(objlist) ;
  } else {
    # If we can scan the library, we make the library depend
    # on its members and each member depend on the on-disk
    # object file.
    Depends $(tgt) : "$(tgt)($(objlist:BS))" ;
    for local objname in $(objlist) {
      Depends "$(tgt)($(objname:BS))" : $(objname) ;
    }
  }

  Clean clean : $(tgt) ;

  #if $(CRELIB) { CreLib $(tgt) : $(objlist[1]) ; }

  Archive $(tgt) : $(objlist) ;

  if $(RANLIB) { Ranlib $(tgt) ; }

  # If we can't scan the library, we have to leave the .o's around.
  if ! ( $(NOARSCAN) || $(NOARUPDATE) ) { RmTemps $(tgt) : $(objlist) ; }

  if $(have_attrs) { return [ --main-postprocess-attrs-- $(3) ] ; }
  return $(3) ;
}
