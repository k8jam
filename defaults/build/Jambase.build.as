# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License ONLY.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# /As object : source ;
#
# Assemble the file _source_, called by the @Object rule.
#
# Do not call this rule directly, since _object_ and _source_ may have
# have platform-specific file extensions
#
rule As {
  Depends $(<) : $(>) ;
  ASFLAGS on $(<) += $(ASFLAGS) $(SUBDIRASFLAGS) ;
  ASHDRS on $(<) = [ FIncludes $(SEARCH_SOURCE) $(SUBDIRHDRS) $(HDRS) ] ;
}


actions As {
  $(AS) $(ASFLAGS) $(ASHDRS) -o $(<) $(>)
}
