# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License ONLY.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# /Lex source.c : source.l ;
#
# Process the lex source file _source.l_ and rename the lex.yy.c
# to _source.c_ . Called by the @Object rule
#
rule Lex {
  ##LexMv $(<) : $(>) ;
  Depends $(<) : $(>) ;
  MakeLocate $(<) : $(LOCATE_SOURCE) ;
  Clean clean : $(<) ;
}

actions Lex {
  $(LEX) -t $(>) >"$(<)"
}

##actions LexMv {
##  $(MV) lex.yy.c $(<)
##}
