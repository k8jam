# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License ONLY.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

rule Yacc {
  local _h = $(<:S=.h:BS) ;

  MakeLocate $(<) $(_h) : $(LOCATE_SOURCE) ;

  if $(YACC) {
    Depends $(<) $(_h) : $(>) ;
    Yacc1 $(<) : $(>) ;
    Clean clean : $(<) $(_h) ;
  }

  # make sure someone includes $(_h) else it will be
  # a deadly independent target
  Includes $(<) : $(_h) ;
}


actions Yacc1 {
  $(YACC) $(YACCFLAGS) $(>) -o $(<)
}
