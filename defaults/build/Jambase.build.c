# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License ONLY.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# /Cc object : source ;
#
# Compile the file source into object, using the C compiler $(CC), its
# flags $(CCFLAGS) and $(OPTIM), and the header file directories $(HDRS).
# Called by the @Object rule
#
# Do not call this rule directly, since _object_ and _source_ may have
# have platform-specific file extensions
#
rule Cc {
  Depends $(<) : $(>) ;
  # Just to clarify here: this sets the per-target CCFLAGS to
  # be the current value of (global) CCFLAGS and SUBDIRCCFLAGS.
  # CCHDRS and CCDEFS must be reformatted each time for some
  # compiles (VMS, NT) that malign multiple -D or -I flags.
  CCFLAGS on $(<) += $(CCFLAGS) $(SUBDIRCCFLAGS) ;
  CCHDRS on $(<) = [ on $(<) FIncludes $(HDRS) ] ;
  CCDEFS on $(<) = [ on $(<) FDefines $(DEFINES) ] ;
  CCHDRS on $(<) += [ FIncludes $(SUBDIRHDRS) ] ;
  CCDEFS on $(<) += [ FDefines $(SUBDIRDEFINES) ] ;
  CC.standard on $(<) += $(CC.standard) ;
}


actions Cc {
  $(CC) -c -o $(<) $(CC.standard) $(CFLAGS.all) $(CCFLAGS) $(OPTIM.all) $(OPTIM) $(CCDEFS) $(CCHDRS) $(>)
}
