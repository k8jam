# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License ONLY.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# /C++ object : source ;
#
# Compile the C++ source file _source_. Similar to @CC, called by @Object
#
# Do not call this rule directly, since _object_ and _source_ may have
# have platform-specific file extensions
#
rule C++ {
  local ktmp ;

  Depends $(<) : $(>) ;

  # Just to clarify here: this sets the per-target CCFLAGS to
  # be the current value of (global) CCFLAGS and SUBDIRCCFLAGS.
  # CCHDRS and CCDEFS must be reformatted each time for some
  # compiles (VMS, NT) that malign multiple -D or -I flags.
  ktmp = $(C++FLAGS) ;
  if ! $(ktmp) { ktmp = $(CCFLAGS) ; }
  C++FLAGS on $(<) += $(ktmp) $(SUBDIRC++FLAGS) ;

  ktmp = $(C++OPTIM) ;
  if ! $(ktmp) { ktmp = $(OPTIM) ; }
  C++OPTIM on $(<) += $(ktmp) ;

  CCHDRS on $(<) = [ on $(<) FIncludes $(HDRS) ] ;
  CCDEFS on $(<) = [ on $(<) FDefines $(DEFINES) ] ;
  CCHDRS on $(<) += [ FIncludes $(SUBDIRHDRS) ] ;
  CCDEFS on $(<) += [ FDefines $(SUBDIRDEFINES) ] ;
  C++.standard on $(<) += $(C++.standard) ;
}


actions C++ {
  $(C++) -c -o $(<) $(C++.standard) $(CFLAGS.all) $(C++FLAGS.all) $(C++FLAGS) $(OPTIM.all) $(C++OPTIM) $(CCDEFS) $(CCHDRS) $(>)
}
