# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License ONLY.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# $(1): check string: 'glib-2.0 >= 1.3.4'
# return empty string if condition is not met
rule pkg-config-exists {
  local cf res ;
  cf = [ Command "$(PKG-CONFIG) --exists '$(1)' 2>/dev/null" : exit-code code-first ] ;
  if $(cf[1]) = '0' {
    res = 'tan' ;
  } else {
    res = ;
  }
  return $(res) ;
}


# call $(1)
# add output to var $(2)
# return resulting (non-empty) string if library is present
# if $(2) = "" -- don't add flags, just return
# [ lib-config-ex '$(PKG-CONFIG) sdl --cflags' : 'CFLAGS' ]
rule lib-config-ex {
  local cf lf res ;
  cf = [ Command "$(1) 2>/dev/null" : parse-output exit-code code-first ] ;
  if $(cf[1]) = '0' && $(cf[2]) {
    res = $(cf[2-]) ;
    if $(2) {
      $(2) += $(cf[2-]) ;
    }
  } else {
    res = ;
  }
  return $(res) ;
}


# call $(1) --cflags and $(1) --libs (many libs provides such configurators)
# add necessary flags to compiler and linker vars
# return 'tan' (non-empty string) if library is present
# if $(2) != "" -- don't add flags, just check
# [ lib-config '$(PKG-CONFIG) sdl' ]
rule lib-config {
  local cf lf hasit ;

  cf = [ Command "$(1) --cflags 2>/dev/null" : parse-output exit-code code-first ] ;
  #Echo 'cf:' $(cf) ;
  if $(cf[1]) = '0' && $(cf[2]) {
    hasit = 'tan' ;
    #Echo 'flags:' $(cf[2-]) ;
    if ! $(2) {
      CCFLAGS += $(cf[2-]) ;
      C++FLAGS += $(cf[2-]) ;
      OBJCFLAGS += $(cf[2-]) ;
    }
  }

  lf = [ Command "$(1) --libs 2>/dev/null" : parse-output exit-code code-first ] ;
  #Echo 'lf:' $(lf) ;
  if $(lf[1]) = '0' && $(lf[2]) {
    hasit = 'tan' ;
    #Echo 'flags:' $(lf[2-]) ;
    if ! $(2) {
      LINKLIBS += $(lf[2-]) ;
      C++LINKLIBS += $(lf[2-]) ;
      OBJCLINKLIBS += $(lf[2-]) ;
    }
  }

  return $(hasit) ;
}

rule pkg-config {
  local res ;
  res = [ lib-config "$(PKG-CONFIG) $(1)" ] ;
  return $(res) ;
}


rule pkg-config-has {
  local res ;
  res = [ lib-config-ex "$(PKG-CONFIG) $(1) --cflags --libs" ] ;
  return $(res) ;
}
