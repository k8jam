/**
 * coded by Ketmar // Invisible Vector &lt;$(B ketmar@ketmar.no-ip.org)&gt;$(BR)
 * $(I Understanding is not required. Only obedience.)$(BR)
 *$(BR)
 *                   INVISIBLE VECTOR PUBLIC LICENSE$(BR)
 *                       Version 0, August 2014$(BR)
 *$(BR)
 * Copyright (C) 2014 Ketmar Dark <ketmar@ketmar.no-ip.org>$(BR)
 *$(BR)
 * Everyone is permitted to copy and distribute verbatim or modified$(BR)
 * copies of this license document, and changing it is allowed as long$(BR)
 * as the name is changed.$(BR)
 *$(BR)
 *                   INVISIBLE VECTOR PUBLIC LICENSE$(BR)
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION$(BR)
 *$(BR)
 * 0. You may not use this software in either source or binary form, any$(BR)
 *    software derived from this software, any library which uses either$(BR)
 *    this software or code derived from this software in any other$(BR)
 *    software which uses Windows API, either directly or indirectly$(BR)
 *    via any chain of libraries.$(BR)
 *$(BR)
 * 1. You may not use this software in either source or binary form, any$(BR)
 *    software derived from this software, any library which uses either$(BR)
 *    this software or code derived from this software in any other$(BR)
 *    software which uses MacOS X API, either directly or indirectly via$(BR)
 *    any chain of libraries.$(BR)
 *$(BR)
 * 2. Redistributions of this software in either source or binary form must$(BR)
 *    retain this list of conditions and the following disclaimer.$(BR)
 *$(BR)
 * 3. Otherwise, you are allowed to use this software in any way that will$(BR)
 *    not violate paragraphs 0, 1 and 2 of this license.$(BR)
 *$(BR)
 *$(BR)
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR$(BR)
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,$(BR)
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE$(BR)
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER$(BR)
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,$(BR)
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN$(BR)
 * THE SOFTWARE.$(BR)
 *
 * Authors: Ketmar // Invisible Vector &lt;$(B ketmar@ketmar.no-ip.org)&gt;
 * License: IVPLv0
 */
module iv.gccattrs;


version(GNU) {
  static import gcc.attribute;
  enum gcc_inline = gcc.attribute.attribute("forceinline");
  enum gcc_noinline = gcc.attribute.attribute("noinline");
  enum gcc_flatten = gcc.attribute.attribute("flatten");
} else {
  // hackery for non-gcc compilers
  enum gcc_inline;
  enum gcc_noinline;
  enum gcc_flatten;
}
