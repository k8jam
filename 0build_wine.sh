#!/bin/sh

MGPATH="wine c:\\\\mingw\\\\bin\\\\"
MGCC="mingw32-gcc.exe -static-libgcc"
CFLAGS="-O2 -DNT -Wall -DMKJAMBASE_COMPACT -march=i486 -mtune=i486"
CC="${MGPATH}${MGCC}"
LINK="${CC}"
LINKFLAGS="-s"
AR="${MGPATH}ar.exe -sru"
#RANLIB="${MGPATH}ranlib.exe"
LINKLIBS="-lkernel32 -ladvapi32"
#LINKFLAGS="-s -Wl,-subsystem,windows"
DESTPATH="bin.win32"
BUILDPATH="_wbuild"
WINE="tan"
XOS="nt"
EXESFX=".exe"
CROSS="ona"

. ./0build_common.sh
