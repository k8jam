#!/bin/sh


if [ "z$CC" = "z" ]; then
  CC="gcc -pipe"
fi
if [ "z$LINK" = "z" ]; then
  LINK="gcc -pipe"
fi
if [ "z$LINKFLAGS" = "z" ]; then
  LINKFLAGS="-s"
fi
if [ "z$AR" = "z" ]; then
  AR="ar sru"
fi
#RANLIB="ranlib"
if [ "z$CFLAGS" = "z" ]; then
  CFLAGS="-std=gnu99 -O2 -Wall -DMKJAMBASE_COMPACT -march=native -mtune=native -fno-strict-aliasing -fwrapv"
fi

checkoldcompiler="ona"
if [ `uname -m` = "armv7l" ]; then
  # n900 native
  checkoldcompiler="tan"
elif [ `uname -m` = "arm" ]; then
  # armel scratchbox
  checkoldcompiler="tan"
elif [ `uname` = "AIX" ]; then
  CC="xlc"
  LINK="xlc"
  CFLAGS="-O2 -qfullpath -qlanglvl=extc99 -DMKJAMBASE_COMPACT"
fi

# check for n900 and old compiler
if [ "$checkoldcompiler" = "tan" ]; then
  ver=`gcc -v 2>&1 | grep version`
  if [ "z$ver" = "zgcc version 4.2.1" ]; then
    echo "MSG: using n900 configuration..."
    CFLAGS="-O0 -Wall -DMKJAMBASE_COMPACT -fno-strict-aliasing -fwrapv"
  else
    which awk >/dev/null 2>&1
    if test "$?" = "0"; then
      ver=`gcc -v 2>&1 | grep version | awk '{print $3}'`
      if [ "z$ver" = "z4.7.2" ]; then
        echo "MSG: using n900 thumb2 configuration..."
        CFLAGS="-O2 -Wall -DMKJAMBASE_COMPACT -fgnu89-inline -fno-strict-aliasing -fwrapv"
      fi
    else
      echo "MSG: no awk, it's so unsexy..."
    fi
  fi
fi

DESTPATH="bin.unix"
BUILDPATH="_build"
XOS="unix"
WINE=""
EXESFX=""
CROSS="ona"

. ./0build_common.sh
