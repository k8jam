#!/bin/sh


# $1: file w/o extension
# $2: src path (optional)
docc() {
  ptmp="$2"
  echo "Cc $1.c"
  if [ "z$ptmp" = "z" ]; then
    ptmp="src"
  fi
  ${CC} -DNDEBUG -c -o ${BUILDPATH}/obj/$1.o ${CFLAGS} -Isrc ${ADDITIONAL_INCLUDES} $ptmp/$1.c
}


# $1: file w/o extension
# $2: dst path (optional)
# $3: liblist
dolink() {
  fname="$1"
  ptmp="$2"
  shift
  echo "Link ${fname}"
  if [ "z$ptmp" = "z" ]; then
    ptmp="${BUILDPATH}"
  else
    shift
  fi
  ${LINK} ${LINKFLAGS} -o $ptmp/${fname}${EXESFX} ${BUILDPATH}/obj/${fname}.o $* ${LINKLIBS}
  if [ "z$WINE" != "z" ]; then
    chmod 711 $ptmp/${fname}${EXESFX}
  fi
}


donativecc() {
  OCC="$CC"
#  CC=gcc
  docc $*
  CC="$OCC"
}


donativelink() {
  OLINK="$LINK"
  OLINKFLAGS="$LINKFLAGS"
  OLINKLIBS="$LINKLIBS"
#  LINK=gcc
  LINKFLAGS=-s
  LINKLIBS=""
  dolink $*
  LINK="$OLINK"
  LINKFLAGS="$OLINKFLAGS"
  LINKLIBS="$OLINKLIBS"
}


srclist="\
bjprng
builtins
command
compile
dstrings
execcmd
filent
fileunix
gdcdeps
genman
pathsys
expand
hash
headers
hcache
jbunpack
lists
make
make1
matchglob
newstr
option
parse
progress
re9
rexp
rules
scan
search
timestamp
variable
hdrmacro
libhaunp"

echo "building K8Jam..."

mkdir -p ${BUILDPATH} 2>/dev/null
rm -rf ${BUILDPATH}/* 2>/dev/null
mkdir -p ${BUILDPATH}/obj 2>/dev/null

mkdir -p ${DESTPATH} 2>/dev/null
rm -rf ${DESTPATH}/* 2>/dev/null

donativecc "unigen"
donativelink "unigen"
echo "unigen"
echo ${BUILDPATH}/unigen${EXESFX} ${BUILDPATH}/re9_unicode_mapping.c unidata/UnicodeData.txt
${BUILDPATH}/unigen${EXESFX} ${BUILDPATH}/re9_unicode_mapping.c unidata/UnicodeData.txt

donativecc "lemon"
donativelink "lemon"
echo "Lemon jamgram"
echo tools/yyacc src/jamgram.lemon src/jamgramtab.h src/jamgram.yy
tools/yyacc src/jamgram.lemon src/jamgramtab.h src/jamgram.yy
echo ${BUILDPATH}/lemon${EXESFX} -q src/jamgram.lemon
${BUILDPATH}/lemon${EXESFX} -q src/jamgram.lemon
#rm ${BUILDPATH}/lemon${EXESFX}
docc "jamgram"

libobj="${BUILDPATH}/obj/jamgram.o"
for srcf in ${srclist}; do
  docc "${srcf}"
  libobj="${libobj} ${BUILDPATH}/obj/${srcf}.o"
done

echo "Ar libjam.a"
${AR} ${BUILDPATH}/obj/libjam.a ${libobj}
if [ "z${RANLIB}" != "z" ]; then
  echo "Ranlib libjam.a"
  ${RANLIB} ${BUILDPATH}/obj/libjam.a
fi
rm -f ${libobj}

docc "jam"

donativecc "mkjambase"
donativelink "mkjambase"
echo "preparing Jambase"
echo ${BUILDPATH}/mkjambase${EXESFX} src/jambase.c defaults/Jambase
${BUILDPATH}/mkjambase${EXESFX} src/jambase.c defaults/Jambase
#rm ${BUILDPATH}/mkjambase${EXESFX}
docc "jambase"

dolink "jam"  ${DESTPATH}  ${BUILDPATH}/obj/jambase.o ${BUILDPATH}/obj/libjam.a
mv ${DESTPATH}/jam ${DESTPATH}/k8jam

#rm ${BUILDPATH}/obj/* 2>/dev/null

echo "K8Jam built"
