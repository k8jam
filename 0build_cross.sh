#!/bin/sh

MGCC="i686-pc-mingw32-gcc -pipe -static-libgcc"
CFLAGS="-O2 -DNT -Wall -DMKJAMBASE_COMPACT -march=i486 -mtune=i486"
CC="${MGCC}"
LINK="${CC}"
#LINKFLAGS="-s"
LINKFLAGS="-s -Wl,-subsystem,console"
AR="i686-pc-mingw32-ar -sru"
#RANLIB="i686-pc-mingw32-ranlib"
LINKLIBS="-lkernel32 -ladvapi32"
DESTPATH="bin.win32"
BUILDPATH="_wbuild"
WINE="tan"
XOS="nt"
EXESFX=".exe"
CROSS="tan"

${CC} -v >/dev/null 2>&1
if test "$?" = "127"; then
  echo "FATAL: MinGW cross-compiler not found!"
  exit 1
fi

. ./0build_common.sh
