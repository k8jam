/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "bjprng.h"


static inline uint32_t hashint (uint32_t a) {
  a -= (a<<6);
  a ^= (a>>17);
  a -= (a<<9);
  a ^= (a<<4);
  a -= (a<<3);
  a ^= (a<<10);
  a ^= (a>>15);
  return a;
}


void bjprngRandomize (BJRandCtx *rng) {
#ifndef OS_NT
  uint32_t res0, res1;
  int fd = open("/dev/urandom", O_RDONLY);
  if (fd >= 0) {
    read(fd, &res0, sizeof(res0));
    read(fd, &res1, sizeof(res1));
    close(fd);
  } else {
    res0 = hashint((uint32_t)getpid()^(uint32_t)time(NULL));
    res1 = hashint((uint32_t)getpid())^hashint((uint32_t)time(NULL));
  }
#else
  uint32_t res0 = hashint((uint32_t)GetTickCount()^(uint32_t)GetCurrentProcessId());
  uint32_t res1 = hashint((uint32_t)GetTickCount())^hashint((uint32_t)GetCurrentProcessId());
#endif
  rng->state = (((uint64_t)res0)<<32)|res1;
  rng->inc = 1;
  for (res0 = 16; res0 > 0; --res0) bjprngRand(rng);
}


// *Really* minimal PCG32 code / (c) 2014 M.E. O'Neill / pcg-random.org
// Licensed under Apache License 2.0 (NO WARRANTY, etc. see website)
uint32_t bjprngRand (BJRandCtx *rng) {
  uint64_t oldstate = rng->state;
  // advance internal state
  rng->state = oldstate*6364136223846793005ULL+(rng->inc|1);
  // calculate output function (XSH RR), uses old state for max ILP
  uint32_t xorshifted = ((oldstate>>18u)^oldstate)>>27u;
  uint32_t rot = oldstate>>59u;
  return (xorshifted>>rot)|(xorshifted<<((-rot)&31));
}
