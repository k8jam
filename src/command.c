/*
 * Copyright 1993, 1995 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "jam.h"

#include "lists.h"
#include "parse.h"
#include "variable.h"
#include "rules.h"
#include "dstrings.h"

#include "command.h"


/*
 * cmd_new() - return a new CMD or 0 if too many args
 */
CMD *cmd_new (RULE *rule, LIST *targets, LIST *sources, LIST *shell, int maxline) {
  CMD *cmd = (CMD *)malloc(sizeof(CMD));
  dstr_init(&cmd->buf);
  cmd->rule = rule;
  cmd->shell = shell;
  cmd->next = 0;
  lol_init(&cmd->args);
  lol_add(&cmd->args, targets);
  lol_add(&cmd->args, sources);
  /* we don't free targets/sources/shell if bailing */
  if (var_string(rule->actions, &cmd->buf, &cmd->args, ' ') < 0) { cmd_free(cmd); return NULL; }
  return cmd;
}


/*
 * cmd_free() - free a CMD
 */
void cmd_free (CMD *cmd) {
  dstr_done(&cmd->buf);
  lol_free(&cmd->args);
  list_free(cmd->shell);
  free(cmd);
}
