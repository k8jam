/*
 * Copyright 1993, 2000 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * parse.c - make and destroy parse trees as driven by the parser
 */
#include "jam.h"
#include "lists.h"
#include "parse.h"
#include "scan.h"
#include "newstr.h"


static PARSE *yypsave;


//#include "jamgram.c"
#define ParseTOKENTYPE token_t
#define ParseARG_PDECL
extern void *ParseAlloc(void *(*mallocProc)(size_t));
extern void Parse(
  void *yyp,                   /* The parser */
  int yymajor,                 /* The major token code number */
  ParseTOKENTYPE yyminor       /* The value for the token */
  ParseARG_PDECL               /* Optional %extra_argument parameter */
);
extern void ParseFree(
  void *p,                    /* The parser to be deleted */
  void (*freeProc)(void*)     /* Function used to reclaim memory */
);


static int yyparse (void) {
  int tk;
  void *pp = ParseAlloc(malloc);
#ifndef NDEBUG
  /*ParseTrace(stderr, "**");*/
#endif
  do {
    tk = yylex();
    Parse(pp, tk, yylval);
  } while (tk != 0);
  ParseFree(pp, free);
  return 0;
}


void parse_file (const char *f) {
  /* suspend scan of current file and push this new file in the stream */
  yyfparse(f);
  /* now parse each block of rules and execute it */
  /* execute it outside of the parser so that recursive */
  /* calls to yyrun() work (no recursive yyparse's) */
  PARSE *old_yypsave = yypsave;
  for (;;) {
    LOL l;
    PARSE *p;
    int jmp = 0; /* JMP_NONE */
    /* $(<) and $(>) empty in outer scope */
    lol_init(&l);
    /* filled by yyparse() calling parse_save() */
    yypsave = 0;
    /* if parse error or empty parse, outta here */
    #if 0
    fprintf(stderr, "IN: <%s>\n", f);
    #endif
    if (yyparse() || !(p = yypsave)) {
      #if 0
      fprintf(stderr, "XOUT: <%s> (%p)\n", f, p);
      #endif
      break;
    }
    #if 0
    fprintf(stderr, "OUT: <%s>\n", f);
    #endif
    /* run the parse tree */
    list_free((*(p->func))(p, &l, &jmp));
    parse_free(p);
  }
  yypsave = old_yypsave;
}


void parse_save (PARSE *p) {
  yypsave = p;
}


PARSE *parse_make (
  LIST *(*func) (PARSE *p, LOL *args, int *jmp),
  PARSE *left,
  PARSE *right,
  PARSE *third,
  const char *string,
  const char *string1,
  int num)
{
  PARSE *p = (PARSE *)malloc(sizeof(PARSE));
  p->func = func;
  p->left = left;
  p->right = right;
  p->third = third;
  p->string = string;
  p->string1 = string1;
  p->num = num;
  p->refs = 1;
  return p;
}


void parse_refer (PARSE *p) {
  ++p->refs;
}


void parse_free (PARSE *p) {
  if (--p->refs) return;
  if (p->string) freestr(p->string);
  if (p->string1) freestr(p->string1);
  if (p->left) parse_free(p->left);
  if (p->right) parse_free(p->right);
  if (p->third) parse_free(p->third);
  free((char *)p);
}


JAMFA_CONST const char *multiform_suffix (int cnt) {
  return (cnt == 1 ? "" : "s");
}
