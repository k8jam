/*
 * Copyright 1994 Christopher Seiwald.  All rights reserved.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * matchglob.c - match a string against a simple pattern
 *
 * Understands the following patterns:
 *
 *  * any number of characters
 *  ? any single character
 *  [a-z] any single character in the range a-z
 *  [^a-z]  any single character not in the range a-z
 *  \x  match x
 *
 * External functions:
 *
 *  matchglob() - match a string against a simple pattern
 *
 * Internal functions:
 *
 *  globchars() - build a bitlist to check for character group match
 */
#include "jam.h"
#include "matchglob.h"


#define CHECK_BIT(tab, bit)  (tab[(bit)/8]&(1<<((bit)%8)))
#define SET_BIT(tab, bit)    (tab[(bit)/8] |= (1<<((bit)%8)))

/* bytes used for [chars] in compiled expr */
#define BITLISTSIZE  (32)


static inline int casechar (char ch) {
  return
    (ch >= 'a' && ch <= 'z') ? ch-32 :
    (ch >= 'A' && ch <= 'Z') ? ch+32 :
    ch;
}


static inline int lower (char ch) {
  return (ch >= 'A' && ch <= 'Z' ? ch+32 : ch);
}


static inline int samechars (char c0, char c1, int casesens) {
  return (casesens ? c0 == c1 : lower(c0) == lower(c1));
}


/*
 * globchars() - build a bitlist to check for character group match
 */
static void globchars (const char *s, const char *e, unsigned char *b, int casesens) {
  int neg = 0, c;
  memset(b, 0, BITLISTSIZE);
  if (*s == '^') ++neg, ++s;
  while (s < e) {
    if (s+2 < e && s[1] == '-') {
      for (c = (unsigned char)s[0]; c <= (unsigned char)s[2]; ++c) {
        SET_BIT(b, c);
        if (!casesens) SET_BIT(b, casechar(c));
      }
      s += 3;
    } else {
      c = (unsigned char)(*s++);
      SET_BIT(b, c);
      if (!casesens) SET_BIT(b, casechar(c));
    }
  }
  if (neg) for (c = 0; c < BITLISTSIZE; ++c) b[c] ^= 0xff;
  /* don't include \0 in either $[chars] or $[^chars] */
  b[0] &= 0xfe;
}


int matchglob (const char *pat, const char *str) {
  return matchglobex(pat, str, 1);
}


/*
 * matchglobex() - match a string against a simple pattern
 */
int matchglobex (const char *pat, const char *str, int casesens) {
  unsigned char bitlist[BITLISTSIZE];
  const char *here;
  for (;;) {
    switch (*pat++) {
      case '\0': return *str?-1:0;
      case '?': if (!*str++) return 1; break;
      case '[':
        /* scan for matching ] */
        here = pat;
        /* k8: allow []...] and [^]...] */
        if (*pat == '^') ++pat;
        if (*pat == ']') ++pat;
        /* k8 */
        do { if (!*pat++) return 1; } while (here == pat || *pat != ']') ;
        ++pat;
        /* build character class bitlist */
        globchars(here, pat, bitlist, casesens);
        if (!CHECK_BIT(bitlist, *(unsigned char *)str)) return 1;
        ++str;
        break;
      case '*':
        here = str;
        while (*str) ++str;
        /* try to match the rest of the pattern in a recursive call */
        /* if the match fails we'll back up chars, retrying */
        while (str != here) {
          int r = (*pat ? matchglobex(pat, str, casesens) : (*str ? -1 : 0)); /* a fast path for the last token in a pattern */
          if (!r) return 0;
          if (r < 0) return 1;
          --str;
        }
        break;
      case '\\':
        /* force literal match of next char */
        if (!*pat || !samechars(*str++, *pat++, casesens)) return 1;
        break;
      default:
        if (!samechars(*str++, pat[-1], casesens)) return 1;
        break;
    }
  }
}
