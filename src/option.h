/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * option.h - command line option processing
 */
#ifndef JAMH_OPTION_H
#define JAMH_OPTION_H


typedef struct option {
  char flag; /* filled in by getoption() */
  const char *val; /* set to random address if true */
} option;


extern int num_cfgargs;
extern char **cfgargs;


#define N_OPTS     (256)
#define N_TARGETS  (256)

extern int getoptions (int argc, char **argv, const char *opts, option *optv, char **targets);
extern const char *getoptval (option *optv, char opt, int subopt) JAMFA_PURE;


#endif
