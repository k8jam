/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "jambase.h"

#include "libhaunp.h"


static const uint8_t *jbpkdata;
static int jbpkdatalen;


static int bread (void *buf, int buflen, void *udata) {
  static int readpos = 0;
  int res = 0;
  uint8_t *ob = (uint8_t *)buf;
  while (buflen > 0) {
    int left = jbpkdatalen-readpos;
    if (left == 0) break;
    if (left > buflen) left = buflen;
    memcpy(ob, jbpkdata+readpos, left);
    res += left;
    ob += left;
    readpos += left;
    buflen -= left;
  }
  return res;
}


void jambase_unpack (void) {
  if (jambase == NULL) {
    char *txt;
    size_t len, pos;
    int linecount = 1, linenum = 0;
    haunp_t hup;
    jbpkdata = (const uint8_t *)jambasepk;
    jbpkdatalen = jbpksize();
    {
      uint32_t n;
      memcpy(&n, jbpkdata, 4);
      jbpkdata += 4;
      len = n;
    }
    hup = haunp_open_io(bread, NULL);
    txt = calloc(len+1, 1);
    if (haunp_read(hup, txt, len) != (int)len) {
      fprintf(stderr, "FATAL: decompression error!\n");
      exit(1);
    }
    haunp_close(hup);
    //fprintf(stderr, "len=%u\n", len);
    //fprintf(stderr, "%s\n", txt);
    for (size_t f = 0; f < len; ++f) if (txt[f] == '\n') ++linecount;
    //fprintf(stderr, "lc=%d\n", linecount);
    jambase = (char **)calloc(linecount, sizeof(char *));
    pos = 0;
    while (pos < len) {
      char *e = txt+len, ec;
      size_t epos = len;
      for (size_t f = pos; f < len; ++f) {
        if (txt[f] == '\n') { e = txt+f+1; epos = f+1; break; }
      }
      ec = *e;
      *e = 0;
      //fprintf(stderr, "%d: %s", linenum, txt+pos);
      jambase[linenum++] = strdup(txt+pos);
      *e = ec;
      pos = epos;
    }
    free(txt);
    //fprintf(stderr, "lc=%d; ln=%d\n", linecount, linenum);
    //fprintf(stderr, "::%s", jambase[0]);
  }
}
