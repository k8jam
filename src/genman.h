/*
 * ttman - text to man converter
 *
 * Copyright 2006 Timo Hirvonen <tihirvon@gmail.com>
 *
 * This file is licensed under the GPLv2.
 *
 * changes by Ketmar // Invisible Vector
 */
#ifndef GENMAN_H
#define GENMAN_H


extern void generate_man (const char *infname, const char *outfname);


#endif

