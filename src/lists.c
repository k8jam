/*
 * Copyright 1993, 1995 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * lists.c - maintain lists of strings
 *
 * This implementation essentially uses a singly linked list, but
 * guarantees that the head element of every list has a valid pointer
 * to the tail of the list, so the new elements can efficiently and
 * properly be appended to the end of a list.
 *
 * To avoid massive allocation, list_free() just tacks the whole freed
 * chain onto freelist and list_new() looks on freelist first for an
 * available list struct.  list_free() does not free the strings in the
 * chain: it lazily lets list_new() do so.
 */
#include "jam.h"
#include "newstr.h"
#include "lists.h"


static LIST *freelist = NULL;  /* junkpile for list_free() */


/*
 * list_append() - append a list onto another one, returning total
 */
LIST *list_append (LIST *l, LIST *nl) {
  if (!nl) {
    /* Just return l */
  } else if (!l) {
    l = nl;
  } else {
    /* graft two non-empty lists */
    l->tail->next = nl;
    l->tail = nl->tail;
  }
  return l;
}


static JAMFA_PURE int contains (LIST *l, const char *str) {
  for (; l != NULL; l = l->next) if (strcmp(l->string, str) == 0) return 1;
  return 0;
}


/*
 * list_removeall() - remove all occurences of `nl` items from `l`
 */
LIST *list_removeall (LIST *l, LIST *nl) {
  if (nl && l) {
    LIST *p = NULL, *h = NULL;
    while (l != NULL) {
      LIST *n = l->next;
      if (contains(nl, l->string)) {
        // remove (i.e. move node to freelist)
        if (p != NULL) p->next = n;
        if (n == NULL && h != NULL) h->tail = p;
        l->next = freelist;
        freelist = l;
      } else {
        // skip
        if (h == NULL) h = l; // save list head
        p = l;
        if (n == NULL && h != NULL) h->tail = l;
      }
      l = n;
    }
    l = h;
  }
  return l;
}


/* get list struct from freelist, if one available, otherwise allocate */
/* if from freelist, must free string first */
static inline LIST *alloc_item (void) {
  LIST *l;
  if (freelist) {
    l = freelist;
    freestr(l->string);
    freelist = freelist->next;
  } else {
    l = malloc(sizeof(*l));
  }
  return l;
}


/*
 * list_new() - tack a string onto the end of a list of strings
 */
/* copy!=0: copystr; else newstr */
LIST *list_new (LIST *head, const char *string, int copy) {
  LIST *l;
  if (DEBUG_LISTS) printf("list > %s <\n", string);
  /* copy/newstr as needed */
  string = (copy ? copystr(string) : newstr(string));
  l = alloc_item();
  /* if first on chain, head points here */
  /* if adding to chain, tack us on */
  /* tail must point to this new, last element */
  if (!head) head = l; else head->tail->next = l;
  head->tail = l;
  l->next = 0;
  l->string = string;
  return head;
}


/*
 * list_copy() - copy a whole list of strings (nl) onto end of another (l)
 */
LIST *list_copy (LIST *l, LIST *nl) {
  for (; nl; nl = list_next(nl)) l = list_new(l, nl->string, 1);
  return l;
}


/*
 * list_sublist() - copy a subset of a list of strings
 */
LIST *list_sublist (LIST *l, int start, int count) {
  LIST *nl = NULL;
  for (; l && start--; l = list_next(l)) ;
  for (; l && count--; l = list_next(l)) nl = list_new(nl, l->string, 1);
  return nl;
}


/* backported from boost-jam; TEST IT!!! */
/*
 * list_sort() - sort list
 */
LIST *list_sort (LIST *l) {
  LIST *first = NULL, *second = NULL, *merged = l, *result;
  if (!l) return L0;
  for (;;) {
    /* split the list in two */
    LIST **dst = &first;
    LIST *src = merged;
    for (;;) {
      *dst = list_append(*dst, list_new(0, src->string, 1));
      if (!src->next) break;
      if (strcmp(src->string, src->next->string) > 0) dst = (dst==&first)?&second:&first;
      src = src->next;
    }
    if (merged != l) list_free(merged);
    merged = 0;
    if (second == 0) { result = first; break; }
    /* merge lists 'first' and 'second' into 'merged' and free 'first'/'second' */
    {
      LIST *f = first, *s = second;
      while (f && s) {
        if (strcmp(f->string, s->string) < 0) {
          merged = list_append(merged, list_new(0, f->string, 1));
          f = f->next;
        } else {
          merged = list_append(merged, list_new(0, s->string, 1));
          s = s->next;
        }
      }
      merged = list_copy(merged, f);
      merged = list_copy(merged, s);
      list_free(first);
      list_free(second);
      first = second = 0;
    }
  }
  return result;
}


/* returns new list */
LIST *list_reverse (const LIST *l) {
  LIST *res = L0, *last = L0;
  if (l != L0) {
    res = last = alloc_item();
    last->string = copystr(l->string);
    last->next = L0;
    last->tail = last;
    l = l->next;
  }
  for (; l != NULL; l = l->next) {
    LIST *i = alloc_item();
    i->string = copystr(l->string);
    i->next = res;
    res = i;
  }
  if (res != L0) res->tail = last;
  return res;
}



/*
 * list_free() - free a list of strings
 */
void list_free (LIST *head) {
  /* Just tack onto freelist. */
  if (head) {
    head->tail->next = freelist;
    freelist = head;
  }
}


/*
 * list_print() - print a list of strings to stdout
 */
void list_print_ex (FILE *fo, const LIST *l, int flags) {
  int spc = (l == NULL);
  for (; l; l = list_next(l)) {
    if (spc && (flags&LPFLAG_NO_SPACES) == 0) fputc(' ', fo);
    fputs(l->string, fo);
    spc = 1;
  }
  if (spc && (flags&LPFLAG_NO_TRSPACE) == 0) fputc(' ', fo);
}


#if 0
/* FIXME! */
/*
 * list_printq() - print a list of safely quoted strings to a file
 */
void list_printq (FILE *out, LIST *l) {
  /* dump each word, enclosed in "s */
  /* suitable for Jambase use. */
  for (; l; l = list_next(l)) {
    const char *p = l->string;
    const char *ep = p+strlen(p);
    const char *op = p;
    fputc('\n', out);
    fputc('\t', out);
    fputc('"', out);
    /* any embedded "'s?  Escape them */
    while ((p = (char *)memchr(op, '"', ep-op))) {
      fwrite(op, p-op, 1, out);
      fputc('\\', out);
      fputc('"', out);
      op = p+1;
    }
    /* Write remainder */
    fwrite(op, ep-op, 1, out);
    fputc('"', out);
    fputc(' ', out);
  }
}
#endif


/*
 * list_length() - return the number of items in the list
 */
JAMFA_PURE int list_length (const LIST *l) {
  int n;
  for (n = 0; l; l = list_next(l), ++n) ;
  return n;
}


/*
 * lol_init() - initialize a LOL (list of lists)
 */
void lol_init (LOL *lol) {
  lol->count = 0;
}


/*
 * lol_add() - append a LIST onto an LOL
 */
void lol_add (LOL *lol, LIST *l) {
  if (lol->count < LOL_MAX) lol->list[lol->count++] = l;
}


/*
 * lol_free() - free the LOL and its LISTs
 */
void lol_free (LOL *lol) {
  for (int i = 0; i < lol->count; ++i) list_free(lol->list[i]);
  lol->count = 0;
}


/*
 * lol_get() - return one of the LISTs in the LOL
 */
JAMFA_PURE LIST *lol_get (LOL *lol, int i) {
  return (i >= 0 && i < lol->count ? lol->list[i] : NULL);
}


/*
 * lol_print() - debug print LISTS separated by ":"
 */
void lol_print (const LOL *lol) {
  for (int i = 0; i < lol->count; ++i) {
    if (i) printf(":");
    list_print(lol->list[i]);
  }
}
