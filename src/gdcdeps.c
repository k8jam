#define _GNU_SOURCE
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>

#include <sys/types.h>
#include <sys/wait.h>

#include "jam.h"
#include "lists.h"
#include "newstr.h"

#include "gdcdeps.h"


//#define GDC_DEPS_DEBUG


extern LIST *var_get (const char *symbol);


static LIST *processDepFile (const char *fname) {
  LIST *res = NULL;
  FILE *fl = fopen(fname, "r");
  if (fl == NULL) return NULL;
  char *fcont;
  long fsize;
  fseek(fl, 0, SEEK_END);
  fsize = ftell(fl);
  if (fsize <= 0) { fclose(fl); return NULL; }
  fseek(fl, 0, SEEK_SET);
  fcont = calloc(fsize+2, 1);
  fread(fcont, fsize, 1, fl);
  fclose(fl);
  uint8_t *tmp = (uint8_t *)fcont;
  // skip "file.o:"
#ifdef GDC_DEPS_DEBUG
  printf("{{%s}}\n", tmp);
#endif
  while (*tmp && *tmp != ':') ++tmp;
  if (tmp[0] && tmp[1]) {
    ++tmp;
    while (*tmp) {
      while (*tmp && (tmp[0] <= 32 || tmp[0] == '\\')) ++tmp;
      if (!tmp[0]) goto quit;
#ifdef GDC_DEPS_DEBUG
      printf("[%s]\n", tmp);
#endif
      uint8_t *e = tmp;
      while (*e && e[0] > ' ' && e[0] != '\\') ++e;
      *e = 0;
      res = list_new(res, (void *)tmp, 0); // don't copy it
#ifdef GDC_DEPS_DEBUG
      printf(" {%s}\n", tmp);
#endif
      *e = ' ';
      tmp = e;
    }
  }
quit:
  free(fcont);
  return res;
}

#ifdef _AIX
int vasprintf(char **ret, const char *format, va_list args)
{
    va_list copy;
    va_copy(copy, args);

    /* Make sure it is determinate, despite manuals indicating otherwise */
    *ret = NULL;

    int count = vsnprintf(NULL, 0, format, args);
    if (count >= 0)
    {
        char *buffer = malloc(count + 1);
        if (buffer == NULL)
            count = -1;
        else if ((count = vsnprintf(buffer, count + 1, format, copy)) < 0)
            free(buffer);
        else
            *ret = buffer;
    }
    va_end(copy);  // Each va_start() or va_copy() needs a va_end()

    return count;
}

int asprintf(char **ret, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    int count = vasprintf(ret, format, args);
    va_end(args);
    return(count);
}
#endif

#define ADDARG(a_)  do { \
  if (argCount >= 510) goto error_quit; \
  args[argCount++] = (a_); \
} while (0)


#define ADDARGPF(...)  do { \
  if (argCount >= 510) goto error_quit; \
  asprintf(&args[argCount], __VA_ARGS__); \
  ++argCount; \
} while (0)


/*
#define ADDINCS(vname_)  do { \
  for (const LIST *hdrs = var_get(vname_); hdrs != NULL; hdrs = hdrs->next) { \
    if (hdrs->string[0]) { \
      ADDARGPF("-I%s", hdrs->string); \
      ADDARGPF("-J%s", hdrs->string); \
    } \
  } \
} while (0)
*/


// NULL: can't, do text scan
LIST *gdcDeps (const char *srcfname) {
  LIST *res = NULL;
  const char *compilerBin = NULL;
  static char *args[512];
  int argCount = 0;
  char tmpFName[128];
  strcpy(tmpFName, "/tmp/jamgdctmp_XXXXXX");
  // get out if here if D compiler is not 'gdc'
  {
    const LIST *gdc = var_get("GDC");
    if (gdc == NULL || gdc->string == NULL) return NULL;
    if (strcmp(gdc->string, "gdc") == 0) {
      compilerBin = "gdc";
    } else {
      return NULL;
    }
    gdc = var_get("ENABLE_GDC_DEPS");
    if (gdc == NULL || gdc->string == NULL || !gdc->string[0]) return NULL;
  }
  // output file name
  {
    int fd = mkstemp(tmpFName);
    if (fd < 0) return NULL;
    close(fd);
  }
  ADDARG(strdup(compilerBin));
  ADDARG(strdup("-c"));
  ADDARG(strdup("-o"));
  ADDARG(strdup("/dev/null"));
  // copy '-fdebug' flags
  for (const LIST *flg = var_get("GDCFLAGS.all"); flg != NULL; flg = flg->next) {
    if (strncmp(flg->string, "-fdebug", 7) == 0) ADDARG(strdup(flg->string));
  }
  // add HDRS
  for (const LIST *hdrs = var_get("HDRS"); hdrs != NULL; hdrs = hdrs->next) {
    if (hdrs->string[0]) {
      ADDARGPF("-I%s", hdrs->string);
      ADDARGPF("-J%s", hdrs->string);
    }
  }
  // add SUBDIR_TOKENS
  static char zpath[8192];
  strcpy(zpath, ".");
  for (const LIST *hdrs = var_get("SUBDIR_TOKENS"); hdrs != NULL; hdrs = hdrs->next) {
    if (hdrs->string[0]) {
      strcat(zpath, "/");
      strcat(zpath, hdrs->string);
      ADDARGPF("-I%s", zpath);
      ADDARGPF("-J%s", zpath);
    }
  }
  // add input file path to includes
  strcpy(zpath, srcfname);
  {
    char *t = strrchr(zpath, '/');
    if (t != NULL) {
      *t = 0;
      ADDARGPF("-I%s", zpath);
      ADDARGPF("-J%s", zpath);
    }
  }
  // output to...
  ADDARGPF("-fmake-mdeps=%s", tmpFName);
  // filename
  ADDARG(strdup(srcfname));
  // done
  args[argCount] = NULL;
#ifdef GDC_DEPS_DEBUG
  // dump
  for (int f = 0; f < argCount; ++f) printf("%d: [%s]\n", f, args[f]);
  for (int f = 0; f < argCount; ++f) printf("%s ", args[f]); printf("\n");
#endif
  // exec it
  pid_t child = fork();
  if (child == (pid_t)-1) goto error_quit;
  if (child == 0) {
    // child
    // close stdout and stderr
    //close(0);
    close(1);
    close(2);
    execvp("gdc", args);
    //fprintf(stderr, "SHIT!!!\n");
    abort();
  }
  int status;
  waitpid(child, &status, 0);
  if (!WIFEXITED(status) || WEXITSTATUS(status) != 0) goto error_quit;
#ifdef GDC_DEPS_DEBUG
  printf("OK! %s\n", tmpFName);
#endif
  res = processDepFile(tmpFName);
  unlink(tmpFName);
error_quit:
  unlink(tmpFName);
  while (--argCount >= 0) free(args[argCount]);
  return res;
}
