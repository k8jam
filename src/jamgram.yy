/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * jamgram.yy - jam grammar
 */
%stack_size 0

%token_type {token_t}

%syntax_error {
  /*yyerror(&yyminor.yy0, "parsing error");*/
  yyerror(&yyminor, "parsing error");
}


%left `|` `||` .
%left `&` `&&` .
/*
%left `==` `=` `!=` `in` `anyin` `~=` .
%left `<` `<=` `>` `>=` .
%left `!` .
*/
%nonassoc `==` `=` `!=` `in` `anyin` `~=` .
%nonassoc `<` `<=` `>` `>=` .
%left `!` .


%include{
#include "jam.h"

#include "lists.h"
#include "variable.h"
#include "parse.h"
#include "scan.h"
#include "compile.h"
#include "newstr.h"
#include "rules.h"

/* for OSF and other less endowed yaccs */
/*#define YYMAXDEPTH  (10000)*/

#define F0  (LIST *(*)(PARSE *, LOL *, int *))0
#define P0  (PARSE *)0
#define S0  (char *)0

#define pappend(l,r)     parse_make(compile_append, l, r, P0, S0, S0, 0)
#define pbreak(l,f)      parse_make(compile_break, l, P0, P0, S0, S0, f)
#define peval(c,l,r)     parse_make(compile_eval, l, r, P0, S0, S0, c)
#define pfor(s,l,r,x)    parse_make(compile_foreach, l, r, P0, s, S0, x)
#define pif(l,r,t)       parse_make(compile_if, l, r, t, S0, S0, 0)
#define pincl(l,soft)    parse_make(compile_include, l, P0, P0, S0, S0, soft)
#define plist(s,sl)      parse_make(compile_list, P0, P0, P0, s, S0, sl)
#define plocal(l,r,t)    parse_make(compile_local, l, r, t, S0, S0, 0)
#define pnull()          parse_make(compile_null, P0, P0, P0, S0, S0, 0)
#define pon(l,r)         parse_make(compile_on, l, r, P0, S0, S0, 0)
#define prule(a,p)       parse_make(compile_rule, a, p, P0, S0, S0, 0)
#define prules(l,r)      parse_make(compile_rules, l, r, P0, S0, S0, 0)
#define pset(l,r,a)      parse_make(compile_set, l, r, P0, S0, S0, a)
#define pset1(l,r,t,a)   parse_make(compile_settings, l, r, t, S0, S0, a)
#define psetc(s,l,r)     parse_make(compile_setcomp, l, r, P0, s, S0, 0)
#define psete(s,l,s1,f)  parse_make(compile_setexec, l, P0, P0, s, s1, f)
#define pswitch(l,r)     parse_make(compile_switch, l, r, P0, S0, S0, 0)
#define pwhile(l,r)      parse_make(compile_while, l, r, P0, S0, S0, 0)

#define pnode(l,r)       parse_make(F0, l, r, P0, S0, S0, 0)
#define psnode(s,l)      parse_make(F0, l, P0, P0, s, S0, 0)
}


/******************************************************************************/
%start_symbol run


run ::= .           /* do nothing */
run ::= rules(R) .  { parse_save(R.parse); }


/*
 * block - zero or more rules
 * rules - one or more rules
 * rule - any one of jam's rules
 * right-recursive so rules execute in order.
 */
block(RR) ::= .           { RR.parse = pnull(); }
block(RR) ::= rules(R) .  { RR.parse = R.parse; }

rules(RR) ::= rule(R) .                                         { RR.parse = R.parse; }
rules(RR) ::= rule(R1) rules(R2) .                              { RR.parse = prules(R1.parse, R2.parse); }
rules(RR) ::= `local` list(LST) `;` block(B) .                  { RR.parse = plocal(LST.parse, pnull(), B.parse); }
rules(RR) ::= `local` list(LST1) `=` list(LST2) `;` block(B) .  { RR.parse = plocal(LST1.parse, LST2.parse, B.parse); }

rule(RR) ::= `{` block(B) `}` .                                 { RR.parse = B.parse; }
rule(RR) ::= `include` list(LST) `;` .                          { RR.parse = pincl(LST.parse, 0); }
rule(RR) ::= `softinclude` list(LST) `;` .                      { RR.parse = pincl(LST.parse, 1); }
rule(RR) ::= arg(A) lol(LST) `;` .                              { RR.parse = prule(A.parse, LST.parse); }
rule(RR) ::= arg(A) assign(X) list(LST) `;` .                   { RR.parse = pset(A.parse, LST.parse, X.number); }
rule(RR) ::= arg(A) `on` list(LST0) assign(X) list(LST1) `;` .  { RR.parse = pset1(A.parse, LST0.parse, LST1.parse, X.number); }
rule(RR) ::= `break` list(LST) `;` .                            { RR.parse = pbreak(LST.parse, JMP_BREAK); }
rule(RR) ::= `continue` list(LST) `;` .                         { RR.parse = pbreak(LST.parse, JMP_CONTINUE); }
rule(RR) ::= `return` list(LST) `;` .                           { RR.parse = pbreak(LST.parse, JMP_RETURN); }
rule(RR) ::= `for` local_opt(LO) T_ARG(A) `in` list(LST) `{` block(B) `}` .  { RR.parse = pfor(A.string, LST.parse, B.parse, LO.number); }
rule(RR) ::= `switch` list(LST) `{` cases(CS) `}` .             { RR.parse = pswitch(LST.parse, CS.parse); }
rule(RR) ::= `if` expr(EXP) `{` block(B) `}` .                  { RR.parse = pif(EXP.parse, B.parse, pnull()); }
rule(RR) ::= `if` expr(EXP) `{` block(B) `}` `else` rule(R) .   { RR.parse = pif(EXP.parse, B.parse, R.parse); }
rule(RR) ::= `while` expr(EXP) `{` block(B) `}` .               { RR.parse = pwhile(EXP.parse, B.parse); }
rule(RR) ::= `rule` T_ARG(A) params(PR) `{` block(B) `}` .      { RR.parse = psetc(A.string, PR.parse, B.parse); }
rule(RR) ::= `on` arg(A) rule(R) .                              { RR.parse = pon(A.parse, R.parse); }
rule(RR) ::= `actions` eflags(EF) T_ARG(A) bindlist(BL) set_string_mode `{` T_STRING(STR) `}` .
  { RR.parse = psete(A.string, BL.parse, STR.string, EF.number); }
/* note that lemon is LALR(1), so we have to move set_string_mode BEFORE opening brace */


local_opt(RR) ::= `local` .  { RR.number = 1; }
local_opt(RR) ::= .          { RR.number = 0; }


/*
 * assign - =, +=, ?=, -=
 */
assign(RR) ::= `=` .   { RR.number = VAR_SET; }
assign(RR) ::= `+=` .  { RR.number = VAR_APPEND; }
assign(RR) ::= `-=` .  { RR.number = VAR_REMOVE; }
assign(RR) ::= `?=` .  { RR.number = VAR_DEFAULT; }


/*
 * expr - an expression for if
 */
expr(RR) ::= arg(A) .                      { RR.parse = peval(EXPR_EXISTS, A.parse, pnull()); }
expr(RR) ::= expr(EXP0) `=` expr(EXP1) .   { RR.parse = peval(EXPR_EQUALS, EXP0.parse, EXP1.parse); }
expr(RR) ::= expr(EXP0) `==` expr(EXP1) .  { RR.parse = peval(EXPR_EQUALS, EXP0.parse, EXP1.parse); }
expr(RR) ::= expr(EXP0) `~=` arg(A) .      { RR.parse = peval(EXPR_REXPEQ, EXP0.parse, A.parse); }
expr(RR) ::= expr(EXP0) `!=` expr(EXP1) .  { RR.parse = peval(EXPR_NOTEQ, EXP0.parse, EXP1.parse); }
expr(RR) ::= expr(EXP0) `<` expr(EXP1) .   { RR.parse = peval(EXPR_LESS, EXP0.parse, EXP1.parse); }
expr(RR) ::= expr(EXP0) `<=` expr(EXP1) .  { RR.parse = peval(EXPR_LESSEQ, EXP0.parse, EXP1.parse); }
expr(RR) ::= expr(EXP0) `>` expr(EXP1) .   { RR.parse = peval(EXPR_MORE, EXP0.parse, EXP1.parse); }
expr(RR) ::= expr(EXP0) `>=` expr(EXP1) .  { RR.parse = peval(EXPR_MOREEQ, EXP0.parse, EXP1.parse); }
expr(RR) ::= expr(EXP0) `&` expr(EXP1) .   { RR.parse = peval(EXPR_AND, EXP0.parse, EXP1.parse); }
expr(RR) ::= expr(EXP0) `&&` expr(EXP1) .  { RR.parse = peval(EXPR_AND, EXP0.parse, EXP1.parse); }
expr(RR) ::= expr(EXP0) `|` expr(EXP1) .   { RR.parse = peval(EXPR_OR, EXP0.parse, EXP1.parse); }
expr(RR) ::= expr(EXP0) `||` expr(EXP1) .  { RR.parse = peval(EXPR_OR, EXP0.parse, EXP1.parse); }
expr(RR) ::= arg(A) `in` list(L) .         { RR.parse = peval(EXPR_IN, A.parse, L.parse); }
expr(RR) ::= arg(A) `anyin` list(L) .      { RR.parse = peval(EXPR_ANYIN, A.parse, L.parse); }
expr(RR) ::= `!` expr(E) .                 { RR.parse = peval(EXPR_NOT, E.parse, pnull()); }
expr(RR) ::= `(` expr(E) `)` .             { RR.parse = E.parse; }


/*
 * cases - action elements inside a 'switch'
 * case - a single action element inside a 'switch'
 * right-recursive rule so cases can be examined in order.
 */
cases(RR) ::= .                      { RR.parse = P0; }
cases(RR) ::= case(CS) cases(CSX) .  { RR.parse = pnode(CS.parse, CSX.parse); }

case(RR) ::= `case` T_ARG(A) `:` block(B) .  { RR.parse = psnode(A.string, B.parse); }


/*
 * params - optional parameter names to rule definition
 * right-recursive rule so that params can be added in order.
 */
params(RR) ::= .                         { RR.parse = P0; }
params(RR) ::= T_ARG(A) `:` params(P) .  { RR.parse = psnode(A.string, P.parse); }
params(RR) ::= T_ARG(A) .                { RR.parse = psnode(A.string, P0); }


/*
 * lol - list of lists
 * right-recursive rule so that lists can be added in order.
 */
lol(RR) ::= list(L) .               { RR.parse = pnode(P0, L.parse); }
lol(RR) ::= list(L) `:` lol(LOL) .  { RR.parse = pnode(LOL.parse, L.parse); }


/*
 * list - zero or more args in a LIST
 * listp - list (in puncutation only mode)
 * arg - one T_ARG or function call
 */
list(RR) ::= listp(L) .  { RR.parse = L.parse; yymode(SCAN_NORMAL); }

listp(RR) ::= .                  { RR.parse = pnull(); yymode(SCAN_PUNCT); }
listp(RR) ::= listp(L) arg(A) .  { RR.parse = pappend(L.parse, A.parse); }

arg(RR) ::= T_ARG(A) .                         { RR.parse = plist(A.string, A.strlit); }
arg(RR) ::= set_normal_mode `[` func(F) `]` .  { RR.parse = F.parse; }
/* note that lemon is LALR(1), so we have to move set_normal_mode BEFORE opening bracket */


/*
 * func - a function call (inside [])
 * This needs to be split cleanly out of 'rule'
 */
func(RR) ::= arg(A) lol(LOL) .                { RR.parse = prule(A.parse, LOL.parse); }
func(RR) ::= `on` arg(A0) arg(A1) lol(LOL) .  { RR.parse = pon(A0.parse, prule(A1.parse, LOL.parse)); }
func(RR) ::= `on` arg(A) `return` list(L) .   { RR.parse = pon(A.parse, L.parse); }


/*
 * eflags - zero or more modifiers to 'executes'
 * eflag - a single modifier to 'executes'
 */
eflags(RR) ::= .                        { RR.number = 0; }
eflags(RR) ::= eflags(EFL) eflag(EF) .  { RR.number = EFL.number|EF.number; }

eflag(RR) ::= `updated` .         { RR.number = RULE_UPDATED; }
eflag(RR) ::= `together` .        { RR.number = RULE_TOGETHER; }
eflag(RR) ::= `ignore` .          { RR.number = RULE_IGNORE; }
eflag(RR) ::= `quietly` .         { RR.number = RULE_QUIETLY; }
eflag(RR) ::= `piecemeal` .       { RR.number = RULE_PIECEMEAL; }
eflag(RR) ::= `existing` .        { RR.number = RULE_EXISTING; }
eflag(RR) ::= `maxline` T_ARG(A) .  {
  RR.number = atoi(A.string);
  if (RR.number < 1) RR.number = 1; else if (RR.number > 1024) RR.number = 1024;
  RR.number = (RR.number<<8)|RULE_MAXLINE;
}


/*
 * bindlist - list of variable to bind for an action
 */
bindlist(RR) ::= .                 { RR.parse = pnull(); }
bindlist(RR) ::= `bind` list(L) .  { RR.parse = L.parse; }


/*
 * mode change rules
 */
set_normal_mode ::= .  { /*if (DEBUG_SCAN) printf("**set_normal_mode\n");*/ yymode(SCAN_NORMAL); }
set_string_mode ::= .  { /*if (DEBUG_SCAN) printf("**set_string_mode\n");*/ yymode(SCAN_STRING); }
