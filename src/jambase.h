/*
 * Copyright 1993, 1995 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * jambase.h - declaration for the internal jambase
 *
 * The file Jambase is turned into a C array of strings in jambase.c
 * so that it can be built in to the executable.
 * This is the declaration for that array.
 */
#ifndef JAMH_JAMBASE_H
#define JAMH_JAMBASE_H

#include "jam.h"


extern char **jambase;
extern unsigned char jambasepk[];

extern int jbpksize (void) JAMFA_CONST;


#endif
