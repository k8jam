/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * search.c - find a target along $(SEARCH) or $(LOCATE)
 */
#include "jam.h"
#include "lists.h"
#include "search.h"
#include "timestamp.h"
#include "pathsys.h"
#include "variable.h"
#include "newstr.h"
#include "parse.h"
#include "compile.h"


static void call_bind_rule (const char *target, const char *boundname) {
  LIST *bind_rule = var_get("BINDRULE");
  if (bind_rule) {
    LOL lol;
    lol_init(&lol);
    lol_add(&lol, list_new(L0, target, 1));
    lol_add(&lol, list_new(L0, boundname, 1));
    if (lol_get(&lol, 1)) list_free(evaluate_rule(bind_rule->string, &lol, L0));
    lol_free(&lol);
  }
}


const char *search (const char *target, time_t *time) {
  PATHNAME f[1];
  LIST *varlist;
  static char buf[MAXJPATH];
  /* parse the filename */
  path_parse(target, f);
  f->f_grist.ptr = 0;
  f->f_grist.len = 0;
  if ((varlist = var_get("LOCATE"))) {
    f->f_root.ptr = varlist->string;
    f->f_root.len = strlen(varlist->string);
    path_build(buf, f); /* was with binding, but it does nothing now */
    if (DEBUG_SEARCH) printf("locate %s: %s\n", target, buf);
    timestamp(buf, time);
    return newstr(buf);
  }
  if ((varlist = var_get("SEARCH"))) {
    while (varlist) {
      f->f_root.ptr = varlist->string;
      f->f_root.len = strlen(varlist->string);
      path_build(buf, f); /* was with binding, but it does nothing now */
      if (DEBUG_SEARCH) printf("search %s: %s\n", target, buf);
      timestamp(buf, time);
      if (*time) return newstr(buf);
      varlist = list_next(varlist);
    }
  }
  /* look for the obvious */
  /* this is a questionable move: should we look in the obvious place if SEARCH is set? */
  f->f_root.ptr = 0;
  f->f_root.len = 0;
  path_build(buf, f); /* was with binding, but it does nothing now */
  if (DEBUG_SEARCH) printf("search %s: %s\n", target, buf);
  timestamp(buf, time);
  call_bind_rule(target, buf);
  return newstr(buf);
}
