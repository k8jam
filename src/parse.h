/*
 * Copyright 1993, 2000 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * parse.h - make and destroy parse trees as driven by the parser
 */
#ifndef JAMH_PARSE_H
#define JAMH_PARSE_H


/* parse tree node */
typedef struct _PARSE PARSE;
struct _PARSE {
  LIST *(*func) (PARSE *p, LOL *args, int *jmp);
  PARSE *left;
  PARSE *right;
  PARSE *third;
  const char *string;
  const char *string1;
  int num;
  int refs;
};


extern void parse_file (const char *f);
extern void parse_save (PARSE *p);

PARSE *parse_make (
  LIST *(*func) (PARSE *p, LOL *args, int *jmp),
  PARSE *left,
  PARSE *right,
  PARSE *third,
  const char *string,
  const char *string1,
  int num
);

extern void parse_refer (PARSE *p);
extern void parse_free (PARSE *p);


extern const char *multiform_suffix (int cnt) JAMFA_CONST;


#endif
