/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * builtins.c - builtin jam rules
 *
 * External routines:
 *
 *  load_builtin() - define builtin rules
 *
 * Internal routines:
 *
 *  builtin_depends() - DEPENDS/INCLUDES rule
 *  builtin_echo() - ECHO rule
 *  builtin_exit() - EXIT rule
 *  builtin_flags() - NOCARE, NOTFILE, TEMPORARY rule
 *  builtin_glob() - GLOB rule
 *  builtin_match() - MATCH rule
 *  builtin_hdrmacro() - HDRMACRO rule
 */
#include <limits.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <strings.h>
#include <unistd.h>
#ifdef _AIX
#include <alloca.h>
#endif

#include "jam.h"

#include "lists.h"
#include "parse.h"
#include "bjprng.h"
#include "builtins.h"
#include "rules.h"
#include "filesys.h"
#include "newstr.h"
#include "re9.h"
#include "pathsys.h"
#include "hdrmacro.h"
#include "matchglob.h"
#include "dstrings.h"


/*
 * builtin_depends() - DEPENDS/INCLUDES rule
 *
 * The DEPENDS builtin rule appends each of the listed sources on the
 * dependency list of each of the listed targets.
 * It binds both the targets and sources as TARGETs.
 */
static LIST *builtin_depends (PARSE *parse, LOL *args, int *jmp) {
  LIST *targets = lol_get(args, 0);
  LIST *sources = lol_get(args, 1);
  for (LIST *l = targets; l; l = list_next(l)) {
    TARGET *t = bindtarget(l->string);
    /* If doing INCLUDES, switch to the TARGET's include TARGET, creating it if needed.
     * The internal include TARGET shares the name of its parent. */
    if (parse->num) {
      if (!t->includes) t->includes = copytarget(t);
      t = t->includes;
    }
    t->depends = targetlist(t->depends, sources);
  }
  return L0;
}


typedef struct {
  int no_newline;
  int no_out;
  int flags;
  FILE *stream;
} echo_flags_t;


static void parse_echo_flags (echo_flags_t *flg, const LIST *l) {
  flg->no_newline = 0;
  flg->no_out = 0;
  flg->flags = LPFLAG_NO_TRSPACE;
  flg->stream = stdout;
  for (; l; l = list_next(l)) {
    const char *s = l->string;
    if (*s == '-') {
      for (++s; *s; ++s) {
        switch (*s) {
          case 'n': flg->no_newline = 1; break;
          case 'Q': flg->no_out = 1; break;
          case 'S': flg->flags |= LPFLAG_NO_SPACES; break;
          case 's': flg->flags &= ~LPFLAG_NO_TRSPACE; break;
          case 'w': flg->stream = stderr; break;
        }
      }
    }
  }
}


/*
 * builtin_echo() - ECHO rule
 *
 * The ECHO builtin rule echoes the targets to the user.
 * No other actions are taken.
 */
static LIST *builtin_echo (PARSE *parse, LOL *args, int *jmp) {
  echo_flags_t ef;
  parse_echo_flags(&ef, lol_get(args, 1));
  if (!ef.no_out) list_print_ex(ef.stream, lol_get(args, 0), ef.flags);
  if (!ef.no_newline) fputc('\n', ef.stream); else fflush(ef.stream);
  return L0;
}


/*
 * builtin_exit() - EXIT rule
 *
 * The EXIT builtin rule echoes the targets to the user and exits
 * the program with a failure status.
 */
static LIST *builtin_exit (PARSE *parse, LOL *args, int *jmp) {
  LIST *l = lol_get(args, 0);
  echo_flags_t ef;
  parse_echo_flags(&ef, lol_get(args, 1));
  if (l != NULL) {
    if (!ef.no_out) list_print_ex(ef.stream, l, ef.flags);
    if (!ef.no_newline) fputc('\n', ef.stream); else fflush(ef.stream);
    exit(EXITBAD); /* yeech */
  }
  exit(EXITOK);
#if __GNUC__ <= 2
  return L0;
#endif
}


/*
 * builtin_flags() - NOCARE, NOTFILE, TEMPORARY rule
 *
 * Builtin_flags() marks the target with the appropriate flag, for use by make0().
 * It binds each target as a TARGET.
 */
static LIST *builtin_flags (PARSE *parse, LOL *args, int *jmp) {
  LIST *l = lol_get(args, 0);
  int flag = parse->num, andflag = ~0;
  switch (flag) {
    case T_FLAG_NOCARE: andflag = ~T_FLAG_FORCECARE; break;
    case T_FLAG_FORCECARE: andflag = ~T_FLAG_NOCARE; break;
    case 666: flag = 0; andflag = ~T_FLAG_NOTFILE; break;
  }
  for (; l; l = list_next(l)) {
    TARGET *t = bindtarget(l->string);
    t->flags |= flag;
    t->flags &= andflag;
  }
  return L0;
}


typedef enum {
  GLOB_ANY,
  GLOB_DIRS,
  GLOB_FILES
} glob_mode;


/*
 * builtin_globbing() - GLOB rule
 */
struct globbing {
  LIST *patterns;
  LIST *results;
  int casesens;
  int cmptype; // <0:glob; 0: plain; >0:# of regexps
  glob_mode mode;
  int namesonly;
  regexp_t **re;
};


static void builtin_glob_back (void  *closure, const char *file, int status, time_t time) {
  struct globbing *globbing = (struct globbing *)closure;
  LIST *l;
  PATHNAME f;
  static char buf[MAXJPATH];
  /* null out directory for matching */
  /* we wish we had file_dirscan() pass up a PATHNAME */
  path_parse(file, &f);
  f.f_dir.len = 0;
  /* For globbing, we unconditionally ignore current and parent
   * directory items. Since those items always exist, there's no
   * reason why caller of GLOB would want to see them.
   * We could also change file_dirscan, but then paths with embedded
   * "." and ".." won't work anywhere. */
  /* k8: will this break anything? it shouldn't... */
  if (!strcmp(f.f_base.ptr, ".") || !strcmp(f.f_base.ptr, "..")) return;
  path_build(buf, &f);
  /*
  fprintf(stderr, "buf: [%s]\n", buf);
  int c;
  for (c = 0; c < 6; ++c) fprintf(stderr, " %d: [%s]\n", c, f.part[c].ptr);
  */
  if (globbing->mode != GLOB_ANY) {
    int ftype = file_type(file);
    switch (globbing->mode) {
      case GLOB_DIRS: if (ftype != 1) return; break;
      case GLOB_FILES: if (ftype != 0) return; break;
      default: ;
    }
  }
  if (globbing->cmptype < 0) {
    for (l = globbing->patterns; l; l = l->next) {
      if (matchglobex(l->string, buf, globbing->casesens) == 0) {
        globbing->results = list_new(globbing->results, (globbing->namesonly ? buf : file), 0);
        break;
      }
    }
  } else if (globbing->cmptype > 0) {
    for (int xxf = 0; xxf < globbing->cmptype; ++xxf) {
      if (regexp_execute(globbing->re[xxf], buf, NULL, 0) > 0) {
        globbing->results = list_new(globbing->results, (globbing->namesonly ? buf : file), 0);
        break;
      }
    }
  } else {
    for (l = globbing->patterns; l; l = l->next) {
      if ((globbing->casesens ? strcmp : strcasecmp)(l->string, buf) == 0) {
        globbing->results = list_new(globbing->results, (globbing->namesonly ? buf : file), 0);
        break;
      }
    }
  }
}


static LIST *builtin_glob (PARSE *parse, LOL *args, int *jmp) {
  LIST *l = lol_get(args, 0);
  LIST *r = lol_get(args, 1);
  LIST *lo;
  struct globbing globbing;
  if (!r) return L0;
  globbing.results = L0;
  globbing.patterns = r;
  globbing.casesens = 1;
  globbing.cmptype = -1;
  globbing.mode = GLOB_ANY;
  globbing.namesonly = 0;
  for (lo = lol_get(args, 2); lo != NULL; lo = lo->next) {
    if (!strcmp("case-sensitive", lo->string)) globbing.casesens = 1;
    else if (!strcmp("case-insensitive", lo->string)) globbing.casesens = 0;
    else if (!strcmp("ignore-case", lo->string)) globbing.casesens = 0;
    else if (!strcmp("glob", lo->string)) globbing.cmptype = -1;
    else if (!strcmp("regexp", lo->string)) globbing.cmptype = 1;
    else if (!strcmp("plain", lo->string)) globbing.cmptype = 0;
    else if (!strcmp("dirs-only", lo->string)) globbing.mode = GLOB_DIRS;
    else if (!strcmp("files-only", lo->string)) globbing.mode = GLOB_FILES;
    else if (!strcmp("any", lo->string)) globbing.mode = GLOB_ANY;
    else if (!strcmp("names-only", lo->string)) globbing.namesonly = 1;
    else if (!strcmp("full-path", lo->string)) globbing.namesonly = 0;
    else {
      printf("jam: invalid option for Glob built-in: '%s'\n", lo->string);
      exit(EXITBAD); /* yeech */
    }
  }
  if (globbing.cmptype > 0) {
    /* compile regexps */
    globbing.cmptype = list_length(r);
    globbing.re = malloc(sizeof(globbing.re[0])*globbing.cmptype);
    if (globbing.re == NULL) { printf("FATAL: out of memory in Glob\n"); exit(42); }
    for (int f = 0; r; r = r->next, ++f) globbing.re[f] = regexp_compile(r->string, (globbing.casesens ? 0 : RE9_FLAG_CASEINSENS));
  } else {
    globbing.re = NULL;
  }
  for (; l; l = list_next(l)) file_dirscan(l->string, builtin_glob_back, &globbing);
  if (globbing.re != NULL) {
    for (int f = 0; f < globbing.cmptype; ++f) regexp_free(globbing.re[f]);
    free(globbing.re);
  }
  return globbing.results;
}


/*
 * builtin_match() - MATCH rule, regexp matching
 */
static LIST *builtin_match (PARSE *parse, LOL *args, int *jmp) {
  LIST *l, *lo;
  LIST *res = L0;
  int casesens = 1;
  int cmptype = 1;
  for (lo = lol_get(args, 2); lo != NULL; lo = lo->next) {
    if (!strcmp("case-sensitive", lo->string)) casesens = 1;
    else if (!strcmp("case-insensitive", lo->string)) casesens = 0;
    else if (!strcmp("ignore-case", lo->string)) casesens = 0;
    else if (!strcmp("glob", lo->string)) cmptype = -1;
    else if (!strcmp("regexp", lo->string)) cmptype = 1;
    else if (!strcmp("plain", lo->string)) cmptype = 0;
    else {
      printf("jam: invalid option for Match built-in: '%s'\n", lo->string);
      exit(EXITBAD); /* yeech */
    }
  }
  /* for each pattern */
  for (l = lol_get(args, 0); l; l = l->next) {
    LIST *r;
    if (cmptype > 0) {
      regexp_t *re;
      re9_sub_t mt[RE9_SUBEXP_MAX];
      re = regexp_compile(l->string, (casesens ? 0: RE9_FLAG_CASEINSENS));
      /* for each string to match against */
      for (r = lol_get(args, 1); r; r = r->next) {
        mt[0].sp = mt[0].ep = NULL;
        if (regexp_execute(re, r->string, mt, RE9_SUBEXP_MAX) > 0) {
          dstring_t buf;
          /* add all parameters up to highest onto list */
          /* must have parameters to have results! */
          //fprintf(stderr, "re: <%s>: nsub=%d\n", re->restr, re9_nsub(re->re));
          dstr_init(&buf);
          for (int i = 1; i < re9_nsub(re->re); ++i) {
            int xxl = mt[i].ep-mt[i].sp;
            dstr_clear(&buf);
            if (xxl > 0) dstr_push_buf(&buf, mt[i].sp, xxl);
            res = list_new(res, dstr_cstr(&buf), 0);
          }
          /* add full match as last item */
          {
            int xxl = mt[0].ep-mt[0].sp;
            dstr_clear(&buf);
            if (xxl > 0) dstr_push_buf(&buf, mt[0].sp, xxl); else dstr_push_cstr(&buf, "1");
            res = list_new(res, dstr_cstr(&buf), 0);
          }
          dstr_done(&buf);
        }
      }
      regexp_free(re);
    } else if (cmptype < 0) {
      for (r = lol_get(args, 1); r; r = r->next) {
        if (matchglobex(l->string, r->string, casesens) == 0) {
          res = list_new(res, r->string, 0);
        }
      }
    } else {
      for (r = lol_get(args, 1); r; r = r->next) {
        if ((casesens ? strcmp : strcasecmp)(l->string, r->string) == 0) {
          res = list_new(res, r->string, 0);
        }
      }
    }
  }
  return res;
}


static LIST *builtin_hdrmacro (PARSE *parse, LOL *args, int *jmp) {
  LIST *l = lol_get(args, 0);
  for (; l; l = list_next(l)) {
    TARGET *t = bindtarget(l->string);
    /* scan file for header filename macro definitions */
    if (DEBUG_HEADER) printf("scanning '%s' for header file macro definitions\n", l->string);
    macro_headers(t);
  }
  return L0;
}


/* backported from boost-jam */
/*
 * Return the current working directory.
 *
 * Usage: pwd = [ PWD ] ;
 */
static LIST *builtin_pwd (PARSE *parse, LOL *args, int *jmp) {
  char pwd_buffer[PATH_MAX];
  if (!getcwd(pwd_buffer, sizeof(pwd_buffer))) {
    perror("can not get current directory");
    return L0;
  }
  return list_new(L0, pwd_buffer, 0);
}


/* backported from boost-jam */
static LIST *builtin_sort (PARSE *parse, LOL *args, int *jmp) {
  LIST *arg = lol_get(args, 0);
  arg = list_sort(arg);
  return arg;
}


/* backported from boost-jam; greatly improved */
/* Command shcmd [[ : options ]] */
static LIST *builtin_command (PARSE *parse, LOL *args, int *jmp) {
  LIST *res = L0;
  LIST *l;
  int ret;
  char buffer[1024], buf1[32], *spos, *epos;
  FILE *p = NULL;
  int exitStatus = -1;
  int optExitStatus = 0;
  int optNoOutput = 0;
  int optTrimLeft = 1;
  int optTrimRight = 1;
  int optStatus1st = 0;
  int optParseOut = 0;
  int optSpaceBreak = 1;
  int optTabBreak = 1;
  int optCRBreak = 1;
  int optLFBreak = 1;
  int no_options = ((l = lol_get(args, 1)) == NULL);
  dstring_t str;
  /* for each string in 2nd list: check for arg */
  for (; l != NULL; l = l->next) {
    if (!strcmp("exit-status", l->string)) optExitStatus = 1;
    else if (!strcmp("exit-code", l->string)) optExitStatus = 1;
    else if (!strcmp("status-first", l->string)) optStatus1st = 1;
    else if (!strcmp("code-first", l->string)) optStatus1st = 1;
    else if (!strcmp("no-output", l->string)) optNoOutput = 1;
    else if (!strcmp("no-trim", l->string)) optTrimLeft = optTrimRight = 0;
    else if (!strcmp("no-trim-left", l->string)) optTrimLeft = 0;
    else if (!strcmp("no-trim-right", l->string)) optTrimRight = 0;
    else if (!strcmp("parse-output", l->string)) optParseOut = 1;
    else if (!strcmp("no-space-break", l->string)) optSpaceBreak = 0;
    else if (!strcmp("no-tab-break", l->string)) optTabBreak = 0;
    else if (!strcmp("no-nl-break", l->string)) optLFBreak = 0;
    else if (!strcmp("no-lf-break", l->string)) optLFBreak = 0;
    else if (!strcmp("no-cr-break", l->string)) optCRBreak = 0;
    else if (!strcmp("dummy", l->string) || !strcmp("xyzzy", l->string)) {}
    else {
      printf("jam: invalid option for Command built-in: '%s'\n", l->string);
      exit(EXITBAD); /* yeech */
    }
  }
  if (no_options) optNoOutput = 1;
  /* build shell command */
  dstr_init(&str);
  /* for each arg */
  for (l = lol_get(args, 0); l; l = l->next) {
    if (dstr_len(&str)) dstr_push_char(&str, ' ');
    dstr_push_cstr(&str, l->string);
  }
  /* no shell command? */
  if (dstr_len(&str) < 1) { dstr_done(&str); return L0; }
  fflush(NULL); /* flush ALL output streams */
  p = popen(dstr_cstr(&str), "r");
  if (!p) { dstr_done(&str); return L0; }
  dstr_clear(&str);
  while ((ret = fread(buffer, sizeof(char), sizeof(buffer)-1, p)) > 0) {
    if (!optNoOutput) {
      buffer[ret] = 0;
      dstr_push_cstr(&str, buffer);
    }
  }
  exitStatus = pclose(p);
  if (no_options) {
    if (exitStatus) {
      snprintf(buf1, sizeof(buf1), "%d", exitStatus);
      res = list_new(L0, buf1, 0);
    } else {
      res = L0;
    }
  } else {
    if (optExitStatus && optStatus1st) {
      snprintf(buf1, sizeof(buf1), "%d", exitStatus);
      res = list_new(res, buf1, 0);
    }
    /* trim output if necessary */
    if (!optNoOutput) {
      if (!optParseOut) {
        /* don't parse */
        if (optTrimRight) {
          // trim trailing blanks
          int sl = dstr_len(&str);
          spos = dstr_cstr(&str);
          while (sl > 0 && (unsigned char)spos[sl-1] <= ' ') --sl;
          dstr_chop(&str, sl);
        }
        spos = dstr_cstr(&str);
        if (optTrimLeft) {
          // trim leading blanks
          while (*spos && *((unsigned char *)spos) <= ' ') ++spos;
        }
        res = list_new(res, spos, 0);
      } else {
        dstring_t tmp;
        /* parse output */
        ret = 0; /* was anything added? list must have at least one element */
        spos = dstr_cstr(&str);
        dstr_init(&tmp);
        while (*spos) {
          /* skip delimiters */
          while (*spos) {
            unsigned char ch = (unsigned char)(*spos);
            if (ch == ' ') { if (!optSpaceBreak) break; }
            else if (ch == '\t') { if (!optTabBreak) break; }
            else if (ch == '\r') { if (!optCRBreak) break; }
            else if (ch == '\n') { if (!optLFBreak) break; }
            else if (ch > ' ') break;
            ++spos;
          }
          if (!*spos) break;
          epos = spos+1;
          while (*epos) {
            int ch = *epos;
            if (ch == ' ') { if (optSpaceBreak) break; }
            else if (ch == '\t') { if (optTabBreak) break; }
            else if (ch == '\r') { if (optCRBreak) break; }
            else if (ch == '\n') { if (optLFBreak) break; }
            else if ((unsigned char)ch <= ' ') break;
            ++epos;
          }
          dstr_clear(&tmp);
          dstr_push_memrange(&tmp, spos, epos);
          res = list_new(res, dstr_cstr(&tmp), 0);
          ret = 1;
          spos = epos;
        }
        dstr_done(&tmp);
        if (!ret) { buf1[0] = '\0'; res = list_new(res, buf1, 0); }
      }
    }
    /* command exit result next */
    if (optExitStatus && !optStatus1st) {
      snprintf(buf1, sizeof(buf1), "%d", exitStatus);
      res = list_new(res, buf1, 0);
    }
  }
  dstr_done(&str);
  return res;
}


/* ExprI1 op0 math op1 */
static LIST *builtin_expri1 (PARSE *parse, LOL *args, int *jmp) {
  char buffer[100];
  int op0, op1, res, comp = 0;
  LIST *el = lol_get(args, 0);
  if (!el || !el->next) return L0;
  if (el->string[0] == '#') {
    // string length
    snprintf(buffer, sizeof(buffer), "%u", (unsigned int)(strlen(el->next->string)));
    return list_new(L0, buffer, 0);
  }
  if (!el->next->next) return L0;
  op0 = atoi(el->string);
  op1 = atoi(el->next->next->string);
  res = 0;
  switch (el->next->string[0]) {
    case '+': res = op0+op1; break;
    case '-': res = op0-op1; break;
    case '*': res = op0*op1; break;
    case '/': res = op0/op1; break;
    case '%': res = op0%op1; break;
    case '<':
      comp = 1;
      if (el->next->string[1] == '=') res = (op0 <= op1); else res = (op0 < op1);
      break;
    case '=': comp = 1; res = (op0 == op1); break;
    case '!': comp = 1; res = (op0 != op1); break;
    case '>':
      comp = 1;
      if (el->next->string[1] == '=') res = (op0 >= op1); else res = (op0 > op1);
      break;
    default:
      printf("jam: rule ExprI1: unknown operator: '%s'\n", el->next->string);
      exit(EXITBAD);
  }
  if (comp) return (res ? list_new(L0, "tan", 0) : L0);
  snprintf(buffer, sizeof(buffer), "%d", res);
  return list_new(L0, buffer, 0);
}


/* based on the code from ftjam by David Turner */
static LIST *builtin_split (PARSE *parse, LOL *args, int *jmp) {
  LIST *input = lol_get(args, 0);
  LIST *tokens = lol_get(args, 1);
  LIST *res = L0;
  char token[256];
  dstring_t str;
  int explode = 0;
  dstr_init(&str);
  /* build token array */
  if (tokens == NULL) {
    memset(token, 1, sizeof(token));
    explode = 1;
  } else {
    memset(token, 0, sizeof(token));
    for (; tokens; tokens = tokens->next) {
      const char *s = tokens->string;
      for (; *s; ++s) token[(unsigned char)*s] = 1;
    }
    if (memchr(token, 1, sizeof(token)) == NULL) {
      memset(token, 1, sizeof(token));
      explode = 1;
    }
  }
  token[0] = 0;
  /* now parse the input and split it */
  for (; input; input = input->next) {
    const char *ptr = input->string;
    const char *lastPtr = input->string;
    while (*ptr) {
      if (token[(unsigned char)*ptr]) {
        size_t count = ptr-lastPtr+explode;
        if (count > 0) {
          dstr_clear(&str);
          dstr_push_memrange(&str, lastPtr, ptr+explode);
          res = list_new(res, dstr_cstr(&str), 0);
        }
        lastPtr = ptr+1;
      }
      ++ptr;
    }
    if (ptr > lastPtr) res = list_new(res, lastPtr, 0);
  }
  dstr_done(&str);
  return res;
}


/*
 * builtin_dependslist()
 *
 * The DependsList builtin rule returns list of dependencies for
 * a given target.
 */
static LIST *builtin_dependslist (PARSE *parse, LOL *args, int *jmp) {
  LIST *res = L0;
  LIST *parents;
  for (parents = lol_get(args, 0); parents; parents = parents->next) {
    TARGET *t = bindtarget(parents->string);
    TARGETS *child;
    for (child = t->depends; child; child = child->next) res = list_new(res, child->target->name, 1);
  }
  return res;
}


/*
 * NormalizePath path [: pwd]
 *
 * it will add 'pwd' if path is not absolute and will try to resolve some '.' and '..' (only leading '..' though).
 * it can be used in SubDir replacement to automate dir building
 * if there is no $(2), use current directory as 'pwd'
 */
static LIST *builtin_normpath (PARSE *parse, LOL *args, int *jmp) {
  LIST *el = lol_get(args, 0), *pl = lol_get(args, 1);
  char *buf;
  int bsz;
  if (!el || !el->string) return L0;
  bsz = strlen(el->string)*2+1024;
  buf = malloc(bsz);
  if (buf == NULL) return L0;
  if (!normalize_path(el->string, buf, bsz, (pl != NULL ? pl->string : NULL))) { free(buf); return L0; }
  el = list_new(NULL, buf, 0);
  free(buf);
  return el;
}


/*
 * ListLength list
 */
static LIST *builtin_listlength (PARSE *parse, LOL *args, int *jmp) {
  char buffer[64];
  LIST *el = lol_get(args, 0);
  if (!el) return list_new(L0, "0", 0);
  snprintf(buffer, sizeof(buffer), "%d", list_length(el));
  return list_new(L0, buffer, 0);
}


/*
 * ListReverse list
 */
static LIST *builtin_listreverse (PARSE *parse, LOL *args, int *jmp) {
  return list_reverse(lol_get(args, 0));
}


/*
 * HaveRule and HaveActions
 */
typedef struct {
  LIST *el;
  int wantAction;
  int casesens;
} HRNGCI;


typedef struct {
  const char *str;
  int wantAction;
  int casesens;
} HRNormalData;


static int hr_normal (const void *hdata, void *udata) {
  const RULE *r = (const RULE *)hdata;
  const HRNormalData *d = (const HRNormalData *)udata;
  if (strcasecmp(r->name, d->str) == 0) {
    if (d->wantAction && r->actions) return 1; // got it
    if (!d->wantAction && r->procedure) return 1; // got it
  }
  return 0;
}


static int hr_glob (const void *hdata, void *udata) {
  const RULE *r = (const RULE *)hdata;
  const HRNormalData *d = (const HRNormalData *)udata;
  if (matchglobex(d->str, r->name, d->casesens) == 0) {
    //fprintf(stderr, ":[%s]\n", r->name);
    if (d->wantAction && r->actions) return 1; // got it
    if (!d->wantAction && r->procedure) return 1; // got it
  }
  return 0;
}


typedef struct {
  regexp_t *re;
  int reflags;
  int wantAction;
} HRREData;


static int hr_regexp (const void *hdata, void *udata) {
  const RULE *r = (const RULE *)hdata;
  HRREData *d = (HRREData *)udata;
  if (regexp_execute(d->re, r->name, NULL, 0) > 0) {
    //fprintf(stderr, ":[%s]\n", r->name);
    if (d->wantAction && r->actions) return 1; // got it
    if (!d->wantAction && r->procedure) return 1; // got it
  }
  return 0;
}


static LIST *builtin_haveruleactions (PARSE *parse, LOL *args, int *jmp) {
  LIST *el = lol_get(args, 0), *l;
  int wantAction = parse->num;
  int casesens = 1;
  int cmptype = 0; // <0:glob; >0:regexp
  if (!el) return L0;
  for (l = lol_get(args, 1); l != NULL; l = l->next) {
    if (!strcmp("case-sensitive", l->string)) casesens = 1;
    else if (!strcmp("case-insensitive", l->string)) casesens = 0;
    else if (!strcmp("ignore-case", l->string)) casesens = 0;
    else if (!strcmp("glob", l->string)) cmptype = -1;
    else if (!strcmp("regexp", l->string)) cmptype = 1;
    else if (!strcmp("plain", l->string)) cmptype = 0;
    else {
      printf("jam: invalid option for Have%s built-in: '%s'\n", (wantAction ? "Actions" : "Rule"), l->string);
      exit(EXITBAD); /* yeech */
    }
  }
  if (casesens == 1 && cmptype == 0) {
    // standard mode
    for (; el; el = el->next) {
      RULE *r = findrule(el->string);
      if (!r) return L0;
      if (wantAction && !r->actions) return L0;
      if (!wantAction && !r->procedure) return L0;
    }
  } else if (cmptype < 0) {
    // glob
    HRNormalData nfo;
    nfo.wantAction = wantAction;
    nfo.casesens = casesens;
    for (; el; el = el->next) {
      nfo.str = el->string;
      if (!iteraterules(hr_glob, &nfo)) return L0;
    }
  } else if (cmptype > 0) {
    // regexp
    HRREData nfo;
    nfo.wantAction = wantAction;
    for (; el; el = el->next) {
      int err;
      nfo.re = regexp_compile(el->string, (casesens ? 0 : RE9_FLAG_CASEINSENS));
      /*printf("FATAL: invalid regexp in Have%s: %s\n", (wantAction ? "Actions" : "Rule"), errmsg);*/
      err = iteraterules(hr_regexp, &nfo);
      regexp_free(nfo.re);
      if (!err) return L0;
    }
  } else {
    // normal, case-insensitive
    HRNormalData nfo;
    nfo.wantAction = wantAction;
    for (; el; el = el->next) {
      nfo.str = el->string;
      if (!iteraterules(hr_normal, &nfo)) return L0;
    }
  }
  return list_new(L0, "1", 0);
}


/* generate random name:
 * [ RandName ] -- /tmp/XXX
 * [ RandName "abc/" ] -- abc/XXX
 * [ RandName "" ] -- XXX
 */
static LIST *builtin_randname (PARSE *parse, LOL *args, int *jmp) {
  static BJRandCtx rctx;
  static int initialized = 0;
  static const char alphabet[] = "0123456789abcdefghijklmnopqrstuvwxyz";
  LIST *el;
  char buffer[9], *s;
  const char *path;
#ifdef OS_NT
  static char tp[8192]
#endif
  if (!initialized) {
    initialized = 1;
    bjprngRandomize(&rctx);
#ifdef OS_NT
    GetTempPath(sizeof(tp), tp);
#endif
  }
  for (int f = 0; f < 8; ++f) buffer[f] = alphabet[bjprngRand(&rctx)%strlen(alphabet)];
  buffer[8] = 0;
  el = lol_get(args, 0);
  path = (el != NULL && el->string != NULL ? el->string :
#ifdef OS_NT
    tp
#else
    "/tmp/"
#endif
  );
  s = alloca(strlen(path)+strlen(buffer)+2);
  sprintf(s, "%s%s", path, buffer);
  return list_new(L0, s, 0);
}


/* write list to file:
 * ListFileWrite filename : list [: terminator] [: append]
 * default terminator is '\n'
 * return success flag
 */
static LIST *builtin_listwrite (PARSE *parse, LOL *args, int *jmp) {
  LIST *el = lol_get(args, 0);
  if (el != NULL && el->string != NULL && el->string[0]) {
    LIST *l = lol_get(args, 3);
    FILE *fo = fopen(el->string, (l != NULL && l->string[0] ? "a" : "w"));
    if (fo != NULL) {
      const char *term;
      l = lol_get(args, 2);
      term = (l != NULL ? l->string : "\n");
      for (l = lol_get(args, 1); l != NULL; l = l->next) {
        if (fprintf(fo, "%s%s", l->string, term) < 0) {
          fclose(fo);
          unlink(el->string);
          return L0;
        }
      }
      fclose(fo);
      return list_new(L0, "1", 0);
    }
  }
  return L0;
}


/* remove duplicates from list */
static LIST *builtin_listremdups (PARSE *parse, LOL *args, int *jmp) {
  LIST *el = lol_get(args, 0);
  if (el != NULL) {
    LIST *l, *res = list_new(L0, el->string, 1);
    for (l = el->next; l != NULL; l = l->next) {
      int found = 0;
      for (LIST *t = el; t != l && !found; t = t->next) if (strcmp(t->string, l->string) == 0) found = 1;
      if (!found) res = list_new(res, l->string, 1);
    }
    return res;
  }
  return el; /* no need to change anything */
}


/*
 * compile_builtin() - define builtin rules
 */

#define P0  ((PARSE *)0)
#define C0  ((char *)0)


/* ":" -- previous name in upper case; "." -- previous name in lower case */
static inline void bind_builtin (PARSE *pp, const char *name) {
  bindrule(name)->procedure = pp;
}


static inline void bind_builtin2 (PARSE *pp, const char *name, const char *name1) {
  bindrule(name)->procedure = pp;
  bindrule(name1)->procedure = pp;
}


void load_builtins (void) {
  bind_builtin(parse_make(builtin_depends, P0, P0, P0, C0, C0, 0),
    "Depends");
  bind_builtin(parse_make(builtin_depends, P0, P0, P0, C0, C0, 1),
    "Includes");
  bind_builtin(parse_make(builtin_dependslist, P0, P0, P0, C0, C0, 0),
    "DependsList");

  bind_builtin(parse_make(builtin_flags, P0, P0, P0, C0, C0, T_FLAG_TOUCHED),
    "Always");
  bind_builtin(parse_make(builtin_flags, P0, P0, P0, C0, C0, T_FLAG_LEAVES),
    "Leaves");
  bind_builtin(parse_make(builtin_flags, P0, P0, P0, C0, C0, T_FLAG_NOCARE),
    "NoCare");
  bind_builtin2(parse_make(builtin_flags, P0, P0, P0, C0, C0, T_FLAG_NOTFILE),
    "NotFile", "NoTime");
  bind_builtin(parse_make(builtin_flags, P0, P0, P0, C0, C0, T_FLAG_NOUPDATE),
    "NoUpdate");
  bind_builtin(parse_make(builtin_flags, P0, P0, P0, C0, C0, T_FLAG_TEMP),
    "Temporary");
  bind_builtin(parse_make(builtin_flags, P0, P0, P0, C0, C0, T_FLAG_FORCECARE),
    "ForceCare");
  bind_builtin(parse_make(builtin_flags, P0, P0, P0, C0, C0, 666),
    "ForceFile");

  bind_builtin(parse_make(builtin_echo, P0, P0, P0, C0, C0, 0),
    "Echo");

  bind_builtin(parse_make(builtin_exit, P0, P0, P0, C0, C0, 0),
    "Exit");

  bind_builtin(parse_make(builtin_glob, P0, P0, P0, C0, C0, 0),
    "Glob");
  bind_builtin(parse_make(builtin_match, P0, P0, P0, C0, C0, 0),
    "Match");

  bind_builtin(parse_make(builtin_hdrmacro, P0, P0, P0, C0, C0, 0),
    "HdrMacro");

  bind_builtin(parse_make(builtin_pwd, P0, P0, P0, C0, C0, 0),
    "Pwd");

  bind_builtin(parse_make(builtin_sort, P0, P0, P0, C0, C0, 0),
    "Sort");

  bind_builtin(parse_make(builtin_command, P0, P0, P0, C0, C0, 0),
    "Command");

  bind_builtin(parse_make(builtin_expri1, P0, P0, P0, C0, C0, 0),
    "ExprI1");

  bind_builtin(parse_make(builtin_split, P0, P0, P0, C0, C0, 0),
    "Split");

  bind_builtin(parse_make(builtin_normpath, P0, P0, P0, C0, C0, 0),
    "NormalizePath");

  bind_builtin(parse_make(builtin_listlength, P0, P0, P0, C0, C0, 0),
    "ListLength");
  bind_builtin(parse_make(builtin_listwrite, P0, P0, P0, C0, C0, 0),
    "ListWrite");
  bind_builtin(parse_make(builtin_listremdups, P0, P0, P0, C0, C0, 0),
    "ListRemoveDuplicates");
  bind_builtin(parse_make(builtin_listreverse, P0, P0, P0, C0, C0, 0),
    "ListReverse");

  bind_builtin(parse_make(builtin_haveruleactions, P0, P0, P0, C0, C0, 0),
    "HaveRule");
  bind_builtin(parse_make(builtin_haveruleactions, P0, P0, P0, C0, C0, 1),
    "HaveActions");
  bind_builtin(parse_make(builtin_haveruleactions, P0, P0, P0, C0, C0, 0),
    "HasRule");
  bind_builtin(parse_make(builtin_haveruleactions, P0, P0, P0, C0, C0, 1),
    "HasActions");

  bind_builtin(parse_make(builtin_randname, P0, P0, P0, C0, C0, 0),
    "RandName");
}
