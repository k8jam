/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "jam.h"
#include "lists.h"
#include "parse.h"
#include "rules.h"
#include "newstr.h"
#include "hash.h"
#include "variable.h"
#include "search.h"

#include "progress.h"


#define PROGRESS_WINDOW  (10)


typedef struct {
  time_t when;
  int completed;
} TIMESTAMP;


struct _PROGRESS {
  int total;
  int stampcount;
  TIMESTAMP stamps[PROGRESS_WINDOW];
};


static double get_time_delta (void) {
  static double res = 10.0;
  static int checked = 0;
  if (!checked) {
    LIST *var = var_get("K8JAM-PROGRESS-TIME-DELTA"); /*FIXME: a perfectly idiotic name*/
    if (var) {
      TARGET *t = bindtarget(var->string);
      pushsettings(t->settings);
      t->boundname = search(t->name, &t->time);
      popsettings(t->settings);
      if (t->boundname) {
        res = atof(t->boundname);
        if (res < 0.0) res = 0.0;
      }
      checked = 1;
    }
  }
  return res;
}


#define LAST_STAMP(p)   ((p)->stamps[(p)->stampcount-1])
#define FIRST_STAMP(p)  ((p)->stamps[0])


PROGRESS *progress_start (int total) {
  PROGRESS *p = malloc(sizeof(PROGRESS));
  p->total = total;
  p->stampcount = 0;
  progress_update(p, 0);
  return p;
}


static double progress_estimate (PROGRESS *progress) {
  int count, left;
  double elapsed, rate;
  if (progress->stampcount < 2) return 0.0;
  count = LAST_STAMP(progress).completed-FIRST_STAMP(progress).completed;
  left = progress->total-LAST_STAMP(progress).completed;
  elapsed = (double)(LAST_STAMP(progress).when-FIRST_STAMP(progress).when);
  if (elapsed <= 0.1) elapsed = 0.1;
  rate = count/elapsed;
  return left/rate;
}


double progress_update (PROGRESS *progress, int completed) {
  time_t now;
  double dd = get_time_delta();
  time(&now);
  /* only return progress every 10 seconds */
  if (progress->stampcount > 0 && (dd <= 0.00001 || difftime(now, LAST_STAMP(progress).when) < dd)) return 0.0;
  if (progress->stampcount == PROGRESS_WINDOW) {
    memmove(progress->stamps, progress->stamps+1, sizeof(progress->stamps[0])*(PROGRESS_WINDOW-1));
    --progress->stampcount;
  }
  ++progress->stampcount;
  LAST_STAMP(progress).completed = completed;
  LAST_STAMP(progress).when = now;
  return progress_estimate(progress);
}
