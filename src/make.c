/*
 * Copyright 1993, 1995 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * make.c - bring a target up to date, once rules are in place
 *
 * This modules controls the execution of rules to bring a target and
 * its dependencies up to date.  It is invoked after the targets, rules,
 * et. al. described in rules.h are created by the interpreting of the
 * jam files.
 *
 * This file contains the main make() entry point and the first pass
 * make0().  The second pass, make1(), which actually does the command
 * execution, is in make1.c.
 *
 * External routines:
 *  make() - make a target, given its name
 *
 * Internal routines:
 *  make0() - bind and scan everything to make a TARGET
 *  make0sort() - reorder TARGETS chain by their time (newest to oldest)
 */
#include "jam.h"

#include "lists.h"
#include "parse.h"
#include "variable.h"
#include "rules.h"

#include "search.h"
#include "newstr.h"
#include "make.h"
#include "hcache.h"
#include "headers.h"
#include "command.h"

//#ifdef OPT_IMPROVED_PROGRESS_EXT
#include "progress.h"
//#endif

#ifndef max
# define max(a,b) ((a)>(b)?(a):(b))
#endif


typedef struct {
  int temp;
  int updating;
  int cantfind;
  int cantmake;
  int targets;
  int made;
} COUNTS;


static void make0 (TARGET *t, TARGET *p, int depth, COUNTS *counts, int anyhow);


static TARGETS *make0sort (TARGETS *c);


static const char *target_fate[] = {
  "init",    /* T_FATE_INIT */
  "making",  /* T_FATE_MAKING */
  "stable",  /* T_FATE_STABLE */
  "newer",   /* T_FATE_NEWER */
  "temp",    /* T_FATE_ISTMP */
  "touched", /* T_FATE_TOUCHED */
  "missing", /* T_FATE_MISSING */
  "needtmp", /* T_FATE_NEEDTMP */
  "old",     /* T_FATE_OUTDATED */
  "update",  /* T_FATE_UPDATE */
  "nofind",  /* T_FATE_CANTFIND */
  "nomake"   /* T_FATE_CANTMAKE */
};


static const char *target_bind[] = {
  "unbound",
  "missing",
  "parents",
  "exists",
};

#define spaces(x)  ("                "+16-((x) > 16 ? 16 : (x)))


/*
 * Return 'silent make' flag
 */
int is_make_silent (void) {
  LIST *var = var_get("JAM_OPTION_MAKE_UPDATES_SILENT");
  return (var && var->string &&
    strcmp(var->string, "ona") != 0 &&
    strcmp(var->string, "no") != 0 &&
    strcmp(var->string, "false") != 0 &&
    strcmp(var->string, "0") != 0);
}


/*
 * make() - make a target, given its name
 */
int make (int n_targets, const char **targets, int anyhow) {
  int i;
  COUNTS counts[1];
  int status = 0; /* 1 if anything fails */
//#ifdef OPT_HEADER_CACHE_EXT
  hcache_init();
//#endif
  memset((char *)counts, 0, sizeof(*counts));
  for (i = 0; i < n_targets; ++i) {
    TARGET *t = bindtarget(targets[i]);
    make0(t, 0, 0, counts, anyhow);
  }
  if (DEBUG_MAKE) {
    if (!is_make_silent()) {
      if (counts->targets) printf("...found %d target%s...\n", counts->targets, multiform_suffix(counts->targets));
      if (counts->temp) printf("...using %d temp target%s...\n", counts->temp, multiform_suffix(counts->temp));
      if (counts->updating) printf("...updating %d target%s...\n", counts->updating, multiform_suffix(counts->updating));
    }
    if (counts->cantfind) printf("...can't find %d target%s...\n", counts->cantfind, multiform_suffix(counts->cantfind));
    if (counts->cantmake) printf("...can't make %d target%s...\n", counts->cantmake, multiform_suffix(counts->cantmake));
  }
  //k8: don't write cache here, 'cause build directory can be created later; write cache in main jam function
//#ifdef OPT_HEADER_CACHE_EXT
  //hcache_done();
//#endif
  globs.updating = counts->updating; //k8
  globs.progress = progress_start(counts->updating);
  status = counts->cantfind || counts->cantmake;
  for (i = 0; i < n_targets; ++i) status |= make1(bindtarget(targets[i]));
  return status;
}


/*
 * make0() - bind and scan everything to make a TARGET
 *
 * Make0() recursively binds a target, searches for #included headers,
 * calls itself on those headers, and calls itself on any dependents.
 */
static void make0 (
  TARGET *t,
  TARGET *p, /* parent */
  int depth, /* for display purposes */
  COUNTS  *counts, /* for reporting */
  int anyhow /* forcibly touch all (real) targets */
) {
  TARGETS *c, *incs;
  TARGET *ptime = t;
  time_t last, leaf, hlast;
  int fate;
  const char *flag = "";
  SETTINGS *s;
  /* step 1: initialize */
  if (DEBUG_MAKEPROG) printf("make\t--\t%s%s\n", spaces(depth), t->name);
  t->fate = T_FATE_MAKING;
  /* step 2: under the influence of "on target" variables, bind the target and search for headers */
  /* step 2a: set "on target" variables */
  s = copysettings(t->settings);
  pushsettings(s);
  /* step 2b: find and timestamp the target file (if it's a file) */
  if (t->binding == T_BIND_UNBOUND && !(t->flags&T_FLAG_NOTFILE)) {
    t->boundname = search(t->name, &t->time);
    t->binding = t->time ? T_BIND_EXISTS : T_BIND_MISSING;
  }
  /* INTERNAL, NOTFILE header nodes have the time of their parents */
  if (p && t->flags&T_FLAG_INTERNAL) ptime = p;
  /* if temp file doesn't exist but parent does, use parent */
  if (p && t->flags&T_FLAG_TEMP && t->binding == T_BIND_MISSING && p->binding != T_BIND_MISSING) {
    t->binding = T_BIND_PARENTS;
    ptime = p;
  }
#ifdef JAM_OPT_SEMAPHORE
  {
    LIST *var = var_get("JAM_SEMAPHORE");
    if (var) {
      TARGET *semaphore = bindtarget(var->string);
      semaphore->progress = T_MAKE_SEMAPHORE;
      t->semaphore = semaphore;
    }
  }
#endif
  /* step 2c: If its a file, search for headers */
  if (t->binding == T_BIND_EXISTS) headers(t);
  /* step 2d: reset "on target" variables */
  popsettings(s);
  freesettings(s);
  /* pause for a little progress reporting */
  if (DEBUG_MAKEPROG) {
    if (strcmp(t->name, t->boundname)) {
      printf("bind\t--\t%s%s: %s\n", spaces(depth), t->name, t->boundname);
    }
    switch (t->binding) {
      case T_BIND_UNBOUND: case T_BIND_MISSING: case T_BIND_PARENTS:
        printf("time\t--\t%s%s: %s\n", spaces(depth), t->name, target_bind[ (int)t->binding ]);
        break;
      case T_BIND_EXISTS:
        printf("time\t--\t%s%s: %s", spaces(depth), t->name, ctime(&t->time));
        break;
    }
  }
  /* step 3: recursively make0() dependents & headers */
  /* step 3a: recursively make0() dependents */
  for (c = t->depends; c; c = c->next) {
    int internal = t->flags&T_FLAG_INTERNAL;
    if (DEBUG_DEPENDS) printf("%s \"%s\" : \"%s\" ;\n", internal?"Includes":"Depends", t->name, c->target->name);
    /* warn about circular deps, except for includes, which include each other alot */
    if (c->target->fate == T_FATE_INIT) make0(c->target, ptime, depth+1, counts, anyhow);
    else if (c->target->fate == T_FATE_MAKING && !internal) printf("warning: %s depends on itself\n", c->target->name);
  }
  /* step 3b: recursively make0() internal includes node */
  if (t->includes) make0(t->includes, p, depth+1, counts, anyhow);
  /* step 3c: add dependents' includes to our direct dependencies */
  incs = 0;
  for (c = t->depends; c; c = c->next) {
    if (c->target->includes) {
      incs = targetentry(incs, c->target->includes);
      /* if the includes are newer than we are their original target also needs to be marked newer */
      /* this is needed so that 'updated' correctly will include the original target in the $(<) variable */
      if (c->target->includes->time > t->time) {
        //if (DEBUG_DEPENDS) printf("TIME*\t--\t%s%s: %s :: %s\n", spaces(depth), t->name, ctime(&t->time), ctime(&c->target->includes->time));
        c->target->fate = max(T_FATE_NEWER, c->target->fate);
      }
    }
  }
  t->depends = targetchain(t->depends, incs);
  /* step 4: compute time & fate */
  /* step 4a: pick up dependents' time and fate */
  last = 0;
  leaf = 0;
  fate = T_FATE_STABLE;
  for (c = t->depends; c; c = c->next) {
    /* if LEAVES has been applied, we only heed the timestamps of the leaf source nodes */
    leaf = max(leaf, c->target->leaf);
    if (t->flags&T_FLAG_LEAVES) { last = leaf; continue; }
    last = max(last, c->target->time);
    fate = max(fate, c->target->fate);
  }
  /* step 4b: pick up included headers time */
  /* if a header is newer than a temp source that includes it, the temp source will need building */
  hlast = t->includes ? t->includes->time : 0;
  /* step 4c: handle NOUPDATE oddity */
  /* If a NOUPDATE file exists, make dependents eternally old */
  /* don't inherit our fate from our dependents */
  /* decide fate based only upon other flags and our binding (done later) */
  if (t->flags&T_FLAG_NOUPDATE) {
    last = 0;
    t->time = 0;
    fate = T_FATE_STABLE;
  }
  /* step 4d: determine fate: rebuild target or what? */
  /*
   * In English:
   * If can't find or make child, can't make target.
   * If children changed, make target.
   * If target missing, make it.
   * If children newer, make target.
   * If temp's children newer than parent, make temp.
   * If temp's headers newer than parent, make temp.
   * If deliberately touched, make it.
   * If up-to-date temp file present, use it.
   * If target newer than non-notfile parent, mark target newer.
   * Otherwise, stable!
   *
   * Note this block runs from least to most stable:
   * as we make it further down the list, the target's
   * fate is getting stabler.
   */
  if (fate >= T_FATE_BROKEN) {
    fate = T_FATE_CANTMAKE;
  } else if (fate >= T_FATE_SPOIL) {
    fate = T_FATE_UPDATE;
  } else if (t->binding == T_BIND_MISSING) {
    fate = T_FATE_MISSING;
  } else if (t->binding == T_BIND_EXISTS && last > t->time) {
    fate = T_FATE_OUTDATED;
  } else if (t->binding == T_BIND_PARENTS && last > p->time) {
    fate = T_FATE_NEEDTMP;
  } else if (t->binding == T_BIND_PARENTS && hlast > p->time) {
    fate = T_FATE_NEEDTMP;
  } else if (t->flags&T_FLAG_TOUCHED) {
    fate = T_FATE_TOUCHED;
  } else if (anyhow && !(t->flags&T_FLAG_NOUPDATE)) {
    fate = T_FATE_TOUCHED;
  } else if (t->binding == T_BIND_EXISTS && (t->flags&T_FLAG_TEMP)) {
    fate = T_FATE_ISTMP;
  } else if (t->binding == T_BIND_EXISTS && p && p->binding != T_BIND_UNBOUND && t->time > p->time) {
    fate = T_FATE_NEWER;
  } else {
    fate = T_FATE_STABLE;
  }
  /* step 4e: handle missing files */
  /* if it's missing and there are no actions to create it, boom */
  /* if we can't make a target we don't care about, 'sokay */
  /* we could insist that there are updating actions for all missing
   * files, but if they have dependents we just pretend it's NOTFILE */
  if (fate == T_FATE_MISSING && !t->actions && !t->depends) {
    if (t->flags&T_FLAG_NOCARE) {
      fate = T_FATE_STABLE;
    } else {
      printf("don't know how to make %s\n", t->name);
      fate = T_FATE_CANTFIND;
    }
  }
  /* step 4f: propagate dependents' time & fate */
  /* set leaf time to be our time only if this is a leaf */
  t->time = max(t->time, last);
  t->leaf = leaf?leaf:t->time;
  t->fate = fate;
  /* step 5: sort dependents by their update time */
  if (globs.newestfirst) t->depends = make0sort(t->depends);
  /* step 6: a little harmless tabulating for tracing purposes */
  /* don't count or report interal includes nodes */
  if (t->flags&T_FLAG_INTERNAL) return;
  ++counts->targets;
  if (counts->targets%4096 == 0 && DEBUG_MAKE) printf("...patience...\n");

  if (fate == T_FATE_ISTMP) ++counts->temp;
  else if (fate == T_FATE_CANTFIND) ++counts->cantfind;
  else if (fate == T_FATE_CANTMAKE && t->actions) ++counts->cantmake;
  else if (fate >= T_FATE_BUILD && fate < T_FATE_BROKEN && t->actions) ++counts->updating;

  if (!(t->flags&T_FLAG_NOTFILE) && fate >= T_FATE_SPOIL) flag = "+";
  else if (t->binding == T_BIND_EXISTS && p && t->time > p->time) flag = "*";

  if (DEBUG_MAKEPROG) printf("made%s\t%s\t%s%s\n", flag, target_fate[(int)t->fate], spaces(depth), t->name);
  if (DEBUG_CAUSES && t->fate >= T_FATE_NEWER && t->fate <= T_FATE_MISSING) {
    printf("%s %s\n", target_fate[(int)t->fate], t->name);
  }
}


/*
 * make0sort() - reorder TARGETS chain by their time (newest to oldest)
 */
static TARGETS *make0sort (TARGETS *chain) {
  TARGETS *result = 0;
  /* we walk chain, taking each item and inserting it on the
   * sorted result, with newest items at the front */
  /* this involves updating each TARGETS' c->next and c->tail */
  /* note that we make c->tail a valid prev pointer for every entry */
  /* normally, it is only valid at the head, where prev == tail */
  /* note also that while tail is a loop, next ends at the end of the chain */
  /* walk current target list */
  while (chain) {
    TARGETS *c = chain, *s = result;
    chain = chain->next;
    /* find point s in result for c */
    while (s && s->target->time > c->target->time) s = s->next;
    /* insert c in front of s (might be 0) */
    /* don't even think of deciphering this */
    c->next = s; /* good even if s = 0 */
    if (result == s) result = c; /* new head of chain? */
    if (!s) s = result; /* wrap to ensure a next */
    if (result != c) s->tail->next = c; /* not head? be prev's next */
    c->tail = s->tail; /* take on next's prev */
    s->tail = c; /* make next's prev us */
  }
  return result;
}
