/*
 * Copyright 1994 Christopher Seiwald.  All rights reserved.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * matchglob.h - match a string against a simple pattern
 *
 * Understands the following patterns:
 *
 *  * any number of characters
 *  ? any single character
 *  [a-z] any single character in the range a-z
 *  [^a-z]  any single character not in the range a-z
 *  \x  match x
 */
#ifndef JAMH_MATCHGLOB_H
#define JAMH_MATCHGLOB_H


/* returns 0 on success */
extern int matchglob (const char *pat, const char *str);
extern int matchglobex (const char *pat, const char *str, int casesens);


#endif
