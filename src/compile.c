/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * compile.c - compile parsed jam statements
 *
 * External routines:
 *
 *  compile_append() - append list results of two statements
 *  compile_break() - compile 'break/continue/return' rule
 *  compile_eval() - evaluate if to determine which leg to compile
 *  compile_foreach() - compile the "for x in y" statement
 *  compile_if() - compile 'if' rule
 *  compile_include() - support for 'include' - call include() on file
 *  compile_list() - expand and return a list
 *  compile_local() - declare (and set) local variables
 *  compile_null() - do nothing -- a stub for parsing
 *  compile_on() - run rule under influence of on-target variables
 *  compile_rule() - compile a single user defined rule
 *  compile_rules() - compile a chain of rules
 *  compile_set() - compile the "set variable" statement
 *  compile_setcomp() - support for `rule` - save parse tree
 *  compile_setexec() - support for `actions` - save execution string
 *  compile_settings() - compile the "on =" (set variable on exec) statement
 *  compile_switch() - compile 'switch' rule
 *
 * Internal routines:
 *
 *  debug_compile() - printf with indent to show rule expansion.
 *  evaluate_rule() - execute a rule invocation
 */
#include "jam.h"

#include "lists.h"
#include "parse.h"
#include "compile.h"
#include "variable.h"
#include "expand.h"
#include "rules.h"
#include "newstr.h"
#include "search.h"
#include "matchglob.h"
#include "filesys.h"


static const char *set_names[] = { "=", "+=", "-=", "?=" };
static void debug_compile (int which, const char *s);


/*
 * compile_append() - append list results of two statements
 *
 *  parse->left   more compile_append() by left-recursion
 *  parse->right  single rule
 */
LIST *compile_append (PARSE *parse, LOL *args, int *jmp) {
  /* Append right to left. */
  return list_append(
    (*parse->left->func)(parse->left, args, jmp),
    (*parse->right->func)(parse->right, args, jmp)
  );
}


/*
 * compile_break() - compile 'break/continue/return' rule
 *
 *  parse->left  results
 *  parse->num   JMP_BREAK/CONTINUE/RETURN
 */
LIST *compile_break (PARSE *parse, LOL *args, int *jmp) {
  LIST *lv = (*parse->left->func)(parse->left, args, jmp);
  *jmp = parse->num;
  return lv;
}


/*
 * compile_eval() - evaluate if to determine which leg to compile
 *
 * Returns:
 *  list  if expression true - compile 'then' clause
 *  L0    if expression false - compile 'else' clause
 */
static JAMFA_PURE int lcmp (LIST *t, LIST *s) {
  int status = 0;
  while (!status && (t || s)) {
    const char *st = (t ? t->string : "");
    const char *ss = (s ? s->string : "");
    status = strcmp(st, ss);
    t = (t ? list_next(t) : t);
    s = (s ? list_next(s) : s);
  }
  return status;
}


static int recmp (LIST *s, LIST *restr) {
  int res;
  /*
  printf("restr: <%s>\n", (restr ? restr->string : "(null)"));
  printf("s    : <%s>\n", (s ? s->string : "(null)"));
  */
  regexp_t *re = regexp_compile((restr ? restr->string : ""), 0);
  res = regexp_execute(re, (s ? s->string : ""), NULL, 0);
  regexp_free(re);
  return (res > 0);
}


LIST *compile_eval (PARSE *parse, LOL *args, int *jmp) {
  LIST *ll, *lr, *s, *t;
  int status = 0;
  /* short circuit lr eval for &&, ||, 'in' and 'any-in' */
  ll = (*parse->left->func)(parse->left, args, jmp);
  lr = 0;
  switch (parse->num) {
    case EXPR_AND:
    case EXPR_IN:
    case EXPR_ANYIN:
      if (ll) goto eval;
      break;
    case EXPR_OR:
      if (!ll) goto eval;
      break;
    default:
eval: lr = (*parse->right->func)(parse->right, args, jmp);
      break;
  }
  /* now eval */
  switch (parse->num) {
    case EXPR_NOT: if (!ll) status = 1; break;
    case EXPR_AND: if (ll && lr) status = 1; break;
    case EXPR_OR: if (ll || lr) status = 1; break;
    case EXPR_IN:
      /* "a in b": make sure each of ll is equal to something in lr */
      for (t = ll; t; t = list_next(t)) {
        for (s = lr; s; s = list_next(s)) if (strcmp(t->string, s->string) == 0) break;
        if (!s) break;
      }
      /* No more ll? success */
      if (!t) status = 1;
      break;
    case EXPR_ANYIN:
      /* "a any-in b": does b contains at least one item from a? */
      for (t = ll; t && !status; t = list_next(t)) {
        for (s = lr; s; s = list_next(s)) if (strcmp(t->string, s->string) == 0) { status = 1; break; } /* success */
      }
      break;
    case EXPR_EXISTS: if (lcmp(ll, L0) != 0) status = 1; break;
    case EXPR_EQUALS: if (lcmp(ll, lr) == 0) status = 1; break;
    case EXPR_NOTEQ: if (lcmp(ll, lr) != 0) status = 1; break;
    case EXPR_LESS: if (lcmp(ll, lr) < 0) status = 1; break;
    case EXPR_LESSEQ: if (lcmp(ll, lr) <= 0) status = 1; break;
    case EXPR_MORE: if (lcmp(ll, lr) > 0) status = 1; break;
    case EXPR_MOREEQ: if (lcmp(ll, lr) >= 0) status = 1; break;
    case EXPR_REXPEQ: if (recmp(ll, lr)) status = 1; break;
  }
  if (DEBUG_IF) {
    debug_compile(0, "if");
    list_print(ll);
    printf("(%d) ", status);
    list_print_ex(stdout, lr, LPFLAG_NO_TRSPACE);
    printf("\n");
  }
  /* find something to return */
  /* in odd circumstances (like "" = "") we'll have to return a new string */
  if (!status) t = NULL;
  else if (ll) { t = ll; ll = NULL; }
  else if (lr) { t = lr; lr = NULL; }
  else t = list_new(L0, "1", 0);
  if (ll) list_free(ll);
  if (lr) list_free(lr);
  return t;
}


/*
 * compile_foreach() - compile the "for x in y" statement
 *
 * Compile_foreach() resets the given variable name to each specified
 * value, executing the commands enclosed in braces for each iteration.
 *
 *  parse->string  index variable
 *  parse->left    variable values
 *  parse->right   rule to compile
 */
LIST *compile_foreach (PARSE *p, LOL *args, int *jmp) {
  LIST *nv = (*p->left->func)(p->left, args, jmp);
  LIST *result = NULL;
  SETTINGS *s = NULL;
  if (p->num) {
    /* have 'local' */
    s = addsettings(s, VAR_SET, p->string, L0);
    pushsettings(s);
  }
  /* for each value for var */
  for (LIST *l = nv; l && *jmp == JMP_NONE; l = list_next(l)) {
    /* reset $(p->string) for each val */
    var_set(p->string, list_new(L0, l->string, 1), VAR_SET);
    /* keep only last result */
    list_free(result);
    result = (*p->right->func)(p->right, args, jmp);
    /* continue loop? */
    if (*jmp == JMP_CONTINUE) *jmp = JMP_NONE;
  }
  /* here by break/continue? */
  if (*jmp == JMP_BREAK || *jmp == JMP_CONTINUE) *jmp = JMP_NONE;
  if (p->num) {
    /* restore locals */
    popsettings(s);
    freesettings(s);
  }
  list_free(nv);
  /* returns result of last loop */
  return result;
}


/*
 * compile_if() - compile 'if' rule
 *
 *  parse->left   condition tree
 *  parse->right  then tree
 *  parse->third  else tree
 */
LIST *compile_if (PARSE *p, LOL *args, int *jmp) {
  LIST *l = (*p->left->func)(p->left, args, jmp);
  p = (l ? p->right : p->third);
  list_free(l);
  return (*p->func)(p, args, jmp);
}


/*
 * compile_include() - support for 'include' - call include() on file
 *
 *  parse->left list of files to include (can only do 1)
 *  parse->num  !0: softinclude
 */
LIST *compile_include (PARSE *parse, LOL *args, int *jmp) {
  int softinc = parse->num;
  LIST *nt = (*parse->left->func)(parse->left, args, jmp);
  if (DEBUG_COMPILE) {
    debug_compile(0, (softinc ? "softinclude" : "include"));
    list_print_ex(stdout, nt, LPFLAG_NO_TRSPACE);
    printf("\n");
  }
  if (nt) {
    TARGET *t = bindtarget(nt->string);
    /* bind the include file under the influence of "on-target" variables */
    /* though they are targets, include files are not built with make() */
    /* needn't copysettings(), as search sets no vars */
    pushsettings(t->settings);
    t->boundname = search(t->name, &t->time);
    popsettings(t->settings);
    /* don't parse missing file if NOCARE set */
    if (t->time || !(t->flags&T_FLAG_NOCARE)) {
      int doit = 1;
      if (file_type(t->boundname) != 0 || access(t->boundname, R_OK) != 0) {
        if (!softinc) {
          printf("Failed to include file '%s'\n", t->boundname);
          exit(EXITBAD);
        }
        doit = 0;
      }
      if (doit) parse_file(t->boundname);
    }
  }
  list_free(nt);
  return L0;
}


/*
 * compile_list() - expand and return a list
 *
 *  parse->string - character string to expand
 *  parse->num    - "don't expand" flag
 */
LIST *compile_list (PARSE *parse, LOL *args, int *jmp) {
  /* voodoo 1 means: parse->string is a copyable string */
  return (parse->num ? list_new(L0, parse->string, 1) : var_expand(parse->string, NULL, args, 1));
}


/*
 * compile_local() - declare (and set) local variables
 *
 *  parse->left   list of variables
 *  parse->right  list of values
 *  parse->third  rules to execute
 */
LIST *compile_local (PARSE *parse, LOL *args, int *jmp) {
  LIST *l;
  SETTINGS *s = NULL;
  LIST *nt = (*parse->left->func)(parse->left, args, jmp);
  LIST *ns = (*parse->right->func)(parse->right, args, jmp);
  LIST *result;
  if (DEBUG_COMPILE) {
    debug_compile(0, "local");
    list_print(nt);
    printf("= ");
    list_print_ex(stdout, ns, LPFLAG_NO_TRSPACE);
    printf("\n");
  }
  /* initial value is ns */
  for (l = nt; l; l = list_next(l)) s = addsettings(s, 0, l->string, list_copy((LIST *)0, ns));
  list_free(ns);
  list_free(nt);
  /* note that callees of the current context get this "local" */
  /* variable, making it not so much local as layered */
  pushsettings(s);
  result = (*parse->third->func)(parse->third, args, jmp);
  popsettings(s);
  freesettings(s);
  return result;
}


/*
 * compile_null() - do nothing -- a stub for parsing
 */
JAMFA_CONST LIST *compile_null (PARSE *parse, LOL *args, int *jmp) {
  return L0;
}


/*
 * compile_on() - run rule under influence of on-target variables
 *
 *  parse->left target list; only first used
 *  parse->right  rule to run
 */
LIST *compile_on (PARSE *parse, LOL *args, int *jmp) {
  LIST *nt = (*parse->left->func)(parse->left, args, jmp);
  LIST *result = NULL;
  if (DEBUG_COMPILE) {
    debug_compile(0, "on");
    list_print_ex(stdout, nt, LPFLAG_NO_TRSPACE);
    printf("\n");
  }
  /* copy settings, so that 'on target var on target = val' doesn't set var globally */
  if (nt) {
    TARGET *t = bindtarget(nt->string);
    SETTINGS *s = copysettings(t->settings);
    pushsettings(s);
    result = (*parse->right->func)(parse->right, args, jmp);
    popsettings(s);
    freesettings(s);
  }
  list_free(nt);
  return result;
}


/*
 * compile_rule() - compile a single user defined rule
 *
 *  parse->left list of rules to run
 *  parse->right  parameters (list of lists) to rule, recursing left
 *
 * Wrapped around evaluate_rule() so that headers() can share it.
 */
LIST *compile_rule (PARSE *parse, LOL *args, int *jmp) {
  LOL nargs[1];
  LIST *result = NULL;
  LIST *ll, *l;
  PARSE *p;
  /* list of rules to run -- normally 1! */
  ll = (*parse->left->func)(parse->left, args, jmp);
  /* build up the list of arg lists */
  lol_init(nargs);
  for (p = parse->right; p; p = p->left) lol_add(nargs, (*p->right->func)(p->right, args, jmp));
  /* run rules, appending results from each */
  for (l = ll; l; l = list_next(l)) result = evaluate_rule(l->string, nargs, result);
  list_free(ll);
  lol_free(nargs);
  return result;
}


/*
 * evaluate_rule() - execute a rule invocation
 */
LIST *evaluate_rule (const char *rulename, LOL *args, LIST  *result) {
//#infdef OPT_EXPAND_RULE_NAMES_EXT
  /*RULE *rule = bindrule(rulename);*/
//#else
  RULE *rule;
  char *expanded, *c;
  dstring_t buf;
  dstr_init(&buf);
  if (var_string(rulename, &buf, args, ' ') < 0) {
    dstr_done(&buf);
    printf("Failed to expand rule %s -- expansion too long\n", rulename);
    exit(EXITBAD);
  }
  expanded = dstr_cstr(&buf);
  while (expanded[0] == ' ') ++expanded;
  while ((c = strrchr(expanded, ' '))) *c = '\0';
  if (DEBUG_COMPILE) {
    debug_compile(1, rulename);
    if (strcmp(rulename, expanded)) printf("-> %s  ", expanded);
    lol_print(args);
    printf("\n");
  }
  rule = bindrule(expanded);
  dstr_done(&buf);
//#endif
  /* check traditional targets $(<) and sources $(>) */
  if (!rule->actions && !rule->procedure) printf("warning: unknown rule %s\n", rule->name);
  /* if this rule will be executed for updating the targets then construct the action for make() */
  if (rule->actions) {
    TARGETS *t;
    ACTION *action;
    /* the action is associated with this instance of this rule */
    action = (ACTION *)malloc(sizeof(ACTION));
    memset((char *)action, '\0', sizeof(*action));
    action->rule = rule;
    action->targets = targetlist((TARGETS *)0, lol_get(args, 0));
    action->sources = targetlist((TARGETS *)0, lol_get(args, 1));
    /* append this action to the actions of each target */
    for (t = action->targets; t; t = t->next) t->target->actions = actionlist(t->target->actions, action);
  }
  /* now recursively compile any parse tree associated with this rule */
  if (rule->procedure) {
    PARSE *parse = rule->procedure;
    SETTINGS *s = 0;
    int jmp = JMP_NONE;
    LIST *l;
    int i;
    /* build parameters as local vars */
    for (l = rule->params, i = 0; l; l = l->next, i++) s = addsettings(s, 0, l->string, list_copy(L0, lol_get(args, i)));
    /* run rule */
    /* bring in local params */
    /* refer/free to ensure rule not freed during use */
    parse_refer(parse);
    pushsettings(s);
    result = list_append(result, (*parse->func)(parse, args, &jmp));
    popsettings(s);
    freesettings(s);
    parse_free(parse);
  }
  if (DEBUG_COMPILE) debug_compile(-1, 0);
  return result;
}


/*
 * compile_rules() - compile a chain of rules
 *
 *  parse->left single rule
 *  parse->right  more compile_rules() by right-recursion
 */
LIST *compile_rules (PARSE *parse, LOL *args, int *jmp) {
  /* ignore result from first statement; return the 2nd */
  /* optimize recursion on the right by looping */
  LIST *result = NULL;
  while (*jmp == JMP_NONE && parse->func == compile_rules) {
    list_free(result);
    result = (*parse->left->func)(parse->left, args, jmp);
    parse = parse->right;
  }
  if (*jmp == JMP_NONE) {
    list_free(result);
    result = (*parse->func)(parse, args, jmp);
  }
  return result;
}


/*
 * compile_set() - compile the "set variable" statement
 *
 *  parse->left   variable names
 *  parse->right  variable values
 *  parse->num    VAR_SET/APPEND/DEFAULT
 */
LIST *compile_set (PARSE *parse, LOL *args, int *jmp) {
  LIST *nt = (*parse->left->func)(parse->left, args, jmp);
  LIST *ns = (*parse->right->func)(parse->right, args, jmp);
  LIST *l;
  if (DEBUG_COMPILE) {
    debug_compile(0, "set");
    list_print(nt);
    printf("%s ", set_names[parse->num]);
    list_print_ex(stdout, ns, LPFLAG_NO_TRSPACE);
    printf("\n");
  }
  /* call var_set to set variable */
  /* var_set keeps ns, so need to copy it */
  for (l = nt; l; l = list_next(l)) var_set(l->string, list_copy(L0, ns), parse->num);
  list_free(nt);
  return ns;
}


/*
 * compile_setcomp() - support for `rule` - save parse tree
 *
 *  parse->string  rule name
 *  parse->left    list of argument names
 *  parse->right   rules for rule
 */
LIST *compile_setcomp (PARSE *parse, LOL *args, int *jmp) {
  RULE *rule = bindrule(parse->string);
  LIST *params = 0;
  PARSE *p;
  /* build param list */
  for (p = parse->left; p; p = p->left) params = list_new(params, p->string, 1);
  if (DEBUG_COMPILE) {
    debug_compile(0, "rule");
    printf("%s ", parse->string);
    list_print_ex(stdout, params, LPFLAG_NO_TRSPACE);
    printf("\n");
  }
  /* free old one, if present */
  if (rule->procedure) parse_free(rule->procedure);
  if (rule->params) list_free(rule->params);
  rule->procedure = parse->right;
  rule->params = params;
  /* we now own this parse tree */
  /* don't let parse_free() release it */
  parse_refer(parse->right);
  return L0;
}


/*
 * compile_setexec() - support for `actions` - save execution string
 *
 *  parse->string   rule name
 *  parse->string1  OS command string
 *  parse->num      flags
 *  parse->left     `bind` variables
 *
 * Note that the parse flags (as defined in compile.h) are transfered
 * directly to the rule flags (as defined in rules.h).
 */
LIST *compile_setexec (PARSE *parse, LOL *args, int *jmp) {
  RULE *rule = bindrule(parse->string);
  LIST *bindlist = (*parse->left->func)(parse->left, args, jmp);
  /* free old one, if present */
  if (rule->actions) {
    freestr(rule->actions);
    list_free(rule->bindlist);
  }
  rule->actions = copystr(parse->string1);
  rule->bindlist = bindlist;
  rule->flags = parse->num;
  return L0;
}


/*
 * compile_settings() - compile the "on =" (set variable on exec) statement
 *
 *  parse->left   variable names
 *  parse->right  target name
 *  parse->third  variable value
 *  parse->num    VAR_SET/APPEND/DEFAULT
 */
LIST *compile_settings (PARSE *parse, LOL *args, int *jmp) {
  LIST *nt = (*parse->left->func)(parse->left, args, jmp);
  LIST *ns = (*parse->third->func)(parse->third, args, jmp);
  LIST *targets = (*parse->right->func)(parse->right, args, jmp);
  LIST *ts;
  if (DEBUG_COMPILE) {
    debug_compile(0, "set");
    list_print(nt);
    printf("on ");
    list_print(targets);
    printf("%s ", set_names[parse->num]);
    list_print_ex(stdout, ns, LPFLAG_NO_TRSPACE);
    printf("\n");
  }
  /* call addsettings to save variable setting addsettings keeps ns, so need to copy it */
  /* pass append flag to addsettings() */
  for (ts = targets; ts; ts = list_next(ts)) {
    TARGET *t = bindtarget(ts->string);
    for (LIST *l = nt; l; l = list_next(l)) {
      t->settings = addsettings(t->settings, parse->num, l->string, list_copy((LIST *)0, ns));
    }
  }
  list_free(nt);
  list_free(targets);
  return ns;
}


/*
 * compile_switch() - compile 'switch' rule
 *
 *  parse->left   switch value (only 1st used)
 *  parse->right  cases
 *
 *  cases->left   1st case
 *  cases->right  next cases
 *
 *  case->string  argument to match
 *  case->left    parse tree to execute
 */
LIST *compile_switch (PARSE *parse, LOL *args, int *jmp) {
  LIST *nt = (*parse->left->func)(parse->left, args, jmp);
  LIST *result = NULL;
  if (DEBUG_COMPILE) {
    debug_compile(0, "switch");
    list_print_ex(stdout, nt, LPFLAG_NO_TRSPACE);
    printf("\n");
  }
  /* step through cases */
  for (parse = parse->right; parse; parse = parse->right) {
    if (!matchglob(parse->left->string, nt?nt->string:"")) {
      /* get and exec parse tree for this case */
      parse = parse->left->left;
      result = (*parse->func)(parse, args, jmp);
      break;
    }
  }
  list_free(nt);
  return result;
}


/*
 * compile_while() - compile 'while' rule
 *
 *  parse->left   condition tree
 *  parse->right    execution tree
 */
LIST *compile_while (PARSE *p, LOL *args, int *jmp) {
  LIST *result = NULL;
  LIST *l;
  /* returns the value from the last execution of the block */
  while ((*jmp == JMP_NONE ) && (l = (*p->left->func)(p->left, args, jmp))) {
    /* always toss while's expression */
    list_free(l);
    /* keep only last result */
    list_free(result);
    result = (*p->right->func)(p->right, args, jmp);
    /* continue loop? */
    if (*jmp == JMP_CONTINUE) *jmp = JMP_NONE;
  }
  /* here by break/continue? */
  if (*jmp == JMP_BREAK || *jmp == JMP_CONTINUE) *jmp = JMP_NONE;
  /* returns result of last loop */
  return result;
}


/*
 * debug_compile() - printf with indent to show rule expansion.
 */
static void debug_compile (int which, const char *s) {
  static int level = 0;
  static const char indent[36] = ">>>>|>>>>|>>>>|>>>>|>>>>|>>>>|>>>>|";
  int i = ((1+level)*2)%35;
  if (which >= 0) printf("%*.*s ", i, i, indent);
  if (s) printf("%s ", s);
  level += which;
}
