#ifndef GDC_DEPS_H
#define GDC_DEPS_H

#include "jam.h"
#include "lists.h"


// NULL: can't, do text scan
extern LIST *gdcDeps (const char *srcfname);


#endif
