/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * jam.h - includes and globals for jam
 */
#ifndef JAMH_MAIN_H
#define JAMH_MAIN_H

#if defined(__GNUC__) && __GNUC__ > 2
# define JAMFA_PURE         __attribute__((pure))
# define JAMFA_CONST
# define JAMFA_NORETURN     __attribute__((noreturn))
# define JAMFA_PRINTF(m,n)  __attribute__((format(printf, m, n)))
# define JAMFA_SENTINEL     __attribute__((sentinel))
#else
# define JAMFA_PURE
# define JAMFA_CONST
# define JAMFA_NORETURN
# define JAMFA_PRINTF(m,n)
# define JAMFA_SENTINEL
#endif


/* Windows NT */
#ifdef NT
# include <fcntl.h>
# include <stdlib.h>
# include <stdio.h>
# include <ctype.h>
# include <malloc.h>
# include <memory.h>
# include <signal.h>
# include <string.h>
# include <time.h>
# define OSMAJOR "NT=true"
# define OSMINOR "OS=NT"
# define OS_NT
# define SPLITPATH ';'
# define MAXLINE 2046 /* longest 'together' actions */
/*# define USE_EXECNT*/
# define PATH_DELIM '\\'
# define DOWNSHIFT_PATHS
#endif


/* Windows MingW32 */
#ifdef MINGW
# include <fcntl.h>
# include <stdlib.h>
# include <stdio.h>
# include <ctype.h>
# include <malloc.h>
# include <memory.h>
# include <signal.h>
# include <string.h>
# include <time.h>
# define OSMAJOR "MINGW=true"
# define OSMINOR "OS=MINGW"
# define OS_NT
# define SPLITPATH ';'
# define MAXLINE 2046  /* longest 'together' actions */
# define PATH_DELIM '\\'
# define DOWNSHIFT_PATHS
#endif


/* God fearing UNIX */
#ifndef OSMINOR
# define OSMAJOR "UNIX=true"
# define USE_FILEUNIX
# define PATH_DELIM '/'
# ifdef __FreeBSD__
#  define OSMINOR "OS=FREEBSD"
#  define OS_FREEBSD
# endif
# ifdef linux
#  define OSMINOR "OS=LINUX"
#  define OS_LINUX
# endif
# ifdef __NetBSD__
#  define unix
#  define OSMINOR "OS=NETBSD"
#  define OS_NETBSD
#  define NO_VFORK
# endif
# ifdef __QNX__
#  ifdef __QNXNTO__
#   define OSMINOR "OS=QNXNTO"
#   define OS_QNXNTO
#  else
#   define unix
#   define OSMINOR "OS=QNX"
#   define OS_QNX
#   define NO_VFORK
#   define MAXLINE 996
#  endif
# endif
# ifdef __APPLE__
#  define unix
#  define OSMINOR "OS=MACOSX"
#  define OS_MACOSX
# endif
# ifdef _AIX
#  define OSMINOR "OS=AIX"
# endif
# ifndef OSMINOR
#  define OSMINOR "OS=UNKNOWN"
# endif
/* All the UNIX includes */
# include <sys/types.h>
# include <sys/stat.h>
# include <sys/file.h>
# include <fcntl.h>
# include <stdio.h>
# include <ctype.h>
# include <signal.h>
# include <string.h>
# include <time.h>
# ifndef OS_QNX
#  include <memory.h>
# endif
# include <stdlib.h>
# if !defined(OS_FREEBSD) && !defined(OS_MACOSX)
#  include <malloc.h>
# endif
# include <sys/types.h>
# include <sys/wait.h>
# include <unistd.h>
#endif


/* OSPLAT definitions - suppressed when it's a one-of-a-kind */
#if defined(_M_PPC) || \
    defined(PPC) || \
    defined(ppc) || \
    defined(__powerpc__) || \
    defined(__POWERPC__) || \
    defined(__ppc__)
# define OSPLAT "OSPLAT=PPC"
#endif

#if defined(_i386_) || \
    defined(__i386__) || \
    defined(_M_IX86)
# if !defined(OS_FREEBSD)
#  define OSPLAT "OSPLAT=X86"
# endif
#endif

#if defined(__x86_64__)
# ifdef OSPLAT
#  undef OSPLAT
# endif
# define OSPLAT "OSPLAT=X86_64"
#endif

#ifdef __sparc__
# define OSPLAT "OSPLAT=SPARC"
#endif

#ifdef __mips__
# define OSPLAT "OSPLAT=MIPS"
#endif

#ifdef __arm__
# define OSPLAT "OSPLAT=ARM"
#endif

#if defined(__ia64__) || defined(__IA64__) || defined(_M_IA64)
# define OSPLAT "OSPLAT=IA64"
#endif

#ifdef __s390__
# define OSPLAT "OSPLAT=390"
#endif

#if defined(__ppc64__)
# ifdef OSPLAT
#  undef OSPLAT
# endif
# define OSPLAT "OSPLAT=PPC64"
#endif

#ifndef OSPLAT
# define OSPLAT ""
#endif


/* Jam implementation misc. */
/* longest 'together' actions' */
#ifndef MAXLINE
# define MAXLINE  (10240)
#endif

#ifndef EXITOK
# define EXITOK   (0)
# define EXITBAD  (1)
#endif

#ifndef SPLITPATH
# define SPLITPATH  ':'
#endif


/* You probably don't need to muck with these. */
#define MAXJPATH  (8192) /* longest filename */

#define MAXJOBS  (64) /* silently enforce -j limit */
#define MAXARGC  (32) /* words in $(JAMSHELL) */


/* Jam private definitions below. */
#define DEBUG_MAX  (16)

struct globs {
  int noexec;
  int jobs;
  int quitquick;
  int newestfirst; /* build newest sources first */
  char debug[DEBUG_MAX];
  FILE *cmdout; /* print cmds, not run them */
//#ifdef OPT_IMPROVED_PROGRESS_EXT
  int updating; /* # targets we are updating */
  void *progress; /* progress data (progress.h) */
//#endif
};

extern struct globs globs;


#define DEBUG_MAKE     (globs.debug[1]) /* -da show actions when executed */
#define DEBUG_MAKEPROG (globs.debug[3]) /* -dm show progress of make0 */

#define DEBUG_EXECCMD  (globs.debug[4]) /* show execcmds()'s work */

#define DEBUG_COMPILE  (globs.debug[5]) /* show rule invocations */

#define DEBUG_HEADER   (globs.debug[6]) /* show result of header scan */
#define DEBUG_BINDSCAN (globs.debug[6]) /* show result of dir scan */
#define DEBUG_SEARCH   (globs.debug[6]) /* show attempts at binding */

#define DEBUG_VARSET   (globs.debug[7]) /* show variable settings */
#define DEBUG_VARGET   (globs.debug[8]) /* show variable fetches */
#define DEBUG_VAREXP   (globs.debug[8]) /* show variable expansions */
#define DEBUG_IF       (globs.debug[8]) /* show 'if' calculations */
#define DEBUG_LISTS    (globs.debug[9]) /* show list manipulation */
#define DEBUG_MEM      (globs.debug[9]) /* show memory use */

#define DEBUG_MAKEQ    (globs.debug[11]) /* -da show even quiet actions */
#define DEBUG_EXEC     (globs.debug[12]) /* -dx show text of actions */
#define DEBUG_DEPENDS  (globs.debug[13]) /* -dd show dependency graph */
#define DEBUG_CAUSES   (globs.debug[14]) /* -dc show dependency graph */
#define DEBUG_SCAN     (globs.debug[15]) /* show scanner tokens */


extern void jambase_unpack (void);


#define JAM_OPT_SEMAPHORE


/******************************************************************************/
#include "re9.h"


typedef struct {
  const char *restr; /* regexp string; DON'T DELETE, DON'T MOVE, DON'T CHANGE! */
  re9_prog_t *re; /* compiled regexp */
  int flags; /* flags for re9_execute() */
  /*REGEXP9_DEBUG_MEMSIZE*/
  int maxmem;
} regexp_t;


/*
 * regexp options:
 *  i: ignore case
 *  u: this is utf-8 string
 *  m: '.' matches newline
 * default mode: non-utf-8 (it can be only reset with /.../u)
 */
extern regexp_t *regexp_compile (const char *str, int flags);
extern int regexp_execute (regexp_t *re, const char *bol, re9_sub_t *mp, int ms);
extern void regexp_free (regexp_t *re);
extern void regexp_done (void);


#endif
