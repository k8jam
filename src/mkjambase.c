/*
 * Copyright 1993, 1995 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * mkjambase.c - turn Jambase into a big C structure
 *
 * Usage: mkjambase jambase.c Jambase ...
 *
 * Results look like this:
 *
 *   char *jambase[] = {
 *   "...\n",
 *   ...
 *   0 };
 *
 * Handles \'s and "'s specially; knows how to delete blank and comment lines.
 */
#include <ctype.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libhapack.h"
#include "libhapack.c"

//#define MKJAMBASE_COMPACT


////////////////////////////////////////////////////////////////////////////////
static void fatal (const char *msg) {
  fprintf(stdout, "FATAL: %s\n", msg);
  exit(1);
}


static char outdata[1024*1024];
static int outdatalen = 0;


////////////////////////////////////////////////////////////////////////////////
static uint8_t *pkdata = NULL;
static size_t pkdataSize = 0;
static size_t pkdataUsed = 0;


static void growPkData (int morebytes) {
  if (morebytes > 0) {
    if (pkdataUsed+morebytes > pkdataSize) {
      pkdataSize = (pkdataUsed+morebytes)+32768;
      pkdata = realloc(pkdata, pkdataSize);
      if (pkdata == NULL) abort();
    }
  }
}


static int bread (void *buf, int buflen, void *udata) {
  static int readpos = 0;
  int res = 0;
  uint8_t *ob = (uint8_t *)buf;
  while (buflen > 0) {
    if (readpos > outdatalen) {
      fprintf(stderr, "FATAL: internal packer error!\n");
      abort();
    }
    int left = outdatalen-readpos;
    if (left == 0) break;
    if (left > buflen) left = buflen;
    memcpy(ob, outdata+readpos, left);
    res += left;
    ob += left;
    readpos += left;
    buflen -= left;
  }
  return res;
}


static int bwrite (const void *buf, int buflen, void *udata) {
  if (buflen > 0) {
    growPkData(buflen);
    memcpy(pkdata+pkdataUsed, buf, buflen);
    pkdataUsed += buflen;
  }
  return buflen;
}


////////////////////////////////////////////////////////////////////////////////
static void outStr (const char *str) {
  memmove(outdata+outdatalen, str, strlen(str));
  outdatalen += strlen(str);
}


#define EMIT(ch) do { \
  if (outp-outbuf >= (int)sizeof(outbuf)) fatal("output line too big\n");\
  *(outp++) = (ch); \
} while (0)


static inline void normSlashes (char *s) {
  for (; *s; ++s) if (*s == '\\') *s = '/';
}


static int doDotC = 0, wasScreen, dontStrip = 0, dropSpaces, doCompress = 1;

static int lineno = 0;
static int inclevel = 0;
static const char *srcfname = "<cli>";

static void processFile (FILE *fout, const char *fname) {
  FILE *fin;
  char *p, *e, quoteCh, *outp;
  static char buf[32768], outbuf[32768];
  int olno = lineno;
  const char *ofn = srcfname;
  srcfname = fname;
  lineno = 0;
  if (++inclevel > 64) {
    fclose(fout);
    fprintf(stderr, "FATAL: too many nested includes, failed in file '%s', line %d\n", ofn, olno);
    exit(1);
  }
  printf(": %s\n", fname);
  if (!(fin = fopen(fname, "r"))) {
    fclose(fout);
    fprintf(stderr, "FATAL: can't open file '%s', failed in file '%s', line %d\n", fname, ofn, olno);
    exit(1);
  }
  if (doDotC) {
    if (!doCompress) fprintf(fout, "/* %s */\n", fname);
  } else {
    outStr("### ");
    outStr(fname);
    outStr(" ###\n");
  }
  while (fgets(buf, sizeof(buf), fin)) {
    ++lineno;
    if (buf[0] == '.') {
      /* include */
      char *fn, *t;
      p = buf+1;
      while (*p && *((unsigned char *)p) <= ' ') ++p;
      if ((t = strchr(p, '#')) != NULL) *t = '\0';
      for (t = p+strlen(p); t > p; --t) if (!isspace(t[-1])) break;
      *t = '\0';
      if (!p[0]) {
        fclose(fout);
        fprintf(stderr, "FATAL: invalid '.' in file '%s', line %d\n", fname, lineno);
        exit(1);
      }
      fn = malloc(strlen(p)+strlen(fname)+64);
      if (!fn) {
        fclose(fout);
        fprintf(stderr, "FATAL: out of memory in file '%s', line %d\n", fname, lineno);
        exit(1);
      }
      strcpy(fn, fname);
      normSlashes(fn);
      if ((t = strrchr(fn, '/')) != NULL) t[1] = '\0'; else fn[0] = '\0';
      strcat(fn, p);
      processFile(fout, fn);
      free(fn);
      continue;
    }
    if (doDotC) {
#ifdef MKJAMBASE_COMPACT
      if (!strncmp(buf, "#DONT_TOUCH", 11)) {
        dontStrip = !dontStrip;
        continue;
      }
#else
      dontStrip = 1;
#endif
      p = buf;
      /* strip leading whitespace */
      if (!dontStrip) {
        while (*p && *((unsigned char *)p) <= ' ') ++p;
        /* drop comments and empty lines */
        if (*p == '#' || !*p) continue;
      }
      /* copy; drop comments if # is not in quotes */
      outp = outbuf;
      quoteCh = 0;
      wasScreen = 0;
      if (!doCompress) EMIT('"');
      dropSpaces = 0;
      for (; *p && *p != '\n' && *p != '\r'; ++p) {
        if (!dontStrip) {
          if (!quoteCh && !wasScreen && *p == '#') break; /* comment follows; drop it */
        }
        switch (*p) {
          case '\\':
            if (!doCompress) EMIT('\\');
            EMIT('\\');
            if (quoteCh != '\x27') wasScreen = !wasScreen;
            dropSpaces = 0;
            break;
          case '"':
            if (!doCompress) EMIT('\\');
            EMIT('"');
            if (!wasScreen) quoteCh = (quoteCh == *p ? 0 : *p);
            dropSpaces = 0;
            break;
          case '\x27': /* ' */
            EMIT('\x27');
            if (!wasScreen) quoteCh = (quoteCh == *p ? 0 : *p);
            dropSpaces = 0;
            break;
          default:
            if (!dontStrip && *((unsigned char *)p) <= ' ') {
              if (wasScreen || !dropSpaces || quoteCh) EMIT(*p);
              dropSpaces = !quoteCh;
            } else {
              EMIT(*p);
              dropSpaces = 0;
            }
            wasScreen = 0;
            break;
        }
      }
      /* terminate output */
      *outp = '\0';
      if (!dontStrip) {
        /* strip ending whitespace */
        e = outp-1;
        while (e >= outbuf && *((unsigned char *)e) <= ' ') --e;
        *(++e) = '\0';
        /* drop empty line */
        if (!outbuf[0]) continue;
      }
      if (doCompress) {
        outStr(outbuf);
        outStr("\n");
      } else {
        fprintf(fout, "%s\\n\",\n", outbuf);
      }
    } else {
      fprintf(fout, "%s", buf);
      //outStr(buf);
    }
  }
  fclose(fin);
  --inclevel;
  srcfname = ofn;
  lineno = olno;
}


int main (int argc, char *argv[]) {
  FILE *fout;
  char *p;

  if (argc < 3) {
    fprintf(stderr, "usage: %s jambase.c Jambase ...\n", argv[0]);
    return 1;
  }

  if (!(fout = fopen(argv[1], "wb"))) {
    perror(argv[1]);
    return 1;
  }

  /* if the file ends in .c generate a C source file */
  if ((p = strrchr(argv[1], '.')) && !strcmp(p, ".c")) ++doDotC;

  /* now process the files */
  argc -= 2, argv += 2;

  if (doDotC) {
    fprintf(fout, "/* Generated by mkjambase from Jambase */\n");
    fprintf(fout, "#include \"jam.h\"\n");
    if (doCompress) {
      fprintf(fout, "char **jambase = 0;\n");
      fprintf(fout, "unsigned char jambasepk[] = {");
    } else {
      fprintf(fout, "const char *jambase[] = {\n");
    }
  }

  for (; argc--; ++argv) processFile(fout, *argv);

  if (outdatalen > 0) {
    if (doCompress) {
      /*
      unsigned char *dest;
      size_t destlen;
      if (lzCompress(outdata, outdatalen, &dest, &destlen) != 0) {
        perror("compression error!");
        return 1;
      }
      */
      static const libha_io_t haio = {
        .bread = bread,
        .bwrite = bwrite,
      };
      {
        uint32_t n = outdatalen;
        bwrite(&n, 4, NULL);
      }
      libha_t ha = libha_alloc(&haio, NULL);
      if (libha_pack(ha) != 0) {
        perror("can't pack!");
        return 1;
      }
      libha_free(ha);
      unsigned char *dest = pkdata;
      size_t destlen = pkdataUsed;
      printf("%d bytes packed to %u bytes (%u%%)\n", outdatalen, (unsigned int)destlen, (unsigned int)(100*destlen/outdatalen));
      //fwrite(dest, destlen, 1, fout);
      int cnt = 0;
      for (size_t f = 0; f < destlen; ++f) {
        if (cnt <= 0) {
          fputc('\n', fout);
          cnt = 16;
        }
        --cnt;
        fprintf(fout, "0x%02x,", dest[f]);
      }
      if (cnt > 0) fputc('\n', fout);
    } else {
      perror("wtf?!");
      return 1;
      //fwrite(outdata, outdatalen, 1, fout);
    }
  }

  if (doDotC) {
    if (!doCompress) fputc('0', fout);
    fprintf(fout, "};\n");
    if (doCompress) fprintf(fout, "\nJAMFA_CONST int jbpksize (void) { return sizeof(jambasepk); }\n");
  }

  fclose(fout);
  return 0;
}
