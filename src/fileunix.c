/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * fileunix.c - manipulate file names and scan directories on UNIX/AmigaOS
 *
 * External routines:
 *
 *  file_dirscan() - scan a directory for files
 *  file_time() - get timestamp of file, if not done by file_dirscan()
 *  file_archscan() - scan an archive for files
 *
 * File_dirscan() and file_archscan() call back a caller provided function
 * for each file found.  A flag to this callback function lets file_dirscan()
 * and file_archscan() indicate that a timestamp is being provided with the
 * file.   If file_dirscan() or file_archscan() do not provide the file's
 * timestamp, interested parties may later call file_time().
 */
#include <unistd.h>

#include "jam.h"
#include "filesys.h"
#include "pathsys.h"

#ifdef USE_FILEUNIX

#if defined(OS_MACOSX)
/* need unistd for rhapsody's proper lseek */
# include <sys/dir.h>
# include <unistd.h>
# define STRUCT_DIRENT struct direct
#else
# include <dirent.h>
# define STRUCT_DIRENT struct dirent
#endif

#include <ar.h>


// <0: error; 0: regular; 1: directory
int file_type (const char *diskname) {
  struct stat stbuf;
  if (stat(diskname, &stbuf) != 0) return -1;
  if (S_ISDIR(stbuf.st_mode)) return 1;
  if (S_ISREG(stbuf.st_mode)) return 0;
  return -1;
}


/*
 * file_dirscan() - scan a directory for files
 */
void file_dirscan (const char *dir, scanback func, void *closure) {
  PATHNAME f;
  DIR *d;
  STRUCT_DIRENT *dirent;
  static char filename[MAXJPATH];
  /* first enter directory itself */
  memset((char *)&f, '\0', sizeof(f));
  f.f_dir.ptr = dir;
  f.f_dir.len = strlen(dir);
  dir = (*dir ? dir : ".");
  /* special case / : enter it */
  if (f.f_dir.len == 1 && f.f_dir.ptr[0] == '/') (*func)(closure, dir, 0 /* not stat()'ed */, (time_t)0);
  /* Now enter contents of directory */
  if (!(d = opendir(dir))) return;
  if (DEBUG_BINDSCAN) printf("scan directory %s\n", dir);
  while ((dirent = readdir(d))) {
    f.f_base.ptr = dirent->d_name;
    f.f_base.len = strlen(f.f_base.ptr);
    path_build(filename, &f);
    (*func)(closure, filename, 0 /* not stat()'ed */, (time_t)0);
  }
  closedir(d);
}


/*
 * file_time() - get timestamp of file, if not done by file_dirscan()
 */
int file_time (const char *filename, time_t *time) {
  struct stat statbuf;
  if (stat(filename, &statbuf) < 0) return -1;
  *time = statbuf.st_mtime;
  return 0;
}


static long scan_long (const char *str, size_t ssize) {
  long res = 0;
  while (ssize > 0 && str[0] && (str[0] < '0' || str[0] > '9')) { ++str; --ssize; }
  if (ssize > 0 && str[0]) {
    while (ssize > 0 && str[0] >= '0' && str[0] <= '9') {
      res = res*10+str[0]-'0';
      ++str;
      --ssize;
    }
  }
  return res;
}


/*
 * file_archscan() - scan an archive for files
 */
#define SARFMAG  2
#define SARHDR   sizeof(struct ar_hdr)

void file_archscan (const char *archive, scanback func, void *closure) {
#ifndef NO_AR
  struct ar_hdr ar_hdr;
  static char buf[MAXJPATH];
  long offset;
  char *string_table = 0;
  int fd;
  if ((fd = open(archive, O_RDONLY, 0)) < 0) return;
  if (read(fd, buf, SARMAG) != SARMAG || strncmp(ARMAG, buf, SARMAG)) {
    close(fd);
    return;
  }
  offset = SARMAG;
  if (DEBUG_BINDSCAN) printf("scan archive %s\n", archive);
#ifdef _AIX
  while (read(fd, &ar_hdr, SARHDR) == SARHDR && !memcmp(ar_hdr._ar_name.ar_fmag, ARFMAG, SARFMAG)) {
#else
  while (read(fd, &ar_hdr, SARHDR) == SARHDR && !memcmp(ar_hdr.ar_fmag, ARFMAG, SARFMAG)) {
#endif
    long lar_date;
    long lar_size;
    char lar_name[256];
    char *dst = lar_name;
    /* solaris sscanf() does strlen first, so terminate somewhere */
#ifdef _AIX
	ar_hdr._ar_name.ar_fmag[0] = 0;
#else
    ar_hdr.ar_fmag[0] = 0;
#endif
    /* get date & size */
    /*
    sscanf(ar_hdr.ar_date, "%ld", &lar_date);
    sscanf(ar_hdr.ar_size, "%ld", &lar_size);
    */
    lar_date = scan_long(ar_hdr.ar_date, sizeof(ar_hdr.ar_date));
    lar_size = scan_long(ar_hdr.ar_size, sizeof(ar_hdr.ar_size));
    /* handle solaris string table
     * the entry under the name // is the table, and entries with the name /nnnn refer to the table */
#ifdef _AIX
    if (ar_hdr._ar_name.ar_name[0] != '/') {
#else
    if (ar_hdr.ar_name[0] != '/') {
#endif
      /* traditional archive entry names: ends at the first space, /, or null */
#ifdef _AIX
      char *src = ar_hdr._ar_name.ar_name;
      const char *e = src+sizeof(ar_hdr._ar_name.ar_name);
#else
      char *src = ar_hdr.ar_name;
      const char *e = src+sizeof(ar_hdr.ar_name);
#endif
      //
      while (src < e && *src && *src != ' ' && *src != '/') *dst++ = *src++;
#ifdef _AIX
    } else if (ar_hdr._ar_name.ar_name[1] == '/') {
#else
    } else if (ar_hdr.ar_name[1] == '/') {
#endif
      /* this is the "string table" entry of the symbol table,
       * which holds strings of filenames that are longer than
       * 15 characters (ie. don't fit into a ar_name) */
      string_table = (char *)malloc(lar_size);
      lseek(fd, offset+SARHDR, 0);
      if (read(fd, string_table, lar_size) != lar_size) printf("error reading string table\n");
#ifdef _AIX
    } else if (string_table && ar_hdr._ar_name.ar_name[1] != ' ') {
#else
    } else if (string_table && ar_hdr.ar_name[1] != ' ') {
#endif
      /* long filenames are recognized by "/nnnn" where nnnn is
       * the offset of the string in the string table represented
       * in ASCII decimals */
#ifdef _AIX
      char *src = string_table+atoi(ar_hdr._ar_name.ar_name+1);
#else
      char *src = string_table+atoi(ar_hdr.ar_name+1);
#endif
      while (*src != '/') *dst++ = *src++;
    }
    /* terminate lar_name */
    *dst = 0;
    /* modern (BSD4.4) long names: if the name is "#1/nnnn", then the actual name is the nnnn bytes after the header */
    if (!strcmp(lar_name, "#1")) {
#ifdef _AIX
      int len = atoi(ar_hdr._ar_name.ar_name+3);
#else
      int len = atoi(ar_hdr.ar_name+3);
#endif
      if (read(fd, lar_name, len) != len) printf("error reading archive entry\n");
      lar_name[len] = 0;
    }
    /* build name and pass it on */
    if (lar_name[0]) {
      if (DEBUG_BINDSCAN) printf("archive name %s found\n", lar_name);
      snprintf(buf, sizeof(buf), "%s(%s)", archive, lar_name);
      (*func)(closure, buf, 1 /* time valid */, (time_t)lar_date);
    }
    /* Position at next member */
    offset += SARHDR+((lar_size+1)&(~1));
    lseek(fd, offset, 0);
  }
  if (string_table) free(string_table);
  close(fd);
#endif /* NO_AR */
}


#endif /* USE_FILEUNIX */
