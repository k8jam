/*
 * Copyright 1993, 1995 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * headers.h - handle #includes in source files
 */
#ifndef JAMH_HEADERS_H
#define JAMH_HEADERS_H


extern void headers (TARGET *t);

//#ifdef OPT_HEADER_CACHE_EXT
extern LIST *headers1 (const char *file, LIST *hdrscan);
//#endif


#endif
