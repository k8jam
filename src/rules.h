/*
 * Copyright 1993, 1995 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * rules.h -  targets, rules, and related information
 *
 * This file describes the structures holding the targets, rules, and
 * related information accumulated by interpreting the statements
 * of the jam files.
 *
 * The following are defined:
 *
 *  RULE - a generic jam rule, the product of RULE and ACTIONS
 *  ACTIONS - a chain of ACTIONs
 *  ACTION - a RULE instance with targets and sources
 *  SETTINGS - variables to set when executing a TARGET's ACTIONS
 *  TARGETS - a chain of TARGETs
 *  TARGET - a file or "thing" that can be built
 */
#ifndef JAMH_RULES_H
#define JAMH_RULES_H

#include "jam.h"
#include "hash.h"


typedef struct _rule RULE;
typedef struct _target TARGET;
typedef struct _targets TARGETS;
typedef struct _action ACTION;
typedef struct _actions ACTIONS;
typedef struct _settings SETTINGS ;


/* RULE - a generic jam rule, the product of RULE and ACTIONS */
enum {
  RULE_UPDATED  = 0x01, /* $(>) is updated sources only */
  RULE_TOGETHER = 0x02, /* combine actions on single target */
  RULE_IGNORE   = 0x04, /* ignore return status of executes */
  RULE_QUIETLY  = 0x08, /* don't mention it unless verbose */
  RULE_PIECEMEAL= 0x10, /* split exec so each $(>) is small */
  RULE_EXISTING = 0x20, /* $(>) is pre-exisitng sources only */
  RULE_MAXLINE  = 0x40  /* cmd specific maxline (last) */
};

struct _rule {
  const char *name;
  PARSE *procedure; /* parse tree from RULE */
  const char *actions; /* command string from ACTIONS */
  LIST *bindlist; /* variable to bind for actions */
  LIST *params; /* bind args to local vars */
  int flags; /* modifiers on ACTIONS (RULE_XXX) */
};

/* ACTIONS - a chain of ACTIONs */
struct _actions {
  ACTIONS *next;
  ACTIONS *tail; /* valid only for head */
  ACTION *action;
};

/* ACTION - a RULE instance with targets and sources */
struct _action {
  RULE  *rule;
  TARGETS *targets;
  TARGETS *sources; /* aka $(>) */
  char running; /* has been started */
  char status; /* see TARGET status */
};

/* SETTINGS - variables to set when executing a TARGET's ACTIONS */
struct _settings {
  SETTINGS *next;
  const char *symbol; /* symbol name for var_set() */
  LIST *value; /* symbol value for var_set() */
};

/* TARGETS - a chain of TARGETs */
struct _targets {
  TARGETS *next;
  TARGETS *tail; /* valid only for head */
  TARGET *target;
};

/* TARGET - a file or "thing" that can be built */
enum {
  T_FLAG_TEMP     = 0x01, /* TEMPORARY applied */
  T_FLAG_NOCARE   = 0x02, /* NOCARE applied */
  T_FLAG_NOTFILE  = 0x04, /* NOTFILE applied */
  T_FLAG_TOUCHED  = 0x08, /* ALWAYS applied or -t target */
  T_FLAG_LEAVES   = 0x10, /* LEAVES applied */
  T_FLAG_NOUPDATE = 0x20, /* NOUPDATE applied */
  T_FLAG_INTERNAL = 0x40,  /* internal INCLUDES node */
  //
  T_FLAG_FORCECARE= 0x100
};

enum {
  T_BIND_UNBOUND, /* a disembodied name */
  T_BIND_MISSING, /* couldn't find real file */
  T_BIND_PARENTS, /* using parent's timestamp */
  T_BIND_EXISTS   /* real file, timestamp valid */
};

enum {
  T_FATE_INIT, /* nothing done to target */
  T_FATE_MAKING, /* make0(target) on stack */

  T_FATE_STABLE, /* target didn't need updating */
  T_FATE_NEWER, /* target newer than parent */

  T_FATE_SPOIL, /* >= SPOIL rebuilds parents */
  T_FATE_ISTMP=T_FATE_SPOIL, /* unneeded temp target oddly present */

  T_FATE_BUILD, /* >= BUILD rebuilds target */
  T_FATE_TOUCHED = T_FATE_BUILD, /* manually touched with -t */
  T_FATE_MISSING, /* is missing, needs updating */
  T_FATE_NEEDTMP, /* missing temp that must be rebuild */
  T_FATE_OUTDATED, /* is out of date, needs updating */
  T_FATE_UPDATE, /* deps updated, needs updating */

  T_FATE_BROKEN, /* >= BROKEN ruins parents */
  T_FATE_CANTFIND = T_FATE_BROKEN, /* no rules to make missing target */
  T_FATE_CANTMAKE /* can't find dependents */
};

enum {
  T_MAKE_INIT, /* make1(target) not yet called */
  T_MAKE_ONSTACK, /* make1(target) on stack */
  T_MAKE_ACTIVE, /* make1(target) in make1b() */
  T_MAKE_RUNNING, /* make1(target) running commands */
  T_MAKE_DONE /* make1(target) done */
#ifdef JAM_OPT_SEMAPHORE
  ,T_MAKE_SEMAPHORE /* special target type for semaphores */
#endif
};

struct _target {
  const char *name;
  const char *boundname; /* if search() relocates target */
  ACTIONS *actions; /* rules to execute, if any */
  SETTINGS *settings; /* variables to define */

  char flags; /* status info (T_FLAG_XXX) */

  char binding; /* how target relates to real file (T_BIND_XXX) */

  TARGETS *depends; /* dependencies */
  TARGET *includes; /* includes */

  time_t time; /* update time */
  time_t leaf; /* update time of leaf sources */
  char fate; /* make0()'s diagnosis (T_FATE_XXX) */

  char progress;   /* tracks make1() progress (T_MAKE_XXX)*/

#ifdef JAM_OPT_SEMAPHORE
  TARGET *semaphore; /* used in serialization */
#endif

  char status;     /* execcmd() result */

  int asynccnt;   /* child deps outstanding */
  TARGETS *parents;   /* used by make1() for completion */
  char *cmds;      /* type-punned command list */
};


extern int haverule (const char *rulename);
extern RULE *findrule (const char *rulename); /* can return NULL */
extern RULE *bindrule (const char *rulename);
extern TARGET *bindtarget (const char *targetname);
extern TARGET *copytarget (const TARGET *t);
extern void touchtarget (const char *t);
extern TARGETS *targetlist (TARGETS *chain, LIST  *targets);
extern TARGETS *targetentry (TARGETS *chain, TARGET *target);
extern TARGETS *targetchain (TARGETS *chain, TARGETS *targets);
extern ACTIONS *actionlist (ACTIONS *chain, ACTION *action);
extern SETTINGS *addsettings (SETTINGS *v, int setflag, const char *sym, LIST *val);
extern SETTINGS *copysettings (SETTINGS *v);
extern void pushsettings (SETTINGS *v);
extern void popsettings (SETTINGS *v);
extern void freesettings (SETTINGS *v);
extern void donerules (void);

extern int iteraterules (hash_iterator_cb itercb, void *udata);


#endif
