/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * This file has been donated to Jam.
 */
int optShowHCacheStats = 0;
int optShowHCacheInfo = 0;

//#ifdef OPT_HEADER_CACHE_EXT
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "jam.h"
#include "lists.h"
#include "parse.h"
#include "rules.h"
#include "re9.h"
#include "headers.h"
#include "newstr.h"
#include "hash.h"
#include "hcache.h"
#include "variable.h"
#include "search.h"
#include "pathsys.h"


/*
 * Craig W. McPheeters, Alias|Wavefront.
 *
 * hcache.c hcache.h - handle cacheing of #includes in source files
 *
 * Create a cache of files scanned for headers.  When starting jam,
 * look for the cache file and load it if present.  When finished the
 * binding phase, create a new header cache.  The cache contains
 * files, their timestamps and the header files found in their scan.
 * During the binding phase of jam, look in the header cache first for
 * the headers contained in a file.  If the cache is present and
 * valid, use its contents.  This results in dramatic speedups with
 * large projects (eg. 3min -> 1min startup for one project.)
 *
 * External routines:
 *    hcache_init() - read and parse the local .jamdeps file.
 *    hcache_done() - write a new .jamdeps file
 *    hcache() - return list of headers on target.  Use cache or do a scan.
 */
typedef struct hcachedata_s {
  const char *boundname;
  time_t time;
  LIST *includes;
  LIST *hdrscan; /* the HDRSCAN value for this target */
  int age; /* if too old, we'll remove it from cache */
  struct hcachedata_s *next;
} hcachedata_t;


static struct hash *hcachehash = NULL;
static hcachedata_t *hcachelist = NULL;

static int queries = 0;
static int hits = 0;
static int hcache_changed = 0;


#define CACHE_FILE_VERSION   "k8jam header cache!"
#define CACHE_RECORD_HEADER  "hdr"
#define CACHE_RECORD_END     "end"


/*
 * Return the name of the header cache file.  May return NULL.
 *
 * The user sets this by setting the HCACHEFILE variable in a Jamfile.
 * We cache the result so the user can't change the cache file during
 * header scanning.
 */
static const char *hcache_file_name (void) {
  static const char *name = NULL;
  if (name == NULL) {
    LIST *hcachevar = var_get("HCACHEFILE");
    if (hcachevar) {
      TARGET *t = bindtarget(hcachevar->string);
      pushsettings(t->settings);
      t->boundname = search(t->name, &t->time);
      popsettings(t->settings);
      if (t->boundname) name = copystr(t->boundname);
    }
  }
  return name;
}


/*
 * Return the maximum age a cache entry can have before it is purged from the cache.
 */
static int hcache_maxage (void) {
  int age = 100;
  LIST *var = var_get("HCACHEMAXAGE");
  if (var && var->string) {
    age = atoi(var->string);
    if (age < 0) age = 0;
  }
  return age;
}


/*
 * read string
 * the returned value is as returned by newstr(), so it need not be freed
 */
static const char *read_str (FILE *fl) {
  uint16_t sz;
  static char buf[33000];
  if (fread(&sz, sizeof(sz), 1, fl) != 1) return NULL;
  if (/*sz < 0 ||*/ sz > 32700) return NULL;
  if (sz > 0) {
    if (fread(buf, sz, 1, fl) != 1) return NULL;
  }
  buf[sz] = 0;
  return newstr(buf);
}


/*
 * write string
 */
static int write_str (FILE *fl, const char *s) {
  size_t sz;
  uint16_t sx;
  if (s == NULL) s = "";
  sz = strlen(s);
  if (sz > 32700) return -1;
  sx = (uint16_t)sz;
  if (fwrite(&sx, sizeof(sx), 1, fl) != 1) return -1;
  if (sz > 0 && fwrite(s, sz, 1, fl) != 1) return -1;
  return 0;
}


void hcache_init (void) {
  hcachedata_t cachedata, *c;
  FILE *fl;
  const char *version, *hcachename;
  int header_count = 0;
  hcachehash = hashinit(sizeof(hcachedata_t), "hcache");
  if (optShowHCacheInfo) printf("hcache_init: fn=[%s]\n", hcache_file_name());
  if (!(hcachename = hcache_file_name())) return;
  if (!(fl = fopen(hcachename, "rb"))) return;
  version = read_str(fl);
  if (!version || strcmp(version, CACHE_FILE_VERSION)) { fclose(fl); return; }
  if (DEBUG_HEADER || optShowHCacheInfo) printf("HCACHE: reading cache from '%s'\n", hcachename);
  for (;;) {
    const char *record_type;
    int i, count;
    LIST *l;
    record_type = read_str(fl);
    if (!record_type) goto bail;
    if (!strcmp(record_type, CACHE_RECORD_END)) break;
    if (strcmp(record_type, CACHE_RECORD_HEADER)) { printf("invalid %s with record separator <%s>\n", hcachename, record_type ? record_type : "<null>"); goto bail; }
    c = &cachedata;
    c->boundname = read_str(fl);
    if (!c->boundname) goto bail;
    if (fread(&c->time, sizeof(c->time), 1, fl) != 1) goto bail;
    if (fread(&c->age, sizeof(c->age), 1, fl) != 1) goto bail;
    if (fread(&count, sizeof(count), 1, fl) != 1) goto bail;
    for (l = 0, i = 0; i < count; ++i) {
      const char *s = read_str(fl);
      if (!s) goto bail;
      l = list_new(l, s, 1);
    }
    c->includes = l;
    if (fread(&count, sizeof(count), 1, fl) != 1) { list_free(c->includes); goto bail; }
    for (l = 0, i = 0; i < count; ++i) {
      const char *s = read_str(fl);
      if (!s) goto bail;
      l = list_new(l, s, 1);
    }
    c->hdrscan = l;
    if (!hashenter(hcachehash, (HASHDATA **)&c)) {
      printf("can't insert header cache item, bailing on %s\n", hcachename);
      goto bail;
    }
    c->next = hcachelist;
    hcachelist = c;
    ++header_count;
  }
  if (DEBUG_HEADER) printf("HCACHE: hcache read from file %s\n", hcachename);
  fclose(fl);
  return;
bail:
  fclose(fl);
  printf("HCACHE: invalid cache file: '%s'\n", hcachename);
}


void hcache_done (void) {
  FILE *fl;
  hcachedata_t *c;
  int header_count = 0;
  const char *hcachename;
  int maxage;
  if (optShowHCacheInfo) printf("hcache_done()\n");
  if (!hcachehash) return;
  maxage = hcache_maxage();
#if 0
  /* this check is not necessary */
  if (!hcache_changed && maxage > 0) {
    /* check if we have some out-of-date items */
    for (c = hcachelist; c != NULL; c = c->next) if (c->age > maxage) { hcache_changed = 1; break; }
  }
#endif
  if (!hcache_changed) return; /* nothing was changed, no need to save cache */
  if (optShowHCacheInfo) printf("hcache_done: fn=[%s]\n", hcache_file_name());
  if (!(hcachename = hcache_file_name())) return;
  if (!(fl = fopen(hcachename, "wb"))) return;
  if (optShowHCacheInfo) printf("hcache_done: saving cache\n");
  /* print out the version */
  if (write_str(fl, CACHE_FILE_VERSION)) goto bail;
  //c = hcachelist;
  for (c = hcachelist; c != NULL; c = c->next) {
    LIST *l;
    int count;
    if (maxage == 0) c->age = 0;
    else if (c->age > maxage) continue;
    if (write_str(fl, CACHE_RECORD_HEADER)) goto bail;
    if (write_str(fl, c->boundname)) goto bail;
    if (fwrite(&c->time, sizeof(c->time), 1, fl) != 1) goto bail;
    if (fwrite(&c->age, sizeof(c->age), 1, fl) != 1) goto bail;
    count = list_length(c->includes);
    if (fwrite(&count, sizeof(count), 1, fl) != 1) goto bail;
    for (l = c->includes; l; l = list_next(l)) {
      if (write_str(fl, l->string)) goto bail;
    }
    count = list_length(c->hdrscan);
    if (fwrite(&count, sizeof(count), 1, fl) != 1) goto bail;
    for (l = c->hdrscan; l; l = list_next(l)) {
      if (write_str(fl, l->string)) goto bail;
    }
    ++header_count;
  }
  if (write_str(fl, CACHE_RECORD_END)) goto bail;
  fclose(fl);
  if (DEBUG_HEADER || optShowHCacheStats) printf("HCACHE: cache written to '%s'; %d dependencies, %.0f%% hit rate\n", hcachename, header_count, queries?100.0*hits/queries:0);
  return;
bail:
  fclose(fl);
  unlink(hcachename);
  printf("HCACHE: can't write cache file: '%s'\n", hcachename);
}


LIST *hcache (TARGET *t, LIST *hdrscan) {
  hcachedata_t cachedata, *c = &cachedata;
  LIST *l = 0;
  static char _normalizedPath[PATH_MAX]; /* hcache() can't be called recursive, so don't put this on stack */
  char *normalizedPath = normalize_path(t->boundname, _normalizedPath, sizeof(_normalizedPath), NULL);
  ++queries;
  c->boundname = (normalizedPath != NULL ? normalizedPath : t->boundname);
  if (hashcheck(hcachehash, (HASHDATA **)&c)) {
    if (c->time == t->time) {
      LIST *l1 = hdrscan, *l2 = c->hdrscan;
      while (l1 && l2) {
        if (l1->string != l2->string) {
          l1 = NULL;
        } else {
          l1 = list_next(l1);
          l2 = list_next(l2);
        }
      }
      if (l1 || l2) {
        if (DEBUG_HEADER) printf("HCACHE: HDRSCAN out of date in cache for %s\n", t->boundname);
        /*
        printf("HDRSCAN out of date for %s\n", t->boundname);
        printf(" real  : ");
        list_print_ex(stdout, hdrscan, LPFLAG_NO_TRSPACE);
        printf("\n cached: ");
        list_print_ex(stdout, c->hdrscan, LPFLAG_NO_TRSPACE);
        printf("\n");
        */
        list_free(c->includes);
        list_free(c->hdrscan);
        c->includes = 0;
        c->hdrscan = 0;
      } else {
        if (DEBUG_HEADER || optShowHCacheInfo) printf("HCACHE: using header cache for %s\n", t->boundname);
        c->age = 0;
        ++hits;
        l = list_copy(0, c->includes);
        return l;
      }
    } else {
      if (DEBUG_HEADER || optShowHCacheInfo) printf("HCACHE: header cache out of date for %s\n", t->boundname);
      list_free(c->includes);
      list_free(c->hdrscan);
      c->includes = 0;
      c->hdrscan = 0;
    }
  } else {
    if (hashenter(hcachehash, (HASHDATA **)&c)) {
      c->boundname = newstr(c->boundname);
      c->next = hcachelist;
      hcachelist = c;
    }
  }
  /* 'c' points at the cache entry; its out of date */
  hcache_changed = 1;
  l = headers1(t->boundname, hdrscan);
  c->time = t->time;
  c->age = 0;
  c->includes = list_copy(0, l);
  c->hdrscan = list_copy(0, hdrscan);
  return l;
}


//#endif
