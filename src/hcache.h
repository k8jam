/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * hcache.h - handle #includes in source files
 */
#ifndef JAMH_HCACHE_H
#define JAMH_HCACHE_H


extern int optShowHCacheStats;
extern int optShowHCacheInfo;


extern void hcache_init (void);
extern void hcache_done (void);
extern LIST *hcache (TARGET *t, LIST *hdrscan);


#endif
