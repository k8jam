/***********************************************************************
 * This file is part of HA, a general purpose file archiver.
 * Copyright (C) 1995 Harri Hirvola
 * Modified by Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 ***********************************************************************/
#include <setjmp.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libhapack.h"


/******************************************************************************/
/* some constants                                                             */
/******************************************************************************/
/* ASC */
#define POSCODES   (31200) /* also, dictionary buffer len for SWD */
#define SLCODES    (16)
#define LLCODES    (48)
#define LLLEN      (16)
#define LLBITS     (4)
#define LLMASK     (LLLEN-1)
#define LENCODES   (SLCODES+LLCODES*LLLEN)
#define LTCODES    (SLCODES+LLCODES)
#define CTCODES    (256)
#define PTCODES    (16)
#define LTSTEP     (8)
#define MAXLT      (750*LTSTEP)
#define CTSTEP     (1)
#define MAXCT      (1000*CTSTEP)
#define PTSTEP     (24)
#define MAXPT      (250*PTSTEP)
#define TTSTEP     (40)
#define MAXTT      (150*TTSTEP)
#define TTORD      (4)
#define TTOMASK    (TTORD-1)
#define LCUTOFF    (3*LTSTEP)
#define CCUTOFF    (3*CTSTEP)
#define CPLEN      (8)
#define LPLEN      (4)
#define MINLENLIM  (4096)

/* SWD */
/* Minimum possible match lenght for this implementation */
#define MINLEN   (3)
#define HSIZE    (16384)
#define HSHIFT   (3)
#define HASH(p)  ((swd->b[p]^((swd->b[p+1]^(swd->b[p+2]<<HSHIFT))<<HSHIFT))&(HSIZE-1))
#define MAXCNT   (1024)
/* derived */
#define MAXFLEN  (LENCODES+MINLEN-1)  /* max len to be found  */
#define MAXDLEN  (POSCODES+MAXFLEN)   /* reserved bytes for dict; POSCODES+2*MAXFLEN-1<32768 !!! */


/******************************************************************************/
static int dummy_bread (void *buf, int buf_len, void *udata) { return -1; }
static int dummy_bwrite (const void *buf, int buf_len, void *udata) { return -1; }


static const libha_io_t dummy_iot = {
  .bread = dummy_bread,
  .bwrite = dummy_bwrite,
};


typedef struct {
  libha_io_t *io;
  void *udata;
  /* input buffer */
  uint8_t *bufin;
  int bufin_pos;
  int bufin_max;
  int bufin_size;
  /* output buffer */
  uint8_t *bufout;
  int bufout_pos;
  int bufout_size;
  /* fuckout */
  jmp_buf errJP;
} io_t;


/******************************************************************************/
static inline int get_byte (io_t *io) {
  if (io->bufin_pos >= io->bufin_max) {
    if (io->bufin_pos < io->bufin_size+1) {
      io->bufin_pos = 0;
      io->bufin_max = io->io->bread(io->bufin, io->bufin_size, io->udata);
      if (io->bufin_max < 0) longjmp(io->errJP, LIBHA_ERR_READ);
      if (io->bufin_max == 0) { io->bufin_pos = io->bufin_size+42; return -1; } /* EOF */
    } else {
      return -1; /* EOF */
    }
  }
  return io->bufin[io->bufin_pos++];
}


static inline void put_byte (io_t *io, int c) {
  if (io->bufout_pos >= io->bufout_size) {
    int res = io->io->bwrite(io->bufout, io->bufout_pos, io->udata);
    if (res != io->bufout_pos) longjmp(io->errJP, LIBHA_ERR_WRITE);
    io->bufout_pos = 0;
  }
  io->bufout[io->bufout_pos++] = c&0xff;
}


static inline void flush (io_t *io) {
  if (io->bufout_pos > 0) {
    int res = io->io->bwrite(io->bufout, io->bufout_pos, io->udata);
    if (res != io->bufout_pos) longjmp(io->errJP, LIBHA_ERR_WRITE);
    io->bufout_pos = 0;
  }
}


/******************************************************************************/
typedef struct {
  uint16_t swd_bpos, swd_mlf;
  int16_t swd_char;
  uint16_t cblen, binb;
  uint16_t bbf, bbl, inptr;
  uint16_t ccnt[HSIZE], ll[MAXDLEN], cr[HSIZE], best[MAXDLEN];
  uint8_t b[MAXDLEN+MAXFLEN]; // was:-1
  uint16_t blen, iblen;
  io_t *io;
} swd_t;


static void swd_init (swd_t *swd) {
  memset(swd->ccnt, 0, sizeof(swd->ccnt));
  memset(swd->ll, 0, sizeof(swd->ll));
  memset(swd->cr, 0, sizeof(swd->cr));
  memset(swd->best, 0, sizeof(swd->best));
  memset(swd->b, 0, sizeof(swd->b));
  swd->binb = swd->bbf = swd->bbl = swd->inptr = 0;
  swd->swd_mlf = MINLEN-1;
}


/* return -1 on EOF or 0 */
static int swd_first_bytes (swd_t *swd) {
  while (swd->bbl < MAXFLEN) {
    int b = get_byte(swd->io);
    if (b < 0) return -1;
    swd->b[swd->inptr++] = b;
    ++swd->bbl;
  }
  return 0;
}


static void swd_accept (swd_t *swd) {
  int16_t j = swd->swd_mlf-2;
  /* relies on non changed swd->swd_mlf !!! */
  do {
    int16_t i;
    if (swd->binb == POSCODES) --swd->ccnt[HASH(swd->inptr)]; else ++swd->binb;
    i = HASH(swd->bbf);
    swd->ll[swd->bbf] = swd->cr[i];
    swd->cr[i] = swd->bbf;
    swd->best[swd->bbf] = 30000;
    ++swd->ccnt[i];
    if (++swd->bbf == MAXDLEN) swd->bbf = 0;
    if ((i = get_byte(swd->io)) < 0) {
      --swd->bbl;
      if (++swd->inptr == MAXDLEN) swd->inptr = 0;
      continue;
    }
    if (swd->inptr < MAXFLEN-1) {
      swd->b[swd->inptr+MAXDLEN] = swd->b[swd->inptr] = i;
      ++swd->inptr;
    } else {
      swd->b[swd->inptr] = i;
      if (++swd->inptr == MAXDLEN) swd->inptr = 0;
    }
  } while (--j);
  swd->swd_mlf = MINLEN-1;
}


static void swd_findbest (swd_t *swd) {
  uint16_t ref, ptr, start_len;
  int ch;
  uint16_t i = HASH(swd->bbf);
  uint16_t cnt = swd->ccnt[i];
  ++swd->ccnt[i];
  if (cnt > MAXCNT) cnt = MAXCNT;
  ptr = swd->ll[swd->bbf] = swd->cr[i];
  swd->cr[i] = swd->bbf;
  swd->swd_char = swd->b[swd->bbf];
  if ((start_len = swd->swd_mlf) >= swd->bbl) {
    if (swd->bbl == 0) swd->swd_char = -1;
    swd->best[swd->bbf] = 30000;
  } else {
    for (ref = swd->b[swd->bbf+swd->swd_mlf-1]; cnt--; ptr = swd->ll[ptr]) {
      if (swd->b[ptr+swd->swd_mlf-1] == ref && swd->b[ptr] == swd->b[swd->bbf] && swd->b[ptr+1] == swd->b[swd->bbf+1]) {
        unsigned char *p1 = swd->b+ptr+3, *p2 = swd->b+swd->bbf+3;
        for (i = 3; i < swd->bbl; ++i) if (*p1++ != *p2++) break;
        if (i <= swd->swd_mlf) continue;
        swd->swd_bpos = ptr;
        if ((swd->swd_mlf = i) == swd->bbl || swd->best[ptr] < i) break;
        ref = swd->b[swd->bbf+swd->swd_mlf-1];
      }
    }
    swd->best[swd->bbf] = swd->swd_mlf;
    if (swd->swd_mlf > start_len) {
      if (swd->swd_bpos < swd->bbf) {
        swd->swd_bpos = swd->bbf-swd->swd_bpos-1;
      } else {
        swd->swd_bpos = MAXDLEN-1-swd->swd_bpos+swd->bbf;
      }
    }
  }
  if (swd->binb == POSCODES) --swd->ccnt[HASH(swd->inptr)]; else ++swd->binb;
  if (++swd->bbf == MAXDLEN) swd->bbf = 0;
  if ((ch = get_byte(swd->io)) < 0) {
    --swd->bbl;
    if (++swd->inptr == MAXDLEN) swd->inptr = 0;
    return;
  }
  if (swd->inptr < MAXFLEN-1) {
    swd->b[swd->inptr+MAXDLEN] = swd->b[swd->inptr] = ch;
    ++swd->inptr;
  } else {
    swd->b[swd->inptr] = ch;
    if (++swd->inptr == MAXDLEN) swd->inptr = 0;
  }
}


/******************************************************************************/
typedef struct {
  uint16_t h, l, v;
  int16_t s;
  int16_t gpat, ppat;
  io_t *io;
} ari_t;


/***********************************************************************
  Bit I/O
***********************************************************************/
#define putbit(b)  do { \
  ari->ppat <<= 1; \
  if (b) ari->ppat |= 1; \
  if (ari->ppat&0x100) { \
    put_byte(ari->io, ari->ppat&0xff); \
    ari->ppat = 1; \
  } \
} while (0)


/***********************************************************************
  Arithmetic encoding
***********************************************************************/
static void ac_out (ari_t *ari, uint16_t low, uint16_t high, uint16_t tot) {
  uint32_t r;
  if (tot == 0) longjmp(ari->io->errJP, LIBHA_ERR_BAD_DATA);
  r = (uint32_t)(ari->h-ari->l)+1;
  ari->h = (uint16_t)(r*high/tot-1)+ari->l;
  ari->l += (uint16_t)(r*low/tot);
  if (!((ari->h^ari->l)&0x8000)) {
    putbit(ari->l&0x8000);
    while (ari->s) {
      --ari->s;
      putbit(~ari->l&0x8000);
    }
    ari->l <<= 1;
    ari->h <<= 1;
    ari->h |= 1;
    while (!((ari->h^ari->l)&0x8000)) {
      putbit(ari->l&0x8000);
      ari->l <<= 1;
      ari->h <<= 1;
      ari->h |= 1;
    }
  }
  while ((ari->l&0x4000) && !(ari->h&0x4000)) {
    ++ari->s;
    ari->l <<= 1;
    ari->l &= 0x7fff;
    ari->h <<= 1;
    ari->h |= 0x8001;
  }
}


static void ac_init_encode (ari_t *ari) {
  ari->h = 0xffff;
  ari->l = ari->s = 0;
  ari->ppat = 1;
}


static void ac_end_encode (ari_t *ari) {
  ++ari->s;
  putbit(ari->l&0x4000);
  while (ari->s--) {
    putbit(~ari->l&0x4000);
  }
  if (ari->ppat == 1) {
    flush(ari->io);
    return;
  }
  while (!(ari->ppat&0x100)) ari->ppat <<= 1;
  put_byte(ari->io, ari->ppat&0xff);
  flush(ari->io);
}


/******************************************************************************/
#define BUFIN_SIZE   (64*1024)
#define BUFOUT_SIZE  (64*1024)

struct libha_s {
  uint16_t ltab[2*LTCODES];
  uint16_t eltab[2*LTCODES];
  uint16_t ptab[2*PTCODES];
  uint16_t ctab[2*CTCODES];
  uint16_t ectab[2*CTCODES];
  uint16_t ttab[TTORD][2];
  uint16_t accnt, pmax, npt;
  uint16_t ces;
  uint16_t les;
  uint16_t ttcon;
  swd_t swd;
  ari_t ari;
  libha_io_t iot;
  io_t io;
  uint8_t bufin[BUFIN_SIZE];
  uint8_t bufout[BUFOUT_SIZE];
  void *udata;
};


static void setup_buffers (libha_t asc) {
  asc->io.bufin = asc->bufin;
  asc->io.bufin_size = sizeof(asc->bufin);
  asc->io.bufin_pos = 0;
  asc->io.bufin_max = 0;
  asc->io.bufout = asc->bufout;
  asc->io.bufout_size = sizeof(asc->bufout);
  asc->io.bufout_pos = 0;
}


libha_t libha_alloc (const libha_io_t *iot, void *udata) {
  libha_t res = calloc(1, sizeof(struct libha_s));
  if (res != NULL) {
    res->iot = (iot ? *iot : dummy_iot);
    res->udata = udata;
  }
  return res;
}


void libha_free (libha_t asc) {
  if (asc != NULL) free(asc);
}


const libha_io_t *libha_get_iot (libha_t asc) {
  return (asc != NULL ? &asc->iot : NULL);
}


int libha_set_iot (libha_t asc, const libha_io_t *iot) {
  if (asc != NULL) {
    asc->iot = (iot ? *iot : dummy_iot);
    return 0;
  }
  return -1;
}


void *libha_get_udata (libha_t asc) {
  return (asc != NULL ? asc->udata : NULL);
}


int libha_set_udata (libha_t asc, void *udata) {
  if (asc != NULL) {
    asc->udata = udata;
    return 0;
  }
  return -1;
}


static inline void tabinit (uint16_t t[], uint16_t tl, uint16_t ival) {
  uint16_t i, j;
  for (i = tl; i < 2*tl; ++i) t[i] = ival;
  for (i = tl-1, j = (tl<<1)-2; i; --i, j -= 2) t[i] = t[j]+t[j+1];
}


static inline void tscale (uint16_t t[], uint16_t tl) {
  uint16_t i, j;
  for (i = (tl<<1)-1; i >= tl; --i) if (t[i] > 1) t[i] >>= 1;
  for (i = tl-1, j = (tl<<1)-2; i; --i, j -= 2) t[i] = t[j]+t[j+1];
}


static inline void tupd (uint16_t t[], uint16_t tl, uint16_t maxt, uint16_t step, uint16_t p) {
  int16_t i;
  for (i = p+tl; i; i >>= 1) t[i] += step;
  if (t[1] >= maxt) tscale(t, tl);
}


static inline void tzero (uint16_t t[], uint16_t tl, uint16_t p) {
  int16_t i, step;
  for (i = p+tl, step = t[i]; i; i >>= 1) t[i] -= step;
}


static void model_init (libha_t asc) {
  int16_t i;
  asc->ces = CTSTEP;
  asc->les = LTSTEP;
  asc->accnt = 0;
  asc->ttcon = 0;
  asc->npt = asc->pmax = 1;
  for (i = 0; i < TTORD; ++i) asc->ttab[i][0] = asc->ttab[i][1] = TTSTEP;
  tabinit(asc->ltab, LTCODES, 0);
  tabinit(asc->eltab, LTCODES, 1);
  tabinit(asc->ctab, CTCODES, 0);
  tabinit(asc->ectab, CTCODES, 1);
  tabinit(asc->ptab, PTCODES, 0);
  tupd(asc->ptab, PTCODES, MAXPT, PTSTEP, 0);
}


static void pack_init (libha_t asc) {
  model_init(asc);
  ac_init_encode(&asc->ari);
}


static inline void ttscale (libha_t asc, uint16_t con) {
  asc->ttab[con][0] >>= 1;
  if (asc->ttab[con][0] == 0) asc->ttab[con][0] = 1;
  asc->ttab[con][1] >>= 1;
  if (asc->ttab[con][1] == 0) asc->ttab[con][1] = 1;
}


static void codepair (libha_t asc, int16_t l, int16_t p) {
  uint16_t i, j, lt, k, cf, tot;
  i = asc->ttab[asc->ttcon][0]+asc->ttab[asc->ttcon][1];
  ac_out(&asc->ari, asc->ttab[asc->ttcon][0], i, i+1); // writes
  asc->ttab[asc->ttcon][1] += TTSTEP;
  if (i >= MAXTT) ttscale(asc, asc->ttcon);
  asc->ttcon = ((asc->ttcon<<1)|1)&TTOMASK;
  while (asc->accnt > asc->pmax) {
    tupd(asc->ptab, PTCODES, MAXPT, PTSTEP, asc->npt++);
    asc->pmax <<= 1;
  }
  for (i = p, j = 0; i; ++j, i >>= 1) ;
  cf = asc->ptab[PTCODES+j];
  tot = asc->ptab[1];
  for (lt = 0, i = PTCODES+j; i; i >>= 1) {
    if (i&1) lt += asc->ptab[i-1];
    asc->ptab[i] += PTSTEP;
  }
  if (asc->ptab[1] >= MAXPT) tscale(asc->ptab, PTCODES);
  ac_out(&asc->ari, lt, lt+cf, tot); // writes
  if (p > 1) {
    for (i = 0x8000U; !(p&i); i >>= 1) ;
    j = p&~i;
    if (i != (asc->pmax>>1)) {
      ac_out(&asc->ari, j, j+1, i); // writes
    } else {
      ac_out(&asc->ari, j, j+1, asc->accnt-(asc->pmax>>1)); // writes
    }
  }
  i = l-MINLEN;
  if (i == LENCODES-1) {
    i = SLCODES-1, j = 0xffff;
  } else if (i < SLCODES-1) {
    j = 0xffff;
  } else {
    j = (i-SLCODES+1)&LLMASK;
    i = ((i-SLCODES+1)>>LLBITS)+SLCODES;
  }
  if ((cf = asc->ltab[LTCODES+i]) == 0) {
    ac_out(&asc->ari, asc->ltab[1], asc->ltab[1]+asc->les, asc->ltab[1]+asc->les); // writes
    for (lt = 0, k = LTCODES+i; k; k >>= 1) {
      if (k&1) lt += asc->eltab[k-1];
      asc->ltab[k] += LTSTEP;
    }
    if (asc->ltab[1] >= MAXLT) tscale(asc->ltab, LTCODES);
    ac_out(&asc->ari, lt, lt+asc->eltab[LTCODES+i], asc->eltab[1]); // writes
    tzero(asc->eltab, LTCODES, i);
    if (asc->eltab[1] != 0) asc->les += LTSTEP; else asc->les = 0;
    for (k = i <= LPLEN ? 0 : i-LPLEN; k < (i+LPLEN >= LTCODES-1 ? LTCODES-1 : i+LPLEN); ++k) {
      if (asc->eltab[LTCODES+k]) tupd(asc->eltab, LTCODES, MAXLT, 1, k);
    }
  } else {
    tot = asc->ltab[1]+asc->les;
    for (lt = 0, k = LTCODES+i; k; k >>= 1) {
      if (k&1) lt += asc->ltab[k-1];
      asc->ltab[k] += LTSTEP;
    }
    if (asc->ltab[1] >= MAXLT) tscale(asc->ltab, LTCODES);
    ac_out(&asc->ari, lt, lt+cf, tot); // writes
  }
  if (asc->ltab[LTCODES+i] == LCUTOFF) asc->les -= (LTSTEP < asc->les ? LTSTEP : asc->les-1);
  if (j != 0xffff) ac_out(&asc->ari, j, j+1, LLLEN); // writes
  if (asc->accnt < POSCODES) {
    asc->accnt += l;
    if (asc->accnt > POSCODES) asc->accnt = POSCODES;
  }
}


static void codechar (libha_t asc, int16_t c) {
  uint16_t i, lt, tot, cf;
  i = asc->ttab[asc->ttcon][0]+asc->ttab[asc->ttcon][1];
  ac_out(&asc->ari, 0, asc->ttab[asc->ttcon][0], i+1); // writes
  asc->ttab[asc->ttcon][0] += TTSTEP;
  if (i >= MAXTT) ttscale(asc, asc->ttcon);
  asc->ttcon = (asc->ttcon<<1)&TTOMASK;
  if ((cf = asc->ctab[CTCODES+c]) == 0) {
    ac_out(&asc->ari, asc->ctab[1], asc->ctab[1]+asc->ces, asc->ctab[1]+asc->ces); // writes
    for (lt = 0, i = CTCODES+c; i; i >>= 1) {
      if (i&1) lt += asc->ectab[i-1];
      asc->ctab[i] += CTSTEP;
    }
    if (asc->ctab[1] >= MAXCT) tscale(asc->ctab, CTCODES);
    ac_out(&asc->ari, lt, lt+asc->ectab[CTCODES+c], asc->ectab[1]); // writes
    tzero(asc->ectab, CTCODES, c);
    if (asc->ectab[1] != 0) asc->ces += CTSTEP; else asc->ces = 0;
    for (i = c <= CPLEN ? 0 : c-CPLEN; i < (c+CPLEN >= CTCODES-1 ? CTCODES-1 : c+CPLEN); ++i) {
      if (asc->ectab[CTCODES+i]) tupd(asc->ectab, CTCODES, MAXCT, 1, i);
    }
  } else {
    tot = asc->ctab[1]+asc->ces;
    for (lt = 0, i = CTCODES+c; i; i >>= 1) {
      if (i&1) lt += asc->ctab[i-1];
      asc->ctab[i] += CTSTEP;
    }
    if (asc->ctab[1] >= MAXCT) tscale(asc->ctab, CTCODES);
    ac_out(&asc->ari, lt, lt+cf, tot); // writes
  }
  if (asc->ctab[CTCODES+c] == CCUTOFF) asc->ces -= (CTSTEP < asc->ces ? CTSTEP : asc->ces-1);
  if (asc->accnt < POSCODES) ++asc->accnt;
}


int libha_pack (libha_t asc) {
  volatile int res;
  int16_t oc;
  uint16_t omlf, obpos;
  asc->io.io = &asc->iot;
  asc->io.udata = asc->udata;
  asc->swd.io = asc->ari.io = &asc->io;
  setup_buffers(asc);
  res = setjmp(asc->io.errJP);
  if (res != 0) return res;
  swd_init(&asc->swd);
  swd_first_bytes(&asc->swd); // reads MAXFLEN bytes
  pack_init(asc);
  //swd_findbest(): reads
  for (swd_findbest(&asc->swd); asc->swd.swd_char >= 0; ) {
    if (asc->swd.swd_mlf > MINLEN || (asc->swd.swd_mlf == MINLEN && asc->swd.swd_bpos < MINLENLIM)) {
      omlf = asc->swd.swd_mlf;
      obpos = asc->swd.swd_bpos;
      oc = asc->swd.swd_char;
      swd_findbest(&asc->swd); // reads
      if (asc->swd.swd_mlf > omlf) {
        codechar(asc, oc);
      } else {
        swd_accept(&asc->swd); // reads
        codepair(asc, omlf, obpos);
        swd_findbest(&asc->swd); // reads
      }
    } else {
      asc->swd.swd_mlf = MINLEN-1;
      codechar(asc, asc->swd.swd_char);
      swd_findbest(&asc->swd); // reads
    }
  }
  ac_out(&asc->ari, asc->ttab[asc->ttcon][0]+asc->ttab[asc->ttcon][1], asc->ttab[asc->ttcon][0]+asc->ttab[asc->ttcon][1]+1, asc->ttab[asc->ttcon][0]+asc->ttab[asc->ttcon][1]+1);
  ac_end_encode(&asc->ari);
  return LIBHA_ERR_OK;
}
