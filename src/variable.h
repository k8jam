/*
 * Copyright 1993, 2000 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * variable.h - handle jam multi-element variables
 */
#ifndef JAMH_VARIABLE_H
#define JAMH_VARIABLE_H

#include "dstrings.h"


/*
 * var_defines() - load a bunch of variable=value settings
 *
 * if variable name ends in PATH, split value at :'s, otherwise, split at blanks
 */
extern void var_defines (const char **e, int dontignore);

/*
 * var_string() - expand a string with variables in it
 *
 * copies in to 'out'; doesn't modify targets & sources
 * 'out' should be initialized
 * returns length of added data or -1
 */
extern int var_string (const char *in, dstring_t *out, LOL *lol, char separator);

/*
 * var_get() - get value of a user defined symbol
 *
 * returns NULL if symbol unset
 */
extern LIST *var_get (const char *symbol);

/*
 * defines for var_set()
 */
enum {
  VAR_SET,    /* override previous value */
  VAR_APPEND, /* append to previous value */
  VAR_REMOVE, /* find and remove all occurences of value */
  VAR_DEFAULT /* set only if no previous value */
};

/*
 * var_set() - set a variable in jam's user defined symbol table
 *
 * 'flag' controls the relationship between new and old values of
 * the variable: SET replaces the old with the new; APPEND appends
 * the new to the old; DEFAULT only uses the new if the variable
 * was previously unset and REMOVE removes all occurences of all
 * items in 'value' list
 *
 * copies symbol, takes ownership of value
 */
extern void var_set (const char *symbol, LIST *value, int flag);

/*
 * var_swap() - swap a variable's value with the given one
 */
extern LIST *var_swap (const char *symbol, LIST *value);

/*
 * var_done() - free variable tables
 */
extern void var_done (void);


#endif
