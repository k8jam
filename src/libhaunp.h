/***********************************************************************
 * This file is part of HA, a general purpose file archiver.
 * Copyright (C) 1995 Harri Hirvola
 * Modified by Ketmar // Invisible Vector
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 ***********************************************************************/
#ifndef LIBHAUNP_H
#define LIBHAUNP_H

/* define this to disable CRC module */
/*#define LIBHAUNP_DISABLE_CRC*/

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************************************/
/* stand-alone unpacker                                                       */
/******************************************************************************/
typedef struct haunp_s *haunp_t;

/* <0: error; 0: EOF; >0: bytes read (can be less that buf_len) */
/* buf_len can never be negative or zero; it will not be more that INT_MAX/2-1 either */
typedef int (*haunp_bread_fn_t) (void *buf, int buf_len, void *udata);


/* return NULL on error (out of memory) */
extern haunp_t haunp_open_io (haunp_bread_fn_t reader, void *udata);
extern haunp_t haunp_open_buf (const void *buf, int buf_len); /* buf MUST be alive until haunp_close() called! */

/* return 0 if ok or -1 on error */
extern int haunp_reset_io (haunp_t hup, haunp_bread_fn_t reader, void *udata);
extern int haunp_reset_buf (haunp_t hup, const void *buf, int buf_len); /* buf MUST be alive until haunp_close() called! */

extern int haunp_close (haunp_t hup);

/* return number of bytes read (<len: end of data) or -1 on error */
extern int haunp_read (haunp_t hup, void *buf, int len);

/******************************************************************************/
/* poly: 0xedb88320L: ISO 3309, ITU-T V.42 */
#ifndef LIBHAUNP_DISABLE_CRC
extern unsigned int haunp_crc32 (const void *src, int len);
extern unsigned int haunp_crc32_begin (void);
extern unsigned int haunp_crc32_end (unsigned int crc);
extern unsigned int haunp_crc32_part (unsigned int crc, const void *src, int len);
#endif


#ifdef __cplusplus
}
#endif
#endif
