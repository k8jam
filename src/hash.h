/*
 * Copyright 1993, 1995 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * hash.h - simple in-memory hashing routines
 */
#ifndef JAMH_HASH_H
#define JAMH_HASH_H


typedef struct hashdata HASHDATA;

extern struct hash *hashinit (int datalen, const char *name);
extern int hashitem (struct hash *hp, HASHDATA **data, int enter);
extern void hashdone (struct hash *hp);

/* return !0 from itercb() to stop; returns result of itercb() */
typedef int (*hash_iterator_cb) (const void *hdata, void *udata);
extern int hashiterate (struct hash *hp, hash_iterator_cb itercb, void *udata);

static inline int hashenter (struct hash *hp, HASHDATA **data) { return !hashitem(hp, data, !0); }
static inline int hashcheck (struct hash *hp, HASHDATA **data) { return hashitem(hp, data, 0); }


#endif
