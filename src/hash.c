/*
 * Copyright 1993, 1995 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * hash.c - simple in-memory hashing routines
 *
 * External routines:
 *
 *     hashinit() - initialize a hash table, returning a handle
 *     hashitem() - find a record in the table, and optionally enter a new one
 *     hashdone() - free a hash table, given its handle
 *
 * Internal routines:
 *
 *     hashrehash() - resize and rebuild hp->tab, the hash table
 */
#include <stddef.h>
#include <stdint.h>

#include "jam.h"
#include "hash.h"


static uint32_t hash_joaat (const void *key, size_t nbytes) {
  uint32_t hash = 0;
  const uint8_t *k = (const uint8_t *)key;
  while (nbytes-- > 0) {
    hash += *k++;
    hash += (hash<<10);
    hash ^= (hash>>6);
  }
  hash += (hash<<3);
  hash ^= (hash>>11);
  hash += (hash<<15);
  return hash;
}


/* header attached to all data items entered into a hash table. */
struct hashhdr {
  struct item *next;
  uint32_t keyval; /* for quick comparisons */
};


/* this structure overlays the one handed to hashenter() */
/* it's actual size is given to hashinit() */
struct hashdata {
  const char *key;
  /* rest of user data */
};


typedef struct item {
  struct hashhdr hdr;
  struct hashdata data;
} ITEM;


#define MAX_LISTS  (32)
struct hash {
  /* the hash table, just an array of item pointers */
  struct {
    int nel;
    ITEM **base;
  } tab;
  int bloat; /* tab.nel / items.nel */
  int inel; /* initial number of elements */
  /* the array of records, maintained by these routines essentially a microallocator */
  struct {
    int more; /* how many more ITEMs fit in lists[ list ] */
    char *next; /* where to put more ITEMs in lists[ list ] */
    int datalen; /* length of records in this hash table */
    int size; /* sizeof(ITEM) + aligned datalen */
    int nel; /* total ITEMs held by all lists[] */
    int list; /* index into lists[] */
    struct {
      int nel; /* total ITEMs held by this list */
      char *base; /* base of ITEMs array */
    } lists[MAX_LISTS];
  } items;
  char *name; /* just for hashstats() */
};


#define ALIGNED(x)  ((x+sizeof(ITEM)-1)&(~(sizeof(ITEM)-1)))


static void hashstat (struct hash *hp) {
  ITEM **tab = hp->tab.base;
  int nel = hp->tab.nel, count = 0, sets = 0;
  int run = (tab[nel-1] != NULL);
  int i, here;
  for (i = nel; i > 0; --i) {
    if ((here = (*tab++ != NULL))) ++count;
    if (here && !run) ++sets;
    run = here;
  }
  printf("%s table: %d+%d+%d (%dK+%dK) items+table+hash, %f density\n",
    hp->name,
    count,
    hp->items.nel,
    hp->tab.nel,
    hp->items.nel*hp->items.size/1024,
    (int)(hp->tab.nel*sizeof(ITEM **)/1024),
    (float)count/(float)sets
  );
}


/*
 * hashrehash() - resize and rebuild hp->tab, the hash table
 */
static void hashrehash (struct hash *hp) {
  int i = ++hp->items.list;
  hp->items.more = (i ? 2*hp->items.nel : hp->inel);
  hp->items.next = malloc(hp->items.more*hp->items.size);
  hp->items.lists[i].nel = hp->items.more;
  hp->items.lists[i].base = hp->items.next;
  hp->items.nel += hp->items.more;
  if (hp->tab.base) free(hp->tab.base);
  hp->tab.nel = hp->items.nel*hp->bloat;
  hp->tab.base = malloc(hp->tab.nel*sizeof(ITEM **));
  memset(hp->tab.base, '\0', hp->tab.nel*sizeof(ITEM *));
  for (i = 0; i < hp->items.list; ++i) {
    int nel = hp->items.lists[i].nel;
    char *next = hp->items.lists[i].base;
    for (; nel--; next += hp->items.size) {
      ITEM *xxi = (ITEM *)next;
      ITEM **ip = hp->tab.base+xxi->hdr.keyval%hp->tab.nel;
      xxi->hdr.next = *ip;
      *ip = xxi;
    }
  }
}


/*
 * hashiterate() - iterate thru all hash entries
 * return !0 from itercb() to stop
 * returns result of itercb() or 0 */
int hashiterate (struct hash *hp, hash_iterator_cb itercb, void *udata) {
  ITEM **tab = hp->tab.base;
  for (int i = hp->tab.nel; i > 0; --i, ++tab) {
    if (*tab != NULL) {
      int res = itercb(&((*tab)->data), udata);
      if (res) return res;
    }
  }
  return 0;
}


/*
 * hashitem() - find a record in the table, and optionally enter a new one
 */
int hashitem (struct hash *hp, HASHDATA **data, int enter) {
  ITEM **base, *i;
  uint32_t keyval;
  if (enter && !hp->items.more) hashrehash(hp);
  if (!enter && !hp->items.nel) return 0;
  keyval = hash_joaat((*data)->key, strlen((*data)->key));
  base = hp->tab.base+(keyval%hp->tab.nel);
  for (i = *base; i != NULL; i = i->hdr.next) {
    if (keyval == i->hdr.keyval && strcmp(i->data.key, (*data)->key) == 0) {
      *data = &i->data;
      return !0;
    }
  }
  if (enter) {
    i = (ITEM *)hp->items.next;
    hp->items.next += hp->items.size;
    --hp->items.more;
    memcpy(&i->data, *data, hp->items.datalen);
    i->hdr.keyval = keyval;
    i->hdr.next = *base;
    *base = i;
    *data = &i->data;
  }
  return 0;
}


/*
 * hashinit() - initialize a hash table, returning a handle
 */
struct hash *hashinit (int datalen, const char *name) {
  struct hash *hp = malloc(sizeof(*hp));
  hp->bloat = 3;
  hp->tab.nel = 0;
  hp->tab.base = NULL;
  hp->items.more = 0;
  hp->items.datalen = datalen;
  hp->items.size = sizeof(struct hashhdr)+ALIGNED(datalen);
  hp->items.list = -1;
  hp->items.nel = 0;
  hp->inel = 11;
  hp->name = strdup(name != NULL ? name : "");
  return hp;
}


/*
 * hashdone() - free a hash table, given its handle
 */
void hashdone (struct hash *hp) {
  if (hp != NULL) {
    if (DEBUG_MEM) hashstat(hp);
    if (hp->tab.base) free((char *)hp->tab.base);
    for (int i = 0; i <= hp->items.list; i++) free(hp->items.lists[i].base);
    free(hp->name);
    free(hp);
  }
}
