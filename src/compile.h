/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * compile.h - compile parsed jam statements
 */
#ifndef JAMH_COMPILE_H
#define JAMH_COMPILE_H

extern void compile_builtins (void);

extern LIST *compile_append (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_break (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_foreach (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_if (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_eval (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_include (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_list (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_local (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_null (PARSE *parse, LOL *args, int *jmp) JAMFA_CONST;
extern LIST *compile_on (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_rule (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_rules (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_set (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_setcomp (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_setexec (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_settings (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_switch (PARSE *parse, LOL *args, int *jmp);
extern LIST *compile_while (PARSE *parse, LOL *args, int *jmp);

extern LIST *evaluate_rule (const char *rulename, LOL *args, LIST *result);


/* conditions for compile_if() */
enum {
  EXPR_NOT, /* ! cond */
  EXPR_AND, /* cond && cond */
  EXPR_OR, /* cond || cond */

  EXPR_EXISTS, /* arg */
  EXPR_EQUALS, /* arg = arg */
  EXPR_NOTEQ, /* arg != arg */
  EXPR_LESS, /* arg < arg  */
  EXPR_LESSEQ, /* arg <= arg */
  EXPR_MORE, /* arg > arg  */
  EXPR_MOREEQ, /* arg >= arg */
  EXPR_IN, /* arg in arg */
  EXPR_ANYIN, /* arg any-in arg */
  EXPR_REXPEQ /* arg ~= regexp */
};


/* Flags for compile_break() */
enum {
  JMP_NONE, /* flow continues */
  JMP_BREAK, /* break out of loop */
  JMP_CONTINUE, /* step to end of loop */
  JMP_RETURN /* return from rule */
};


#endif
