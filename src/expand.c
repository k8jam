/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * expand.c - expand a buffer, given variable values
 *
 * External routines:
 *
 *  var_expand() - variable-expand input string into list of strings
 */
#include <alloca.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include <stdio.h>
#include <unistd.h>

#include "jam.h"
#include "lists.h"
#include "expand.h"
#include "newstr.h"
#include "dstrings.h"

#include "pathsys.h"
#include "variable.h"


/*
 * need_screen() - does the given char need to be screened?
 */
static inline int need_screen (int ch) {
  ch &= 0xff;
  if (!ch) return 0;
  if (ch <= '*' || ch == '`' || ch == 127 ||
      (ch >= ';' && ch <= '<') ||
      (ch >= '>' && ch <= '?') ||
      (ch >= '[' && ch <= ']') ||
      (ch >= '{' && ch <= '}')) return 1;
  return 0;
}


static inline void shell_quote_set (char set[256]) {
  for (int f = 0; f < 256; ++f) set[f] = need_screen(f);
}


static const char *skip_varaccess (const char *s, const char *e);

/*
 * skip_varaccess() - skip variable access
 *
 * 's' points to '$(...)'.
 * return first char after '$(...)' or NULL on error
 */
static const char *skip_varaccess_intr (const char *s, const char *e) {
  int state = 0; /* 0: in varname; 1: in []; 2: in ':' */
  s += (*s == '$' ? 2 : 1); /* skip '(' */
  if (s >= e || s[0] == ')' || s[0] == ':' || s[0] == '[') return NULL; /* bad luck */
  while (s < e) {
    if (s[0] == ')') return s+1; /* that's all folks */
    /* indexing? */
    if (s[0] == '[' && state < 2) {
      if (state == 1 || s+1 >= e) return NULL; /* double indexing is no-no */
      state = 1;
      ++s; /* skip '[' */
      while (s < e) {
        if (s[0] == '$' && s+1 < e && s[1] == '(') {
          if ((s = skip_varaccess(s, e)) == NULL) return NULL;
        } else {
          ++s;
          if (s[-1] == ']') break;
        }
      }
      if (s >= e) return NULL; /* bad luck */
      continue;
    }
    /* postfix modifiers? */
    if (s[0] == ':') {
      state = 2;
      /* skip selectors */
      for (;;) {
        for (++s; s < e && s[0] >= 'A' && s[0] <= 'Z'; ++s) ;
        if (s >= e) return NULL; /* bad luck */
        if (s < e && s[0] == '=') {
          /* assign */
          char qch = 0;
          if (++s >= e) return NULL; /* bad luck */
          /* here we can have quoted string or var access */
          if (*s == '"' || *s == '\'') qch = *s++;
          while (s < e) {
            if (!qch && s[0] == ')') return s+1; /* that's all folks */
            if (qch != '\'' && s[0] == '$' && s+1 < e && s[1] == '(') {
              /* variable access */
              if ((s = skip_varaccess(s, e)) == NULL) return NULL;
              continue;
            }
            if (!qch && s[0] == ':') break; /* another modifier */
            if (qch && s[0] == qch) {
              /* quoted string terminator */
              if (qch == '\'' && s+1 < e && s[1] == '\'') {
                /* "''" */
                s += 2;
              } else {
                ++s; /* skip closing quote */
                qch = 0;
                break;
              }
              continue;
            }
            if (qch != '\'' && s+1 < e && s[0] == '\\') {
              /* quoted char */
              s += 2;
              continue;
            }
            /* ordinary char */
            ++s;
          }
          if (qch) return NULL; /* unclosed quoting */
        }
        if (s >= e || s[0] < 'A' || s[0] > 'Z') break;
        /* we have another selector, do it all again */
      }
      continue;
    }
    /* var access? */
    if (s[0] == '$' && s+1 < e && s[1] == '(') {
      /* this is another var access, do recursive skip */
      if ((s = skip_varaccess(s, e)) == NULL) return NULL; /* something goes wrong along the way */
      continue;
    }
    /* var name */
    if (state != 0) return NULL; /* bad luck: varname char in 'not varname' state */
    ++s;
  }
  /* we should never come here if there were no errors */
  return NULL;
}


static const char *skip_varaccess (const char *s, const char *e) {
  const char *res;
  if (DEBUG_VAREXP) printf(" skip_varaccess: '%.*s'\n", (int)(e-s), s);
  res = skip_varaccess_intr(s, e);
  if (DEBUG_VAREXP) {
    if (res == NULL) printf(" FATAL: invalid var access!\n");
    else printf(" skip_varaccess rest: '%.*s'\n", (int)(e-res), res);
  }
  return res;
}


/* this function used to parse indicies in "[]" */
/* returns either NULL on error or pointer to terminator char (with trailing spaces skipped) */
static const char *parse_number (int *num, const char *s, const char *e, LOL *lol) {
  int neg = 0, n = 0;
  //  if (DEBUG_VAREXP) printf("parse_number: '%.*s'\n", (int)(e-s), s);
  for (; s < e && isspace(*s); ++s) ; /* skip leading spaces */
  if (s < e && s[0] == '-') { neg = 1; ++s; }
  if (s >= e) return NULL; /* bad luck */
  //if (DEBUG_VAREXP) printf(" 1:parse_number: '%.*s'\n", (int)(e-s), s);
  /* this must be either variable access or number */
  if (isdigit(*s)) {
    /* number */
    for (; s < e && isdigit(*s); ++s) n = n*10+s[0]-'0';
    //if (DEBUG_VAREXP) printf("  2:parse_number: '%.*s'\n", (int)(e-s), s);
  } else if (s[0] == '$' && s+1 < e && s[1] == '(') {
    /* variable access */
    LIST *vv;
    const char *ve = skip_varaccess(s, e);
    if (ve == NULL) return NULL; /* something is wrong */
    //if (DEBUG_VAREXP) printf("  3:parse_number: '%.*s'\n", (int)(ve-s), s);
    vv = var_expand(s, ve, lol, 0);
    s = ve;
    if (vv != L0 && vv->string[0]) {
      /* convert expanded value to number */
      /* note that bad number means 'item zero' here, which is always nothing */
      const char *t;
      for (t = vv->string; *t && isspace(*t); ++t) ;
      if (*t == '-') { neg = !neg; ++t; } /* we can have preceding '-', so use '!' */
      for (; *t && isdigit(*t); ++t) n = n*10+t[0]-'0';
      for (; *t && isspace(*t); ++t) ;
      if (*t) n = 0; /* this is not a number, so reset index */
    }
    list_free(vv);
  } else {
    return NULL; /* bad luck, have to have something here */
  }
  for (; s < e && isspace(*s); ++s) ; /* skip trailing spaces */
  if (s >= e) return NULL; /* bad luck */
  //if (DEBUG_VAREXP) printf("  4:parse_number: '%.*s'\n", (int)(e-s), s);
  if (neg) n = -n;
  *num = n;
  return s;
}


static int digit (int c, int base) {
  if (c == EOF) return -1;
  if (c >= 'a' && c <= 'z') c -= 32;
  if (c < '0' || (c > '9' && c < 'A') || c > 'Z') return -1;
  if ((c -= '0') > 9) c -= 7;
  if (c >= base) return -1;
  return c;
}


/*
 * parse_modifier() - parse one modifier
 *
 * 's' can point either to modifier char or to ':'
 * 'rep' can be NULL
 * 'mc' can be NULL, if it is not NULL and '*mc' != 0: expand only this modifier
 * if 'wasrep' is not NULL, set it to !0 if we have '=' here (and store everything after '=' in 'rep')
 * set 'mc' to modifier char
 */
static const char *parse_modifier (const char *s, const char *e, LOL *lol, char *mc, dstring_t *rep, int *wasrep) {
  int mod = (mc != NULL ? *mc : 0);
  if (mc != NULL) *mc = 0;
  if (wasrep != NULL) *wasrep = 0;
  if (rep != NULL) dstr_clear(rep);
  while (s < e && *s == ':') ++s;
  if (s < e) {
    if (mod && *s != mod) rep = NULL; /* don't expand this */
    if (mc != NULL) *mc = *s;
    if (*s != '=') ++s;
    if (s < e && *s == '=') {
      ++s;
      if (wasrep != NULL) *wasrep = 1;
      /* now parse replacement string; note that we can have variables in it */
      /* do it easy way: collect string and pass it to var_expand() */
      /* note that quoting works only for the whole string, not for string parts */
      if (s < e) {
        if (*s == '\'') {
          /* single-quoted string; the easiest thing */
          for (++s; s < e; ++s) {
            if (*s == '\'') {
              ++s;
              if (s >= e || *s != '\'') break;
            }
            dstr_push_char(rep, *s);
          }
        } else {
          int need_expand = 0;
          char qch = (*s == '"' ? '"' : 0);
          if (qch) ++s;
          while (s < e) {
            if (!qch && s[0] == ':') break;
            if (s[0] == '$' && s+1 < e && s[1] == '(') {
              /* variable access */
              const char *ve = skip_varaccess(s, e);
              if (ve == NULL) { printf("FATAL: invalid var access: '%.*s'\n", (int)(e-s), s); exit(42); }
              dstr_push_memrange(rep, s, ve);
              need_expand = 1;
              s = ve;
              continue;
            }
            if (s[0] == '\\') {
              /* screened char */
              if (qch && s+1 < e) {
                int n;
                ++s;
                switch (*s++) {
                  case 'a': dstr_push_char(rep, '\a'); break;
                  case 'b': dstr_push_char(rep, '\b'); break;
                  case 'e': dstr_push_char(rep, '\x1b'); break;
                  case 'f': dstr_push_char(rep, '\f'); break;
                  case 'n': dstr_push_char(rep, '\n'); break;
                  case 'r': dstr_push_char(rep, '\r'); break;
                  case 't': dstr_push_char(rep, '\t'); break;
                  case 'v': dstr_push_char(rep, '\v'); break;
                  case 'x':
                    if (s >= e) { printf("FATAL: invalid hex escape!\n"); exit(42); }
                    n = digit(*s++, 16); /* first digit */
                    if (n < 0) { printf("FATAL: invalid hex escape!\n"); exit(42); }
                    if (s < e) {
                      /* second digit */
                      int d = digit(*s++, 16);
                      if (d < 0) { printf("FATAL: invalid hex escape!\n"); exit(42); }
                      n = n*16+d;
                    }
                    if (n == 0) { printf("FATAL: invalid hex escape!\n"); exit(42); }
                    dstr_push_char(rep, n);
                    break;
                  /*TODO: add '\uXXXX'?*/
                  default:
                    if (isalnum(s[-1])) { printf("FATAL: invalid escape: '%c'!\n", s[-1]); exit(42); }
                    dstr_push_char(rep, s[-1]);
                    break;
                }
              } else {
                if (s+1 < e) ++s;
                dstr_push_char(rep, *s);
                ++s;
              }
              continue;
            }
            /* end of quoted string? */
            if (qch && s[0] == qch) {
              qch = 0;
              ++s;
              break;
            }
            /* normal char */
            dstr_push_char(rep, *s);
            ++s;
          }
          /* now expand collected string */
          if (rep != NULL && need_expand) {
            LIST *l = var_expand(dstr_cstr(rep), NULL, lol, 0);
            dstr_clear(rep);
            if (l != L0) dstr_set_cstr(rep, l->string);
            list_free(l);
          }
        }
      }
    }
  }
  return s;
}


static int find_join (const char *s, const char *e, LOL *lol, dstring_t *rep) {
  while (s < e) {
    char mc = 'J';
    s = parse_modifier(s, e, lol, &mc, rep, NULL);
    if (s == NULL) abort(); /* the thing that should not be */
    if (mc == 'J') {
      if (strcmp(dstr_cstr(rep), "|space|") == 0) dstr_set_cstr(rep, " ");
      else if (strcmp(dstr_cstr(rep), "|tab|") == 0) dstr_set_cstr(rep, "\t");
      return 1; /* got 'join', wow! */
    }
  }
  return 0;
}


static int find_empty (const char *s, const char *e, LOL *lol, dstring_t *rep) {
  while (s < e) {
    char mc = 'E';
    s = parse_modifier(s, e, lol, &mc, rep, NULL);
    if (s == NULL) abort(); /* the thing that should not be */
    if (mc == 'E') return 1; /* got 'empty', wow! */
  }
  return 0;
}


static inline void dstr_cpy (dstring_t *dest, dstring_t *src) { dstr_set_buf(dest, dstr_cstr(src), dstr_len(src)); }


/* returns boolean: success */
/*TODO: check overflow */
static inline int s2i (int *val, const char *s) {
  if (s != NULL) {
    int n = 0, neg = 0;
    while (*s && isspace(*s)) ++s;
    if (*s == '-') { neg = 1; ++s; } else if (*s == '+') ++s;
    for (; *s && isdigit(*s); ++s) n = n*10+s[0]-'0';
    while (*s && isspace(*s)) ++s;
    if (neg) n = -n;
    if (val != NULL) *val = n;
    return (*s == 0);
  } else {
    if (val != NULL) *val = 0;
    return 0;
  }
}


/*FIXME: this is full of hacks using internal dstring structures! add necessary API to dstrings and remove hacks!*/
static void do_mod_cw (dstring_t *str, int was_wc, int marg_w, int marg_c) {
  int len = dstr_len(str);
  if ((was_wc&0x01) && abs(marg_c) < len) {
    /* apply 'C' */
    if (marg_c < 0) {
      /* remove left part */
      marg_c = -marg_c;
      memmove(str->str, str->str+str->len-marg_c, marg_c+1); /* take terminating zero too */
    } /* removing right part don't need any hackery */
    dstr_chop(str, marg_c);
    len = marg_c;
  }
  if ((was_wc&0x02) && abs(marg_w) > len) {
    /* apply 'W' */
    dstr_reserve(str, abs(marg_w)+1); /* reserve room for terminating zero too */
    if (marg_w > 0) {
      /* add trailing spaces */
      memset(str->str+len, ' ', marg_w-len);
    } else {
      /* add leading spaces */
      marg_w = -marg_w;
      memmove(str->str+(marg_w-len), str->str, len+1);
      memset(str->str, ' ', marg_w-len);
    }
    str->len = marg_w;
    str->str[marg_w] = 0; /* add terminating zero */
  }
}


/* modifiers "ULQ" applies after all others */
static void process_selectors (dstring_t *res, const char *s, const char *e, LOL *lol, const char *vval) {
  /* no recursive calls to process_selectors() allowed, so we can make all vars static */
  int doupdown = 0; /* <0: down; >0: up */
  char qchar = 0; /* quoting char; 0: don't quote */
  int was_wc = 0; /* bit 0: have ':C=', bit 1: have ':W=', bit 2: have ':W' */
  int marg_w = 0, marg_c = 0; /* args for ':W' and ':C' */
  int pnp_inited = 0; /* 1: inited, no ':x' (w/o assign) was hit; 2: ':x' (w/o assign) was hit */
  static PATHNAME pnp;
  static dstring_t s_grist, s_root, s_dir, s_base, s_suffix, s_member; /* initialized if pnp_inited != 0 */
  static dstring_t mval;
  static char qset[256]; /* chars to quote; initialized if qchar != 0 */
  dstr_init(&mval);
  while (s < e) {
    char mc = 0;
    int wasass = 0;
    /* some optimizations */
    while (s < e && *s == ':') ++s;
    if (s >= e) break;
    if (s[0] == 'E' || s[0] == 'J') {
      /* skip this ones */
      s = parse_modifier(s, e, lol, NULL, NULL, NULL);
      if (s == NULL) abort(); /* the thing that should not be */
      continue;
    }
    s = parse_modifier(s, e, lol, &mc, &mval, &wasass);
    if (DEBUG_VAREXP) printf("mod: '%c'; len=%d; s=<%s>\n", mc, dstr_len(&mval), dstr_cstr(&mval));
    if (s == NULL) abort(); /* the thing that should not be */
    {
      int mvlen = (wasass ? dstr_len(&mval) : 0);
      const char *mv = (wasass ? dstr_cstr(&mval) : "");
      if (mc == 'U') { doupdown = 1; continue; }
      if (mc == 'L') { doupdown = -1; continue; }
      if (mc == 'Q') {
        qchar = (mvlen > 0 ? mv[0] : '\\');
        if (mvlen < 2) {
          shell_quote_set(qset);
        } else {
          /* first char is quoting char, others are chars to be quoted */
          memset(qset, 0, 256);
          for (++mv; *mv; ++mv) qset[(unsigned char)(mv[0])] = 1;
        }
        continue;
      }
      if (mc == 'W') {
        if (wasass) {
          if (s2i(&marg_w, mv)) was_wc |= 0x02; else was_wc &= ~0x02;
        } else {
          was_wc |= 0x04;
        }
        continue;
      }
      if (mc == 'C') {
        if (wasass) {
          if (s2i(&marg_c, mv)) was_wc |= 0x01; else was_wc &= ~0x01;
        }
        continue;
      }
      if (strchr("GRDBSM", mc) == NULL) { printf("FATAL: invalid selector: '%c'\n", mc); exit(42); }
      /* parse value if it is necessary */
      if (!pnp_inited && strchr("GRDBSM", mc) != NULL) {
        pnp_inited = 1;
        path_parse(vval, &pnp);
        dstr_init_buf(&s_grist, pnp.f_grist.ptr, pnp.f_grist.len);
        dstr_init_buf(&s_root, pnp.f_root.ptr, pnp.f_root.len);
        dstr_init_buf(&s_dir, pnp.f_dir.ptr, pnp.f_dir.len);
        dstr_init_buf(&s_base, pnp.f_base.ptr, pnp.f_base.len);
        dstr_init_buf(&s_suffix, pnp.f_suffix.ptr, pnp.f_suffix.len);
        dstr_init_buf(&s_member, pnp.f_member.ptr, pnp.f_member.len);
      }
      /* first ':x' (w/o assign) hit -- clear all parts */
      if (!wasass && pnp_inited == 1) {
        pnp_inited = 2;
        pnp.f_grist.len = pnp.f_root.len = pnp.f_dir.len = pnp.f_base.len = pnp.f_suffix.len = pnp.f_member.len = 0;
      }
      switch (mc) {
        case 'G':
          pnp.f_grist.len = 1;
          if (wasass) dstr_cpy(&s_grist, &mval);
          break;
        case 'R':
          pnp.f_root.len = 1;
          if (wasass) dstr_cpy(&s_root, &mval);
          break;
        case 'D':
          pnp.f_dir.len = 1;
          if (wasass) dstr_cpy(&s_dir, &mval);
          break;
        case 'B':
          pnp.f_base.len = 1;
          if (wasass) dstr_cpy(&s_base, &mval);
          break;
        case 'S':
          pnp.f_suffix.len = 1;
          if (wasass) dstr_cpy(&s_suffix, &mval);
          break;
        case 'M':
          pnp.f_member.len = 1;
          if (wasass) dstr_cpy(&s_member, &mval);
          break;
/*
        case 'P':
          if (wasass) { printf("FATAL: invalid selector assign: 'P'\n"); exit(42); }
          //if (edits->parent) path_parent(&pathname);
          pnp.f_grist.len = pnp.f_root.len = pnp.f_dir.len = 1;
          pnp.f_base.len = pnp.f_suffix.len = pnp.f_member.len = 0;
          break;
*/
        default: printf("FATAL: invalid selector: '%c'\n", mc); exit(42);
      }
    }
  }
  /* rebuild string */
  if (pnp_inited) {
    /* at least one of part selectors was hit */
    /* all parts that should be included have len > 0 */
    static char fname[MAXJPATH];
    /* grist */
    if (pnp.f_grist.len > 0) { pnp.f_grist.ptr = dstr_cstr(&s_grist); pnp.f_grist.len = dstr_len(&s_grist); }
    if (pnp.f_root.len > 0) { pnp.f_root.ptr = dstr_cstr(&s_root); pnp.f_root.len = dstr_len(&s_root); }
    if (pnp.f_dir.len > 0) { pnp.f_dir.ptr = dstr_cstr(&s_dir); pnp.f_dir.len = dstr_len(&s_dir); }
    if (pnp.f_base.len > 0) { pnp.f_base.ptr = dstr_cstr(&s_base); pnp.f_base.len = dstr_len(&s_base); }
    if (pnp.f_suffix.len > 0) { pnp.f_suffix.ptr = dstr_cstr(&s_suffix); pnp.f_suffix.len = dstr_len(&s_suffix); }
    if (pnp.f_member.len > 0) { pnp.f_member.ptr = dstr_cstr(&s_member); pnp.f_member.len = dstr_len(&s_member); }
    path_build(fname, &pnp);
    dstr_done(&s_grist);
    dstr_done(&s_root);
    dstr_done(&s_dir);
    dstr_done(&s_base);
    dstr_done(&s_suffix);
    dstr_done(&s_member);
    dstr_set_cstr(&mval, fname);
  } else {
    dstr_set_cstr(&mval, vval);
  }
  /* do case conversion */
  if (doupdown < 0) for (char *p = dstr_cstr(&mval); *p; ++p) *p = tolower(*p);
  if (doupdown > 0) for (char *p = dstr_cstr(&mval); *p; ++p) *p = toupper(*p);
  /* do quoting */
  if (qchar) {
    dstring_t tval, *ts = res;
    const unsigned char *t = (const unsigned char *)(dstr_cstr(&mval));
    for (; *t; ++t) if (qset[*t]) break;
    if (was_wc&0x7) {
      /* we need to expand/cut string, use temprorary storage */
      ts = &tval;
      dstr_init(ts);
    }
    /* copy 'ok' chars */
    dstr_push_memrange(ts, dstr_cstr(&mval), t);
    if (*t) {
      /* need to do quoting */
      for (; *t; ++t) {
        if (qset[*t]) dstr_push_char(ts, qchar);
        dstr_push_char(ts, *t);
      }
    }
    /* apply width modifiers */
    if (was_wc&0x7) {
      do_mod_cw(ts, was_wc, marg_w, marg_c);
      if (was_wc&0x04) {
        snprintf(qset, sizeof(qset), "%d", dstr_len(ts));
        dstr_set_cstr(&mval, qset);
        dstr_push_buf(res, dstr_cstr(&mval), dstr_len(&mval));
      } else {
        dstr_push_buf(res, dstr_cstr(ts), dstr_len(ts));
      }
      dstr_done(ts);
    }
  } else {
    /* apply width modifiers */
    if (was_wc&0x7) {
      do_mod_cw(&mval, was_wc, marg_w, marg_c);
      if (was_wc&0x04) {
        snprintf(qset, sizeof(qset), "%d", dstr_len(&mval));
        dstr_set_cstr(&mval, qset);
      }
    }
    dstr_push_buf(res, dstr_cstr(&mval), dstr_len(&mval));
  }
  dstr_done(&mval);
}


/*
 * get everything before first varref to string (pref)
 * get variable value (lval)
 * recursively expand everything after varref if anything (lrest)
 * lnew = L0;
 * foreach (l in lval) {
 *   str = pref;
 *   str += l->string;
 *   if (lrest) {
 *     foreach (r in lrest) {
 *       str1 = str+r->string;
 *       lnew = list_new(lnew, str1, 0);
 *     }
 *   } else {
 *     lnew = list_new(lnew, str, 0);
 *   }
 * }
 * list_free(lrest)
 * list_free(lval)
 * return lnew;
 */


/*
 * var_expand() - variable-expand input string into list of strings
 *
 * Would just copy input to output, performing variable expansion,
 * except that since variables can contain multiple values the result
 * of variable expansion may contain multiple values (a list).  Properly
 * performs "product" operations that occur in "$(var1)xxx$(var2)" or
 * even "$($(var2))".
 *
 * Returns a newly created list.
 *
 * `end` can be NULL, it means: `in` is cstr.
 */
LIST *var_expand (const char *in, const char *end, LOL *lol, int cancopyin) {
  LIST *res;
  const char *inp = in;
  if (end == NULL) end = in+strlen(in);
  if (DEBUG_VAREXP) printf("***expand '%.*s'\n", (int)(end-in), in);
  /* this gets alot of cases: $(<), $(>), $(1)...$(9) */
  if (end-in == 4 && in[0] == '$' && in[1] == '(' && in[3] == ')') {
    switch (in[2]) {
      case '0': return L0;
      case '<': return list_copy(L0, lol_get(lol, 0));
      case '>': return list_copy(L0, lol_get(lol, 1));
      default: if (isdigit(in[2])) return list_copy(L0, lol_get(lol, in[2]-'1'));
    }
  }
  /* another aliases */
  if (end-in == 5 && memcmp(in, "$(in)", 5) == 0) return list_copy(L0, lol_get(lol, 1));
  if (end-in == 6 && memcmp(in, "$(out)", 6) == 0) return list_copy(L0, lol_get(lol, 0));
  /* if there is no '$(' -- just copy */
  while (in < end-1) {
    if (in[0] == '$' && in[1] == '(') goto expand;
    ++in;
  }
  /* no variables expanded - just add copy of input string to list */
  /* cancopyin is an optimization: if the input was already a list */
  /* item, we can use the copystr() to put it on the new list, */
  /* otherwise, we use the slower newstr() */
  if (cancopyin) {
    res = list_new(L0, inp, 1);
  } else {
    /*FIXME: make this faster!*/
    /*FIXME: we should always have something at end[0]*/
    if (*end) {
      intptr_t len = end-inp;
      if (len > 0) {
        /* non-empty string */
        if (len < 512) {
          /* small string: use alloca() */
          char *s = alloca(len+1);
          memcpy(s, inp, len);
          s[len] = 0;
          res = list_new(L0, s, 0);
        } else {
          /* big string: use malloc() */
          char *s = malloc(len+1);
          memcpy(s, inp, len);
          s[len] = 0;
          res = list_new(L0, s, 0);
          free(s);
        }
      } else {
        /* empty string */
        res = list_new(L0, "", 0);
      }
    } else {
      /* string ends with zero char */
      res = list_new(L0, inp, 0);
    }
  }
  if (DEBUG_VAREXP) { printf(" **expand_res (%d): ", list_length(res)); list_print_ex(stdout, res, LPFLAG_NO_TRSPACE); printf("\n"); }
  return res;
expand: ;
  dstring_t prefix, varspec;
  int prefix_len; /* length of prefix -- for chopping */
  LIST *var_value, *rest_list;
  const char *vnstart;
  const char *rest; /* this will point just after the "$(...)" */
  int free_var_value = 0;
  /*
   * Input so far (ignore blanks):
   *
   *      stuff-in-outbuf $(variable) remainder
   *                      ^                    ^
   *                      in                   end
   * Output so far: nothing
   */
  dstr_init_memrange(&prefix, inp, in);
  prefix_len = dstr_len(&prefix);
  if ((rest = skip_varaccess(in, end)) == NULL) {
    printf("FATAL: invalid var access: '%.*s'\n", (int)(end-in), in);
    exit(42);
  }
  if (DEBUG_VAREXP) printf(" rest: '%.*s'\n", (int)(end-rest), rest);
  --rest; /* don't take last ')' */
  vnstart = (in += 2); /* skip $ and '(' */
  dstr_init(&varspec);
  /*
   * check if we have any $(...) before rest or ':' or '['.
   * if not, we can just use var_get() to get var value, else
   * we have to do complex things.
   */
  while (in < rest) {
    if (in[0] == ':' || in[0] == '[') break;
    dstr_push_char(&varspec, *in++);
    if (in[-1] == '$' && in < rest && in[0] == '(') break;
  }
  /* here 'in' either >= 'rest' or points to '(' after '$' or points to ':' or points to '[' */
  if (in < rest && in[0] == '(') {
    LIST *vname;
    /* we have to extract everything before ':' or '[' and expand it, then use var_get() to get resulting value */
    /* skip all var accessing till ':', '[' or EOL */
    --in; /* return to '$' */
    /* collect complex varname */
    for (;;) {
      const char *e = skip_varaccess(in, rest);
      if (e == NULL) {
        printf("FATAL: invalid var access: '%.*s'\n", (int)(rest-vnstart), vnstart);
        exit(42);
      }
      in = e;
      while (in < rest) {
        if (in[0] == ':' || in[0] == '[') break;
        if (in[0] == '$' && in+1 < rest && in[1] == '(') break;
        ++in;
      }
      /* here 'in' either >= 'rest' or points or '$(' or to terminator */
      if (in >= rest || in[0] != '$') break;
    }
    if (DEBUG_VAREXP) printf(" varname_expand: '%.*s'\n", (int)(in-vnstart), vnstart);
    vname = var_expand(vnstart, in, lol, 0);
    if (!vname) {
      printf("FATAL: trying to dereference NULL variable: '%.*s'\n", (int)(in-vnstart), vnstart);
      exit(42);
    }
    if (DEBUG_VAREXP) printf(" varname_expand result: '%s'\n", vname->string);
    dstr_set_cstr(&varspec, vname->string);
    list_free(vname);
  }
  /* get variable value, check for special vars */
  if (DEBUG_VAREXP) printf(" var_get: '%s'\n", dstr_cstr(&varspec));
  {
    const char *vn = dstr_cstr(&varspec);
    if (!vn[1] && isdigit(vn[0])) var_value = (vn[0] == '0' ? L0 : lol_get(lol, vn[0]-'1'));
    else if ((!vn[1] && vn[0] == '<') || strcmp(vn, "out") == 0) var_value = lol_get(lol, 0);
    else if ((!vn[1] && vn[0] == '>') || strcmp(vn, "in") == 0) var_value = lol_get(lol, 1);
    else var_value = var_get(dstr_cstr(&varspec));
  }
  /*
   * here 'in' either >= 'rest' or points to ':' or points to '['
   * prefix is literal prefix
   * var_value is variable value (NOT COPIED!)
   *
   * do indexing first and then selectors
   */
  if (DEBUG_VAREXP) { printf(" var_value (%d): ", list_length(var_value)); list_print_ex(stdout, var_value, LPFLAG_NO_TRSPACE); printf("\n"); }
  /* optimization: if this is simple var access, just return it's value */
  if (in >= end && dstr_len(&prefix) == 0) {
    dstr_done(&varspec);
    dstr_done(&prefix);
    res = list_copy(L0, var_value);
    if (DEBUG_VAREXP) { printf(" *(s)expand_res (%d): ", list_length(res)); list_print_ex(stdout, res, LPFLAG_NO_TRSPACE); printf("\n"); }
    return res;
  }
  if (in < rest && in[0] == '[') {
    /* process indexing */
    /* here we have special rules for variable expansion: '-' and ',' are terminators, leading and trailing spaces ignored */
    LIST *nval = L0;
    int vvlen = list_length(var_value);
    ++in; /* skip '[' */
    while (in < rest && in[0] != ']') {
      int idx = 0;
      for (; in < rest && *in && isspace(*in); ++in) ; /* skip leading spaces */
      if (in < rest && in[0] == ',') { ++in; continue; } /* skip excessive commas */
      /* go till terminator */
      if (in >= rest) { printf("FATAL: invalid index (0): '%.*s'\n", (int)(rest-vnstart), vnstart); exit(42); }
      if (in[0] == ']') break;
      /* this must be number */
      if ((in = parse_number(&idx, in, rest, lol)) == NULL) { printf("FATAL: invalid index (1): '%.*s'\n", (int)(rest-vnstart), vnstart); exit(42); }
      if (idx < 0 && (idx += vvlen+1) < 0) idx = 0;
      if (in[0] == '-') {
        /* this is range */
        int iend;
        ++in; /* skip '-' */
        for (; in < rest && *in && isspace(*in); ++in) ; /* skip leading spaces */
        if (in >= rest) { printf("FATAL: invalid index (2): '%.*s'\n", (int)(rest-vnstart), vnstart); exit(42); }
        if (in[0] == ',' || in[0] == ']') {
          iend = vvlen;
        } else {
          if ((in = parse_number(&iend, in, rest, lol)) == NULL) { printf("FATAL: invalid index (3): '%.*s'\n", (int)(rest-vnstart), vnstart); exit(42); }
          if (iend < 0 && (iend += vvlen+1) < 0) iend = 0;
        }
        if (iend > 0) {
          if (idx < 0) idx = 1;
          if (iend > vvlen) iend = vvlen;
          if (idx <= vvlen && iend >= idx) {
            LIST *i;
            iend -= idx; /* convert to length-1 */
            for (i = var_value; --idx > 0; i = i->next) ;
            for (; iend-- >= 0; i = i->next) nval = list_new(nval, i->string, 1);
          }
        }
      } else {
        /* single index */
        if (idx >= 1 && idx <= vvlen) {
          LIST *i;
          for (i = var_value; --idx > 0; i = i->next) ;
          nval = list_new(nval, i->string, 1);
        }
      }
      if (in < rest && in[0] == ']') break; /* done */
      if (in >= rest || in[0] != ',') { printf("FATAL: invalid index (4): '%.*s'\n", (int)(rest-vnstart), vnstart); exit(42); }
      ++in; /* skip ',' */
    }
    if (in >= rest || in[0] != ']') { printf("FATAL: invalid index (5): '%.*s'\n", (int)(rest-vnstart), vnstart); exit(42); }
    ++in; /* skip ']' */
    free_var_value = 1;
    var_value = nval;
  }
  /* 'rest' points to ')' */
  if (rest+1 < end) {
    /* we have something after the var, expand it */
    rest_list = var_expand(rest+1, end, lol, 0);
    if (DEBUG_VAREXP) { printf("   rest_list: (%d): ", list_length(rest_list)); list_print_ex(stdout, rest_list, LPFLAG_NO_TRSPACE); printf("\n"); }
  } else {
    /* this is the end my only friend */
    rest_list = L0;
  }
  /* if we have ':E' modifier, apply it */
  if (var_value == L0 && in < rest && in[0] == ':' && find_empty(in, rest, lol, &varspec)) {
    if (DEBUG_VAREXP) printf(" E: '%s'\n", dstr_cstr(&varspec));
    free_var_value = 1;
    var_value = list_new(L0, dstr_cstr(&varspec), 0);
  }
  /* if we have ':J' modifier, join all values */
  if (var_value != L0 && var_value->next && in < rest && in[0] == ':' && find_join(in, rest, lol, &varspec)) {
    /* join var_value, processing selectors by the way */
    dstring_t nval;
    dstr_init(&nval);
    if (DEBUG_VAREXP) printf(" JOIN: '%s'\n", dstr_cstr(&varspec));
    for (LIST *cv = var_value; cv != NULL; cv = cv->next) {
      /*int olen = dstr_len(&nval);*/
      if (in < rest && in[0] == ':') {
        process_selectors(&nval, in, rest, lol, cv->string);
      } else {
        dstr_push_cstr(&nval, cv->string);
      }
      /* skip empty strings */
      if (cv->next != NULL /*&& dstr_len(&nval) > olen*/) dstr_push_cstr(&nval, dstr_cstr(&varspec));
    }
    if (free_var_value) list_free(var_value);
    free_var_value = 1;
    var_value = list_new(L0, dstr_cstr(&nval), 0);
    dstr_done(&nval);
  }
  /* assemble output */
  res = L0;
  /* for each item in var_value list */
  for (LIST *cv = var_value; cv != NULL; cv = cv->next) {
    /* we have to process selectors seperately for each item */
    if (in < rest && in[0] == ':') {
      process_selectors(&prefix, in, rest, lol, cv->string);
    } else {
      dstr_push_cstr(&prefix, cv->string);
    }
    /* here 'prefix' actually contains "prefix"+'current_value' */
    /* have something in rest_list? */
    if (rest_list != L0) {
      /* yes, combine current prefix with each rest_list item */
      int ol = dstr_len(&prefix);
      for (LIST *rl = rest_list; rl != NULL; rl = rl->next) {
        dstr_chop(&prefix, ol);
        dstr_push_cstr(&prefix, rl->string);
        res = list_new(res, dstr_cstr(&prefix), 0);
      }
    } else {
      /* just add prefix */
      res = list_new(res, dstr_cstr(&prefix), 0);
    }
    /* restore prefix and process next item */
    dstr_chop(&prefix, prefix_len);
  }
  list_free(rest_list);
  if (free_var_value) list_free(var_value);
  dstr_done(&varspec);
  dstr_done(&prefix);
  if (DEBUG_VAREXP) { printf(" *expand_res (%d): ", list_length(res)); list_print_ex(stdout, res, LPFLAG_NO_TRSPACE); printf("\n"); }
  return res;
}
