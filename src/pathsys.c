/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * pathsys.c - manipulate file names on UNIX, NT, OS2, AmigaOS
 *
 * External routines:
 *
 *  path_parse() - split a file name into dir/base/suffix/member
 *  path_build() - build a filename given dir/base/suffix/member
 *  path_parent() - make a PATHNAME point to its parent dir
 *
 * File_parse() and path_build() just manipuate a string and a structure;
 * they do not make system calls.
 */
#include <limits.h>

#include "jam.h"
#include "pathsys.h"


/*
 * path_parse() - split a file name into dir/base/suffix/member
 */
void path_parse (const char *file, PATHNAME *f) {
  const char *p, *end;
  memset(f, 0, sizeof(*f));
  /* look for <grist> */
  if (file[0] == '<' && (p = strchr(file, '>')) != NULL) {
    f->f_grist.ptr = file;
    f->f_grist.len = p-file;
    file = p+1; /* 'file' moved past grist */
  }
  /* look for dir/ */
  p = strrchr(file, '/');
#if PATH_DELIM == '\\'
  /* on NT, look for dir\ as well */
  {
    char *p1 = strrchr(file, '\\');
    p = (p1 > p ? p1 : p);
  }
#endif
  if (p != NULL) {
    f->f_dir.ptr = file;
    f->f_dir.len = p-file;
    /* special case for / - dirname is /, not "" */
    if (f->f_dir.len == 0) f->f_dir.len = 1;
#if PATH_DELIM == '\\'
    /* special case for D:/ - dirname is D:/, not "D:" */
    if (f->f_dir.len == 2 && file[1] == ':') f->f_dir.len = 3;
#endif
    file = p+1; /* 'file' moved past dir */
  }
  end = file+strlen(file);
  if (end > file) {
    const char *q;
    /* look for (member) */
    if (end[-1] == ')' && (p = strrchr(file, '(')) != NULL) {
      f->f_member.ptr = p+1;
      f->f_member.len = end-p-2;
      end = p;
    }
    /* look for .suffix */
    /* this would be memrchr() */
    p = NULL;
    q = file;
    while ((q = memchr(q, '.', end-q))) p = q++;
    if (p != NULL) {
      f->f_suffix.ptr = p;
      f->f_suffix.len = end-p;
      end = p;
    }
  }
  /* leaves base */
  f->f_base.ptr = file;
  f->f_base.len = end-file;
}


/*
 * path_build() - build a filename given dir/base/suffix/member
 */
void path_build (char *file, const PATHNAME *f) {
  /* start with the grist; if the current grist isn't surrounded by <>'s, add them */
  if (f->f_grist.len > 0) {
    if (f->f_grist.ptr[0] != '<') *file++ = '<';
    memmove(file, f->f_grist.ptr, f->f_grist.len);
    file += f->f_grist.len;
    if (file[-1] != '>') *file++ = '>';
  }
  /* don't prepend root if it's "." or directory is rooted */
  if (f->f_root.len > 0 && !(f->f_root.len == 1 && f->f_root.ptr[0] == '.') && !(f->f_dir.len > 0 && f->f_dir.ptr[0] == '/')
#if PATH_DELIM == '\\'
      && !(f->f_dir.len > 0 && f->f_dir.ptr[0] == '\\') && !(f->f_dir.len > 0 && f->f_dir.ptr[1] == ':')
#endif
     )
  {
    memmove(file, f->f_root.ptr, f->f_root.len);
    file += f->f_root.len;
    *file++ = PATH_DELIM;
  }
  if (f->f_dir.len > 0) {
    memmove(file, f->f_dir.ptr, f->f_dir.len);
    file += f->f_dir.len;
  }
  /* UNIX: Put / between dir and file */
  /* NT:   Put \ between dir and file */
  if (f->f_dir.len > 0 && (f->f_base.len > 0 || f->f_suffix.len > 0)) {
    /* UNIX: Special case for dir \ : don't add another \ */
    /* NT:   Special case for dir / : don't add another / */
#if PATH_DELIM == '\\'
    if (!(f->f_dir.len == 3 && f->f_dir.ptr[1] == ':'))
#endif
    if (!(f->f_dir.len == 1 && f->f_dir.ptr[0] == PATH_DELIM)) *file++ = PATH_DELIM;
  }
  if (f->f_base.len > 0) {
    memmove(file, f->f_base.ptr, f->f_base.len);
    file += f->f_base.len;
  }
  if (f->f_suffix.len > 0) {
    memmove(file, f->f_suffix.ptr, f->f_suffix.len);
    file += f->f_suffix.len;
  }
  if (f->f_member.len > 0) {
    *file++ = '(';
    memmove(file, f->f_member.ptr, f->f_member.len);
    file += f->f_member.len;
    *file++ = ')';
  }
  *file = 0;
}


/*
 *  path_parent() - make a PATHNAME point to its parent dir
 */
void path_parent (PATHNAME *f) {
  /* just set everything else to nothing */
  f->f_base.ptr = f->f_suffix.ptr = f->f_member.ptr = "";
  f->f_base.len = f->f_suffix.len = f->f_member.len = 0;
}


/*
 *  normalize_path() - normalize a path
 *
 *  It doesn't really generate a unique representation of a path to an entry,
 *  but at least reduces the number of categories that represent the same
 *  entry. On error, or if the supplied buffer is too small, NULL is returned.
 */
char *normalize_path (const char *path, char *buffer, size_t buf_size, const char *pwd) {
#if PATH_DELIM == '\\'
  /* stupid windoze; convert all idiotic backslashes to normal slashes */
  static char w2upath[PATH_MAX];
#endif
  char *res = buffer;
  static char cwd_buf[PATH_MAX];
  char *cwd = NULL;
  static size_t cwd_len = 0;
  int res_len = 0;
  int ends_with_slash = 0;
  /* init cwd */
  if (pwd == NULL) {
    if ((cwd = getcwd(cwd_buf, PATH_MAX)) == NULL) return NULL;
  } else {
    /*FIXME: possible overflow*/
    snprintf(cwd_buf, sizeof(cwd_buf), "%s", pwd);
    cwd = cwd_buf;
  }
  cwd_len = strlen(cwd);
  if (path == NULL) path = "";
  /* start reconstructing path */
#if PATH_DELIM == '\\'
  snprintf(w2upath, sizeof(w2upath), "%s", path);
  for (char *p = w2upath; *p; ++p) if (*p == '\\') *p = '/';
  path = w2upath;
  /* check if this path is absolute */
  /* windoze: check if we have disk letter here */
  if (path[0] && path[1] == ':') {
    /* copy disk letter */
    if (buf_size < 3) return NULL;
    buf_size -= 2;
    *buffer++ = (cwd[0] && cwd[1] == ':' ? cwd[0] : path[0]);
    *buffer++ = ':';
    path += 2;
  }
  /* convert cwd */
  if (cwd[0] && cwd[1] == ':') { cwd += 2; cwd_len -= 2; }
  for (char *p = cwd; *p; ++p) if (*p == '\\') *p = '/';
#endif
  /* check if this path is absolute */
  if (path[0] != '/') {
    /* prepend pwd */
    if (cwd_len > 0) {
      if (buf_size < cwd_len+1) return NULL;
      memmove(buffer, cwd, cwd_len);
      res_len += cwd_len;
    }
  } else {
    /* put slash in output; excess slashes will be eaten by the main loop */
    if (buf_size < 2) return NULL;
    buffer[res_len++] = '/';
  }
  ends_with_slash = (path[0] && path[strlen(path)-1] == '/');
  /* main loop */
  while (*path) {
    const char *e;
    /* skip leading slashes */
    while (*path && path[0] == '/') ++path;
    if (!path[0]) break; /* no more */
    if ((e = strchr(path, '/')) == NULL) e = path+strlen(path);
    if (e-path == 1 && path[0] == '.') {
      /* this is unnecessary dot, skip it */
      path = e;
      continue;
    }
    if (e-path == 2 && path[0] == '.' && path[1] == '.') {
      /* dotdot; go one dir up if we can */
      /* we can't go up if we are at root or if previous dir is "." or ".." */
      if (res_len > 0 && !(res_len == 1 && buffer[0] == '/')) {
        char *pd = buffer+res_len;
        int sz = res_len;
        int do_remove = 0;
        if (pd[-1] == '/') { --pd; --sz; }
        if (sz > 0) {
          if (pd[-1] != '.') {
            do_remove = 1;
          } else if (sz > 1) {
            /* last char is '.' and we have more chars */
            if (pd[-2] != '.') {
              do_remove = 1;
            } else if (sz > 2) {
              /* last two chars is ".." and we have more chars */
              do_remove = (pd[-3] != '/');
            }
          }
        }
        if (do_remove) {
          if (res_len > 0 && buffer[res_len-1] == '/') --res_len; /* remove trailing slash if any */
          while (res_len > 0 && buffer[res_len-1] != '/') --res_len; /* remove last dir */
          path = e;
          continue;
        }
      }
    }
    /* append this dir */
    if (res_len > 0 && buffer[res_len-1] != '/') {
      if (res_len+2 > (ssize_t)buf_size) return NULL;
      buffer[res_len++] = '/';
    }
    if (res_len+(ptrdiff_t)(e-path)+1 > (ssize_t)buf_size) return NULL;
    memmove(buffer+res_len, path, (size_t)(ptrdiff_t)(e-path));
    res_len += e-path;
    path = e;
  }
  /* add trailing slash if we need it */
  if (ends_with_slash && res_len > 0 && buffer[res_len-1] != '/') {
    if (res_len+2 > (ssize_t)buf_size) return NULL;
    buffer[res_len++] = '/';
  }
  /* add terminator */
  if (res_len+1 > (ssize_t)buf_size) return NULL;
  buffer[res_len] = 0;
  return res;
}
