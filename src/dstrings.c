/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dstrings.h"


static inline void dstr_zero (dstring_t *s) {
  if (s != NULL) {
    s->str = s->sbuf;
    s->len = 0;
    s->size = sizeof(s->sbuf);
    s->sbuf[0] = 0;
  }
}


void dstr_init (dstring_t *s) {
  dstr_zero(s);
}


void dstr_init_cstr (dstring_t *s, const void *str) {
  dstr_init(s);
  dstr_push_cstr(s, str);
}


void dstr_init_buf (dstring_t *s, const void *start, int len) {
  dstr_init(s);
  dstr_push_buf(s, start, len);
}


void dstr_init_memrange (dstring_t *s, const void *start, const void *finish) {
  dstr_init(s);
  dstr_push_memrange(s, start, finish);
}


void dstr_done (dstring_t *s) {
  if (s != NULL) {
    if (s->str != s->sbuf) free(s->str);
    dstr_zero(s);
  }
}


void dstr_clear (dstring_t *s) {
  if (s != NULL) {
    s->str[s->len=0] = 0;
  }
}


void dstr_empty (dstring_t *s) {
  if (s != NULL) {
    if (s->str != s->sbuf) free(s->str);
    dstr_zero(s);
  }
}


static void dstr_resv (dstring_t *s, int size) {
  if (s != NULL) {
    if (size < 0) size = 0;
    if (size > 0) {
      if (s->str == s->sbuf) {
        s->str = malloc(size);
        if (s->len > 0) memcpy(s->str, s->sbuf, s->len);
        s->str[s->len] = 0;
      } else {
        char *ns = realloc(s->str, size);
        if (ns == NULL) { fprintf(stderr, "FATAL: out of memory!\n"); abort(); }
        s->str = ns;
      }
    }
    s->size = size;
  }
}


void dstr_reserve (dstring_t *s, int size) {
  if (s != NULL && size > s->size) dstr_resv(s, size);
}


// make room for at least `spc` chars
static inline void dstr_expand (dstring_t *s, int spc) {
  if (s != NULL && spc >= 0) dstr_reserve(s, ((s->len+spc+1)|0x3ff)+1);
}


void dstr_push_buf (dstring_t *s, const void *start, int len) {
  if (s != NULL) {
    if (len < 0) len = (start != NULL ? strlen((const char *)start) : 0);
    if (len > 0) {
      dstr_expand(s, len);
      if (start == NULL) memset(s->str+s->len, 32, len); else memmove(s->str+s->len, start, len);
      s->str[s->len+=len] = 0;
    }
  }
}


void dstr_push_cstr (dstring_t *s, const void *str) {
  dstr_push_buf(s, str, -1);
}


void dstr_push_memrange (dstring_t *s, const void *start, const void *finish) {
  if (s != NULL && finish > start) dstr_push_buf(s, start, ((const char *)finish)-((const char *)start));
}


void dstr_push_char (dstring_t *s, int x) {
  if (s != NULL) {
    dstr_expand(s, 1);
    s->str[s->len++] = x&0xff;
    s->str[s->len] = 0;
  }
}


void dstr_set_cstr (dstring_t *s, const void *cstr) {
  dstr_clear(s);
  dstr_push_cstr(s, cstr);
}


void dstr_set_buf (dstring_t *s, const void *start, int len) {
  dstr_clear(s);
  dstr_push_buf(s, start, len);
}

void dstr_set_memrange (dstring_t *s, const void *start, const void *finish) {
  dstr_clear(s);
  dstr_push_memrange(s, start, finish);
}


void dstr_chop (dstring_t *s, int n) {
  if (s != NULL) {
    if (n < 0) n = 0; else if (n > s->len) n = s->len;
    s->str[s->len=n] = 0;
  }
}


int dstr_pop_char (dstring_t *s) {
  if (s != NULL) {
    int res = (s->len > 0 ? (unsigned char)(s->str[--s->len]) : 0);
    s->str[s->len] = 0;
    return res;
  }
  return 0;
}


char dstr_last_char (dstring_t *s) {
  return (s != NULL && s->len > 0 ? s->str[s->len-1] : 0);
}


char *dstr_cstr (dstring_t *s) {
  return (s != NULL ? s->str : NULL);
}


int dstr_len (dstring_t *s) {
  return (s != NULL ? s->len : 0);
}
