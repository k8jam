/*
 * Copyright 1993, 1995 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * lists.h - the LIST structure and routines to manipulate them
 *
 * The whole of jam relies on lists of strings as a datatype.  This
 * module, in conjunction with newstr.c, handles these relatively
 * efficiently.
 *
 * Structures defined:
 *
 *  LIST - list of strings
 *  LOL - list of LISTs
 *
 * External routines:
 *
 *  list_append() - append a list onto another one, returning total
 *  list_new() - tack a string onto the end of a list of strings
 *  list_copy() - copy a whole list of strings
 *  list_sublist() - copy a subset of a list of strings
 *  list_free() - free a list of strings
 *  list_print() - print a list of strings to stdout
 *  list_printq() - print a list of safely quoted strings to a file
 *  list_length() - return the number of items in the list
 *
 *  lol_init() - initialize a LOL (list of lists)
 *  lol_add() - append a LIST onto an LOL
 *  lol_free() - free the LOL and its LISTs
 *  lol_get() - return one of the LISTs in the LOL
 *  lol_print() - debug print LISTS separated by ":"
 */
#ifndef JAMH_LISTS_H
#define JAMH_LISTS_H

#include <stdio.h>


/* LIST - list of strings */
typedef struct _list LIST;
struct _list {
  LIST *next;
  LIST *tail; /* only valid in head node */
  const char *string; /* private copy */
};


/*  LOL - list of LISTs */
#define LOL_MAX  (64)
typedef struct _lol LOL;
struct _lol {
  int count;
  LIST *list[LOL_MAX];
};


enum {
  LPFLAG_DEFAULT    = 0,
  LPFLAG_NO_SPACES  = 0x01,
  LPFLAG_NO_TRSPACE = 0x02  /* suppress only trailing space */
};

extern LIST *list_append (LIST *l, LIST *nl);
extern LIST *list_copy (LIST *l, LIST *nl);
extern void list_free (LIST *head);
extern LIST *list_new (LIST *head, const char *string, int copy);
extern void list_print_ex (FILE *fo, const LIST *l, int flags); /* LPFLAG_xxx */
static inline void list_print (const LIST *l) { list_print_ex(stdout, l, LPFLAG_DEFAULT); }
extern int list_length (const LIST *l) JAMFA_PURE;
extern LIST *list_sublist (LIST *l, int start, int count);
extern LIST *list_sort (LIST *l);
extern LIST *list_reverse (const LIST *l); /* returns new list */

extern LIST *list_removeall (LIST *l, LIST *nl);

#define list_next(l)  ((l)->next)

#define L0  ((LIST *)0)

extern void lol_add (LOL *lol, LIST *l);
extern void lol_init (LOL *lol);
extern void lol_free (LOL *lol);
extern LIST *lol_get (LOL *lol, int i) JAMFA_PURE;
extern void lol_print (const LOL *lol);


#endif
