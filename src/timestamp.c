/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * timestamp.c - get the timestamp of a file or archive member
 */
#include "jam.h"
#include "hash.h"
#include "filesys.h"
#include "pathsys.h"
#include "timestamp.h"
#include "newstr.h"


/*
 * BINDING - all known files
 */

#define BIND_SCANNED  0x01  /* if directory or arch, has been scanned */

enum {
  BIND_INIT, /* never seen */
  BIND_NOENTRY, /* timestamp requested but file never found */
  BIND_SPOTTED, /* file found but not timed yet */
  BIND_MISSING, /* file found but can't get timestamp */
  BIND_FOUND /* file found and time stamped */
};


typedef struct _binding BINDING;
struct _binding {
  const char *name;
  short flags; // BIND_SCANNED
  short progress; // enum
  time_t time; /* update time - 0 if not exist */
};


static struct hash *bindhash = 0;
static void time_enter (void *, const char *, int, time_t);


static const char *time_progress[] = {
  "INIT",
  "NOENTRY",
  "SPOTTED",
  "MISSING",
  "FOUND"
};


/*
 * timestamp() - return timestamp on a file, if present
 */
void timestamp (char *target, time_t *time) {
  PATHNAME f1, f2;
  BINDING binding, *b = &binding;
  static char buf[MAXJPATH];
#ifdef DOWNSHIFT_PATHS
  static char path[MAXJPATH];
  char *p = path;
  do { *p++ = tolower(*target); } while (*target++) ;
  target = path;
#endif
  if (!bindhash) bindhash = hashinit(sizeof(BINDING), "bindings");
  /* quick path: is it there? */
  b->name = target;
  b->time = b->flags = 0;
  b->progress = BIND_INIT;
  if (hashenter(bindhash, (HASHDATA **)&b)) b->name = newstr(target); /* never freed */
  if (b->progress != BIND_INIT) goto afterscanning;
  b->progress = BIND_NOENTRY;
  /* not found: have to scan for it */
  path_parse(target, &f1);
  /* scan directory if not already done so */
  {
    BINDING xxbinding, *xxb = &xxbinding;
    //
    f2 = f1;
    f2.f_grist.len = 0;
    path_parent(&f2);
    path_build(buf, &f2);
    xxb->name = buf;
    xxb->time = xxb->flags = 0;
    xxb->progress = BIND_INIT;
    if (hashenter(bindhash, (HASHDATA **)&xxb)) xxb->name = newstr(buf); /* never freed */
    if (!(xxb->flags&BIND_SCANNED)) {
      file_dirscan(buf, time_enter, bindhash);
      xxb->flags |= BIND_SCANNED;
    }
  }
  /* scan archive if not already done so */
  if (f1.f_member.len) {
    BINDING xxbinding, *xxb = &xxbinding;
    f2 = f1;
    f2.f_grist.len = 0;
    f2.f_member.len = 0;
    path_build(buf, &f2);
    xxb->name = buf;
    xxb->time = xxb->flags = 0;
    xxb->progress = BIND_INIT;
    if (hashenter(bindhash, (HASHDATA **)&xxb)) xxb->name = newstr(buf); /* never freed */
    if (!(xxb->flags&BIND_SCANNED)) {
      file_archscan(buf, time_enter, bindhash);
      xxb->flags |= BIND_SCANNED;
    }
  }
afterscanning:
  if (b->progress == BIND_SPOTTED) {
    if (file_time(b->name, &b->time) < 0) b->progress = BIND_MISSING;
    else b->progress = BIND_FOUND;
  }
  *time = b->progress == BIND_FOUND ? b->time : 0;
}


static void time_enter (void *closure, const char *target, int found, time_t time) {
  BINDING binding, *b = &binding;
  struct hash *bindhash = (struct hash *)closure;
#ifdef DOWNSHIFT_PATHS
  static char path[MAXJPATH];
  char *p = path;
  do { *p++ = tolower(*target); } while (*target++) ;
  target = path;
#endif
  b->name = target;
  b->flags = 0;
  if (hashenter(bindhash, (HASHDATA **)&b)) b->name = newstr(target); /* never freed */
  b->time = time;
  b->progress = found ? BIND_FOUND : BIND_SPOTTED;
  if (DEBUG_BINDSCAN) printf("time ( %s ) : %s\n", target, time_progress[b->progress]);
}


/*
 * donestamps() - free timestamp tables
 */
void donestamps (void) {
  hashdone(bindhash);
}
