/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 *
 * License is hereby granted to use this software and distribute it
 * freely, as long as this copyright notice is retained and modifications
 * are clearly marked.
 *
 * ALL WARRANTIES ARE HEREBY DISCLAIMED.
 *
 */
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * jam.c - make redux
 *
 * See Jam.html for usage information.
 *
 * These comments document the code.
 *
 * The top half of the code is structured such:
 *
 *                       jam
 *                      / | \
 *                 +---+  |  \
 *                /       |   \
 *         jamgram     option  \
 *        /  |   \              \
 *       /   |    \              \
 *      /    |     \             |
 *  scan     |     compile      make
 *   |       |    /  | \       / |  \
 *   |       |   /   |  \     /  |   \
 *   |       |  /    |   \   /   |    \
 * jambase parse     |   rules  search make1
 *                   |           |      |   \
 *                   |           |      |    \
 *                   |           |      |     \
 *               builtins    timestamp command execute
 *                               |
 *                               |
 *                               |
 *                             filesys
 *
 *
 * The support routines are called by all of the above, but themselves
 * are layered thus:
 *
 *                     variable|expand
 *                      /  |   |   |
 *                     /   |   |   |
 *                    /    |   |   |
 *                 lists   |   |   pathsys
 *                    \    |   |
 *                     \   |   |
 *                      \  |   |
 *                     newstr  |
 *                        \    |
 *                         \   |
 *                          \  |
 *                          hash
 *
 * Roughly, the modules are:
 *
 *  builtins.c - jam's built-in rules
 *  command.c - maintain lists of commands
 *  compile.c - compile parsed jam statements
 *  execcmd.c - execute a shell script on UNIX
 *  expand.c - expand a buffer, given variable values
 *  file*.c - scan directories and archives on *
 *  hash.c - simple in-memory hashing routines
 *  headers.c - handle #includes in source files
 *  jambase.c - compilable copy of Jambase
 *  jamgram.y - jam grammar
 *  lists.c - maintain lists of strings
 *  make.c - bring a target up to date, once rules are in place
 *  make1.c - execute command to bring targets up to date
 *  newstr.c - string manipulation routines
 *  option.c - command line option processing
 *  parse.c - make and destroy parse trees as driven by the parser
 *  path*.c - manipulate file names on *
 *  hash.c - simple in-memory hashing routines
 *  re9.c - regexp engine
 *  rules.c - access to RULEs, TARGETs, and ACTIONs
 *  scan.c - the jam yacc scanner
 *  search.c - find a target along $(SEARCH) or $(LOCATE)
 *  timestamp.c - get the timestamp of a file or archive member
 *  variable.c - handle jam multi-element variables
 */
#include "jam.h"
#include "jambase.h"
#include "option.h"
#include "patchlevel.h"

/* These get various function declarations. */
#include "lists.h"
#include "parse.h"
#include "variable.h"
#include "compile.h"
#include "builtins.h"
#include "rules.h"
#include "newstr.h"
#include "scan.h"
#include "timestamp.h"
#include "make.h"
#include "hcache.h"

#include "genman.h"

/* And UNIX for this */
#ifdef unix
# include <sys/utsname.h>
#endif


#ifdef USE_DIETLIBC
char *stpcpy (char *dst, const char *src) {
  while ((*dst++ = *src++));
  return (dst-1);
}
#endif


struct globs globs = {
  .noexec = 0,
  .jobs = 1,
  .quitquick = 0,
  .newestfirst = 0, /* newestfirst */
  .debug = { 0, 1 }, /* display actions */
  .cmdout = NULL, /* output commands, not run them */
//#ifdef OPT_IMPROVED_PROGRESS_EXT
  .updating = 0,
  .progress = NULL,
//#endif
};


/* symbols to be defined as true for use in Jambase */
static const char *othersyms[] = { OSMAJOR, OSMINOR, OSPLAT, JAMVERSYM, JAMVERSYM_HI, JAMVERSYM_MID, JAMVERSYM_LO, 0 } ;


/* Known for sure:
 *  OS2 needs extern environ
 */
#ifndef use_environ
# define use_environ environ
# if !defined(__WATCOM__) && !defined(OS_NT)
extern char **environ;
# endif
#endif


static void fixJobs (void) {
  LIST *var = var_get("K8JAM-JOBS"); /*FIXME: a perfectly idiotic name*/
  char buf[128];
  //
  if (var) {
    int cnt = list_length(var);
    //
    if (cnt == 0) {
      globs.jobs = 1;
    } else {
      int jj = atoi(var->string);
      //
      if (jj < 1) jj = 1; else if (jj > 64) jj = 64;
      globs.jobs = jj;
    }
  }
  //
  if (globs.jobs < 1) globs.jobs = 1; else if (globs.jobs > 64) globs.jobs = 64;
  snprintf(buf, sizeof(buf), "%d", globs.jobs);
  var = list_new(L0, buf, 0);
  var_set("K8JAM-JOBS", var, VAR_SET);
}


static void var_define_host_cpu (void) {
  /* host CPU and bitness: HOST_CPU, HOST_32BIT, HOST_64BIT */
  #if defined(i386) || defined(__i386__) || defined(__i386) || defined(_M_IX86)
  const char *hcpu[] = { "HOST_CPU=x86", "HOST_32BIT=tan", "HOST_64BIT=", NULL };
  #elif defined(__x86_64__) || defined(_M_X64)
  const char *hcpu[] = { "HOST_CPU=x86_64", "HOST_32BIT=", "HOST_64BIT=tan", NULL };
  #elif defined(__ARM_ARCH_2__)
  const char *hcpu[] = { "HOST_CPU=arm", "HOST_32BIT=tan", "HOST_64BIT=", "HOST_ARM=arm2", NULL };
  #elif defined(__ARM_ARCH_3__) || defined(__ARM_ARCH_3M__)
  const char *hcpu[] = { "HOST_CPU=arm", "HOST_32BIT=tan", "HOST_64BIT=", "HOST_ARM=arm3", NULL };
  #elif defined(__ARM_ARCH_4T__) || defined(__TARGET_ARM_4T)
  const char *hcpu[] = { "HOST_CPU=arm", "HOST_32BIT=tan", "HOST_64BIT=", "HOST_ARM=arm4t", NULL };
  #elif defined(__ARM_ARCH_5_) || defined(__ARM_ARCH_5E_)
  const char *hcpu[] = { "HOST_CPU=arm", "HOST_32BIT=tan", "HOST_64BIT=", "HOST_ARM=arm5", NULL };
  #elif defined(__ARM_ARCH_6T2_) || defined(__ARM_ARCH_6T2_)
  const char *hcpu[] = { "HOST_CPU=arm", "HOST_32BIT=tan", "HOST_64BIT=", "HOST_ARM=arm6t2", NULL };
  #elif defined(__ARM_ARCH_6__) || defined(__ARM_ARCH_6J__) || defined(__ARM_ARCH_6K__) || defined(__ARM_ARCH_6Z__) || defined(__ARM_ARCH_6ZK__)
  const char *hcpu[] = { "HOST_CPU=arm", "HOST_32BIT=tan", "HOST_64BIT=", "HOST_ARM=arm6", NULL };
  #elif defined(__ARM_ARCH_7__) || defined(__ARM_ARCH_7A__) || defined(__ARM_ARCH_7R__) || defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7S__)
  const char *hcpu[] = { "HOST_CPU=arm", "HOST_32BIT=tan", "HOST_64BIT=", "HOST_ARM=arm7", NULL };
  #elif defined(__ARM_ARCH_7A__) || defined(__ARM_ARCH_7R__) || defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7S__)
  const char *hcpu[] = { "HOST_CPU=arm", "HOST_32BIT=tan", "HOST_64BIT=", "HOST_ARM=arm7a", NULL };
  #elif defined(__ARM_ARCH_7R__) || defined(__ARM_ARCH_7M__) || defined(__ARM_ARCH_7S__)
  const char *hcpu[] = { "HOST_CPU=arm", "HOST_32BIT=tan", "HOST_64BIT=", "HOST_ARM=arm7r", NULL };
  #elif defined(__ARM_ARCH_7M__)
  const char *hcpu[] = { "HOST_CPU=arm", "HOST_32BIT=tan", "HOST_64BIT=", "HOST_ARM=arm7m", NULL };
  #elif defined(__ARM_ARCH_7S__)
  const char *hcpu[] = { "HOST_CPU=arm", "HOST_32BIT=tan", "HOST_64BIT=", "HOST_ARM=arm7s", NULL };
  #elif defined(__aarch64__) || defined(_M_ARM64)
  const char *hcpu[] = { "HOST_CPU=arm64", "HOST_32BIT=", "HOST_64BIT=tan", "HOST_ARM=arm64", NULL };
  #elif defined(mips) || defined(__mips__) || defined(__mips)
  const char *hcpu[] = { "HOST_CPU=mips", "HOST_32BIT=tan", "HOST_64BIT=", NULL };
  #elif defined(__sh__)
  const char *hcpu[] = { "HOST_CPU=superh", "HOST_32BIT=tan", "HOST_64BIT=", NULL };
  #elif defined(__powerpc) || defined(__powerpc__) || defined(__powerpc64__) || defined(__POWERPC__) || defined(__ppc__) || defined(__PPC__) || defined(_ARCH_PPC)
  const char *hcpu[] = { "HOST_CPU=ppc", "HOST_32BIT=tan", "HOST_64BIT=", NULL };
  #elif defined(__PPC64__) || defined(__ppc64__) || defined(_ARCH_PPC64)
  const char *hcpu[] = { "HOST_CPU=ppc64", "HOST_32BIT=", "HOST_64BIT=tan", NULL };
  #elif defined(__sparc__) || defined(__sparc)
  const char *hcpu[] = { "HOST_CPU=sparc", "HOST_32BIT=tan", "HOST_64BIT=", NULL };
  #elif defined(__m68k__)
  const char *hcpu[] = { "HOST_CPU=m68k", "HOST_32BIT=tan", "HOST_64BIT=", NULL };
  #else
  #error "unknown CPU!"
  #endif
  var_defines(hcpu, 1);
}


int main (int argc, char **argv, char **arg_environ) {
  int n, num_targets;
  const char *s;
  struct option optv[N_OPTS];
  char *targets[N_TARGETS];
  const char *all = "all";
  int anyhow = 0;
  int status;
  int wasOptH = 0;
  const char *mybin = argv[0];
  if (!mybin) mybin = "k8jam";
  //
  --argc; ++argv;
  if ((num_targets = getoptions(argc, argv, "H:d:j:f:gs:t:ano:qvZW", optv, targets)) < 0) {
    printf("usage: jam [ options ] targets...\n\n"
      "-a     Build all targets, even if they are current.\n"
      "-dx    Display (a)actions (c)causes (d)dependencies\n"
      "       (m)make tree (x)commands ([+]0-9) debug levels.\n"
      "-fx    Read x instead of Jambase.\n"
      "-g     Build from newest sources first.\n"
      "-jx    Run up to x shell commands concurrently.\n"
      "-n     Don't actually execute the updating actions.\n"
      "-ox    Write the updating actions to file x.\n"
      "-q     Quit quickly as soon as a target fails.\n"
      "-sx=y  Set variable x=y, overriding environment.\n"
      "-tx    Rebuild x, even if it is up-to-date.\n"
      "-v     Print the version of jam and exit.\n"
      "-Hx    Show header cache statistics (a)ll, (h)its, (s)tats.\n"
      "-W     Write built-in Jambase and exit.\n"
      "\ndebug levels:\n"
      " 1  same as -da, but don't show quiet actions\n"
      " 2  nothing\n"
      " 3  same as -dm\n"
      " 4  execcmds()'s work\n"
      " 5  rule invocations\n"
      " 6  result of header scan, dir scan and attempts at binding\n"
      " 7  variable settings\n"
      " 8  variable fetches and expansions, 'if' calculations\n"
      " 9  list manipulation, scanner tokens, memory use\n"
      "\nmodules:\n"
      "  jammod genman infile.txt [outfile.man]\n"
    );
    exit(EXITBAD);
  }
  /* version info. */
  if ((s = getoptval(optv, 'v', 0))) {
    printf("K8Jam %s %s\n", VERSION, OSMINOR);
    printf(
      "(C) 1993-2003 Christopher Seiwald at all\n"
      "changes by Ketmar // Invisible Vector, psyc://ketmar.no-ip.org/~ketmar\n"
      "http://repo.or.cz/k8jam.git\n"
    );
    exit(EXITOK);
  }
  /* pick up interesting options */
  if ((s = getoptval(optv, 'W', 0))) {
    FILE *fl = fopen("Jambase", "w");
    if (fl != NULL) {
      printf("writing Jambase...\n");
      jambase_unpack();
      for (int f = 0; jambase[f]; ++f) fprintf(fl, "%s", jambase[f]);
      fclose(fl);
    }
    exit(EXITOK);
  }
  if ((s = getoptval(optv, 'n', 0))) ++globs.noexec, DEBUG_MAKE = DEBUG_MAKEQ = DEBUG_EXEC = 1;
  if ((s = getoptval(optv, 'q', 0))) globs.quitquick = 1;
  if ((s = getoptval(optv, 'a', 0))) ++anyhow;
  if ((s = getoptval(optv, 'j', 0))) globs.jobs = atoi(s);
  if ((s = getoptval(optv, 'g', 0))) globs.newestfirst = 1;
  /* turn on/off debugging */
  for (n = 0; (s = getoptval(optv, 'd', n)) != 0; ++n) {
    int i = atoi(s);
    /* first -d, turn off defaults. */
    if (!n) DEBUG_MAKE = DEBUG_MAKEQ = DEBUG_EXEC = 0;
    /* n turns on levels 1-n */
    /* +n turns on level n */
    /* c turns on named display c */
    if (i < 0 || i >= DEBUG_MAX) {
      printf( "Invalid debug level '%s'.\n", s );
    } else if (*s == '+') {
      globs.debug[i] = 1;
    } else if (i) {
      for (; i > 0; --i) globs.debug[i] = 1;
    } else {
      while (*s) {
        switch (*s++) {
          case 'a': DEBUG_MAKE = DEBUG_MAKEQ = 1; break;
          case 'c': DEBUG_CAUSES = 1; break;
          case 'd': DEBUG_DEPENDS = 1; break;
          case 'm': DEBUG_MAKEPROG = 1; break;
          case 'x': DEBUG_EXEC = 1; break;
          case 'h': DEBUG_HEADER = 1; break;
          case 's': DEBUG_SCAN = 1; break;
          case '0': break;
          default: printf("Invalid debug flag '%c'.\n", s[-1]);
        }
      }
    }
  }
  /* header cache statistics */
  for (n = 0; (s = getoptval(optv, 'H', n)) != 0; ++n) {
    wasOptH = 1;
    for (; *s; ++s) {
      switch (*s) {
        case 'a': optShowHCacheStats = optShowHCacheInfo = 1; break;
        case 'h': optShowHCacheInfo = 1; break;
        case 's': optShowHCacheStats = 1; break;
      }
    }
  }
  if (wasOptH && (optShowHCacheStats == 0 && optShowHCacheInfo == 0)) optShowHCacheStats = optShowHCacheInfo = 1;
  /* set JAMDATE first */
  {
    char buf[128], *p;
    time_t clock;
    //
    time(&clock);
    strcpy(buf, ctime(&clock));
    /* trim newline from date */
    p = buf+strlen(buf);
    while (p >= buf && *((unsigned char *)p) <= ' ') --p;
    *(++p) = '\0';
    var_set("JAMDATE", list_new(L0, buf, 0), VAR_SET);
  }
  /* And JAMUNAME */
#ifdef unix
  {
    struct utsname u;
    //
    if (uname(&u) >= 0) {
      LIST *l = L0;
      l = list_new(l, u.machine, 0);
      l = list_new(l, u.version, 0);
      l = list_new(l, u.release, 0);
      l = list_new(l, u.nodename, 0);
      l = list_new(l, u.sysname, 0);
      var_set("JAMUNAME", l, VAR_SET);
    }
  }
#endif /* unix */
  /* run modules */
  if (num_targets > 0 &&
      (strcmp(targets[0], "jammod") == 0 || strcmp(targets[0], "jammodule") == 0)) {
    if (num_targets < 2) {
      fprintf(stderr, "ERROR: module name missing!\n");
      exit(EXITBAD);
    }
    if (strcmp(targets[1], "genman") == 0) {
      if (num_targets < 3 || num_targets > 4) {
        fprintf(stderr, "ERROR: invalid genman module invocation!\n");
        exit(EXITBAD);
      }
      static char destmanfile[4096];
      if (num_targets == 3) {
        strcpy(destmanfile, targets[2]);
        char *ext = strrchr(destmanfile, '.');
        if (!ext || strchr(ext, '/')) {
          strcat(destmanfile, ".man");
        } else {
          strcpy(ext, ".man");
        }
      } else {
        strcpy(destmanfile, targets[3]);
      }
      if (DEBUG_EXEC) printf("jammod genman \"%s\" \"%s\"\n", targets[2], destmanfile);
      if (!globs.noexec) {
        generate_man(targets[2], destmanfile);
      }
      exit(EXITOK);
    }
    fprintf(stderr, "ERROR: unknown module '%s'!\n", targets[1]);
    exit(EXITBAD);
  }
  /* load up environment variables */
  var_defines((const char **)use_environ, 0);
  /* Jam defined variables OS, OSPLAT */
  var_defines(othersyms, 1);
  /* host CPU and bitness */
  var_define_host_cpu();
  /* load up variables set on command line. */
  for (n = 0; (s = getoptval(optv, 's', n)) != 0; ++n) {
    const char *symv[2];
    symv[0] = s;
    symv[1] = 0;
    var_defines(symv, 1);
  }
  var_set("JAM_BINARY", list_new(L0, mybin, 0), VAR_SET);
  /* add JAMCMDARGS */
  {
    LIST *l0 = L0, *l1 = L0;
    //
    if (num_targets < 1) {
      l0 = list_new(l0, "all", 0);
      l1 = list_new(l1, "all", 0);
    } else {
      for (n = 0; n < num_targets; ++n) {
        //printf("target %i: %s\n", n, targets[n]);
        l0 = list_new(l0, targets[n], 0);
        l1 = list_new(l1, targets[n], 0);
      }
    }
    var_set("JAMCMDARGS", l0, VAR_SET);
    /* k8 */
    var_set("JAM_TARGETS", l1, VAR_SET);
  }
  /* add JAMCONFIGARGS */
  {
    LIST *l0 = L0;
    //
    for (n = 0; n < num_cfgargs; ++n) l0 = list_new(l0, cfgargs[n], 0);
    var_set("JAMCONFIGARGS", l0, VAR_SET);
  }
  /* initialize built-in rules */
  load_builtins();
  /* parse ruleset */
  for (n = 0; (s = getoptval(optv, 'f', n)) != 0; ++n) parse_file(s);
  if (!n) parse_file("::Jambase");
  status = 0; /*yyanyerrors();*/
  if (status) {
    printf("FATAL: parsing error occured!\n");
    exit(EXITBAD);
  }
  /* manually touch -t targets */
  for (n = 0; (s = getoptval(optv, 't', n)) != 0; ++n) touchtarget(s);
  /* if an output file is specified, set globs.cmdout to that */
  if ((s = getoptval(optv, 'o', 0)) != 0) {
    if (!(globs.cmdout = fopen(s, "w"))) {
      printf("Failed to write to '%s'\n", s);
      exit(EXITBAD);
    }
    ++globs.noexec;
  }
  /* fix number of jobs (if necessary) */
  fixJobs();
#ifndef NO_OPT_JAM_TARGETS_VARIABLE_EXT
  /* ported from Haiku */
  /* get value of variable JAM_TARGETS and build the targets */
  {
    LIST *l = var_get("JAM_TARGETS");
    int targetCount = list_length(l);
    int i;
    //
    if (targetCount == 0) {
      /* no targets: nothing to do */
      printf("No targets. Nothing to do.\n");
      exit(EXITOK);
    }
    if (targetCount >= N_TARGETS) {
      printf("ERROR: Too many targets!\n");
      exit(EXITBAD);
    }
    for (i = 0; i < targetCount; ++i) {
      const char *s0 = l->string;
      targets[i] = malloc((strlen(s0)+1)*sizeof(char));
      if (!targets[i]) {
        printf("ERROR: Out of memory!\n");
        exit(EXITBAD);
      }
      strcpy(targets[i], s0);
      l = l->next;
    }
    num_targets = targetCount;
/*
    printf("new targets:\n");
    for (i = 0; i < num_targets; i++) {
      printf("target %i: %s\n", i, targets[i]);
    }
*/
  }
#endif
  /* now make target */
  if (!num_targets) status |= make(1, &all, anyhow);
  else status |= make(num_targets, (const char **)targets, anyhow);
  /* widely scattered cleanup */
//#ifdef OPT_HEADER_CACHE_EXT
  /* note that cache will not be written if it is not updated, so 'clean' will work ok */
  if (!globs.noexec) hcache_done();
//#endif
  regexp_done();
  var_done();
  donerules();
  donestamps();
  donestr();
  /* close cmdout */
  if (globs.cmdout) fclose(globs.cmdout);
  return (status ? EXITBAD : EXITOK);
}
