/*
 * Copyright 1993, 2000 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * headers.c - handle #includes in source files
 *
 * Using regular expressions provided as the variable $(HDRSCAN),
 * headers() searches a file for #include files and phonies up a
 * rule invocation:
 *
 *  $(HDRRULE) <target> : <include files> ;
 *
 * External routines:
 *    headers() - scan a target for include files and call HDRRULE
 *
 * Internal routines:
 *    headers1() - using regexp, scan a file and build include LIST
 */
#include "jam.h"
#include "lists.h"
#include "parse.h"
#include "compile.h"
#include "rules.h"
#include "variable.h"
#include "re9.h"
#include "headers.h"
#include "newstr.h"
#include "hdrmacro.h"
#include "hcache.h"
#include "dstrings.h"
#include "gdcdeps.h"


/*
#ifndef OPT_HEADER_CACHE_EXT
static LIST *headers1 (const char *file, LIST *hdrscan);
#endif
*/


/*
 * headers() - scan a target for include files and call HDRRULE
 */
#define MAXINC  (32)

void headers (TARGET *t) {
  LIST *hdrscan;
  LIST *hdrrule;
  LOL lol;
  if (!(hdrscan = var_get("HDRSCAN")) || !(hdrrule = var_get("HDRRULE"))) return;
  /* doctor up call to HDRRULE rule */
  /* call headers1() to get LIST of included files */
  if (DEBUG_HEADER) printf("header scan %s\n", t->name);
  lol_init(&lol);
  lol_add(&lol, list_new(L0, t->name, 1));
/*
#ifdef OPT_HEADER_CACHE_EXT
  lol_add(&lol, hcache(t, hdrscan));
#else
  lol_add(&lol, headers1(t->boundname, hdrscan));
#endif
*/
  lol_add(&lol, hcache(t, hdrscan));
  if (lol_get(&lol, 1)) list_free(evaluate_rule(hdrrule->string, &lol, L0));
  /* clean up */
  lol_free(&lol);
}


struct lcicacheentry_s {
  const char *str;
  const char *val;
  struct lcicacheentry_s *children;
  struct lcicacheentry_s *next;
};
typedef struct lcicacheentry_s lcicacheentry_t;

static lcicacheentry_t *lcicache = NULL;


const char *lci_cache_find (const char *basefile, int bflen, dstring_t *fname) {
  if (basefile == NULL || basefile[0] == 0 || bflen < 1) return NULL;
  if (fname == NULL || dstr_cstr(fname)[0] == 0) return NULL;
  for (lcicacheentry_t *e0 = lcicache; e0 != NULL; e0 = e0->next) {
    int slen = strlen(e0->str);
    if (slen == bflen && memcmp(e0->str, basefile, slen) == 0) {
      // found bucket
      const char *fnc = dstr_cstr(fname);
      for (lcicacheentry_t *e1 = e0->children; e1 != NULL; e1 = e1->next) {
        if (strcmp(e1->str, fnc) == 0) return e1->val;
      }
    }
  }
  return NULL;
}


void lci_cache_put (const char *basefile, int bflen, dstring_t *fname, dstring_t *xname) {
  if (basefile == NULL || basefile[0] == 0 || bflen < 1) return;
  if (fname == NULL || dstr_cstr(fname)[0] == 0) return;
  if (xname == NULL) return;
  // create final entry
  lcicacheentry_t *ne = calloc(1, sizeof(lcicacheentry_t));
  ne->str = strdup(dstr_cstr(fname));
  ne->val = strdup(dstr_cstr(xname));
  // find bucket
  for (lcicacheentry_t *e0 = lcicache; e0 != NULL; e0 = e0->next) {
    int slen = strlen(e0->str);
    if (slen == bflen && memcmp(e0->str, basefile, slen) == 0) {
      // found bucket
      ne->next = e0->children;
      e0->children = ne;
      return;
    }
  }
  // create new bucket
  lcicacheentry_t *bk = calloc(1, sizeof(lcicacheentry_t));
  bk->str = strdup(basefile);
  bk->children = ne;
  bk->next = lcicache;
  lcicache = bk;
}


static void resolve_local_include (dstring_t *dest, const char *basefile, dstring_t *fname) {
  dstr_init(dest);
  if (dstr_cstr(fname)[0] == '/') { dstr_set_cstr(dest, dstr_cstr(fname)); return; }
  int bflen = strlen(basefile);
  while (bflen > 0 && basefile[bflen-1] != '/') --bflen;
  if (bflen == 0) { dstr_set_cstr(dest, dstr_cstr(fname)); return; }
  // check cache
  const char *riname = lci_cache_find(basefile, bflen, fname);
  if (riname != NULL) {
    // i found her!
    dstr_set_cstr(dest, riname);
    return;
  }
  // build name
  /*
  if (basefile[0] != '/') {
    static char buf[8192];
    char *dir = getcwd(buf, sizeof(buf));
    if (dir && dir[0]) {
      dstr_push_cstr(dest, dir);
      if (dir[strlen(dir)-1] != '/') dstr_push_char(dest, '/');
    }
  }
  */
  dstr_push_buf(dest, basefile, bflen);
  dstr_push_cstr(dest, dstr_cstr(fname));
  if (access(dstr_cstr(dest), R_OK) != 0) dstr_set_cstr(dest, dstr_cstr(fname));
  // put it in cache
  lci_cache_put(basefile, bflen, fname, dest);
}


/*
 * headers1() - using regexp, scan a file and build include LIST
 */
/*
#ifndef OPT_HEADER_CACHE_EXT
static
#endif
*/
LIST *headers1 (const char *file, LIST *hdrscan) {
  FILE  *f;
  int i;
  int rec = 0;
  LIST *result = NULL;
  regexp_t *re[MAXINC];
  regexp_t *re_macros, *re_dlinc = NULL;
  re9_sub_t mt[4];
  int line_size = 16384;
  char *buf; /* line_size size */
  dstring_t buf2, buf3;
  int dlang = 0;
  for (const LIST *t = var_get("HDRPATH_IGNORE"); t != NULL; t = t->next) {
    if (t->string && t->string[0]) {
      if (strncmp(file, t->string, strlen(t->string)) == 0) {
        if (DEBUG_HEADER) printf("DBG: ignoring header [%s]\n", file);
        return result;
      }
    }
  }
  {
    char *t = strrchr(file, '.');
    dlang = (t != NULL && (strcmp(t, ".d") == 0 || strcmp(t, ".di") == 0));
  }
  if (DEBUG_HEADER) printf("DBG: trying header [%s] (D=%d)\n", file, dlang);
  //fprintf(stderr, "DBG: trying header [%s] (D=%d)\n", file, dlang);
  if (dlang) {
    // try GDC
    /*
    {
      LIST *gdc = var_get("GDC");
      fprintf(stderr, "GDC: <"); list_print_ex(stderr, gdc, LPFLAG_NO_TRSPACE); fprintf(stderr, ">\n");
      LIST *gdcfa = var_get("GDCFLAGS.all");
      fprintf(stderr, "GDCFLAGS.all: <"); list_print_ex(stderr, gdcfa, LPFLAG_NO_TRSPACE); fprintf(stderr, ">\n");
      LIST *gdcf = var_get("GDCFLAGS");
      fprintf(stderr, "GDCFLAGS: <"); list_print_ex(stderr, gdcf, LPFLAG_NO_TRSPACE); fprintf(stderr, ">\n");
      LIST *t;
      t = var_get("TOP");
      fprintf(stderr, "TOP: <"); list_print_ex(stderr, t, LPFLAG_NO_TRSPACE); fprintf(stderr, ">\n");
      t = var_get("SUBDIR_TOKENS");
      fprintf(stderr, "SUBDIR_TOKENS: <"); list_print_ex(stderr, t, LPFLAG_NO_TRSPACE); fprintf(stderr, ">\n");
      t = var_get("HDRS");
      fprintf(stderr, "HDRS: <"); list_print_ex(stderr, t, LPFLAG_NO_TRSPACE); fprintf(stderr, ">\n");
    }
    fprintf(stderr, "DBG: trying header [%s] (D=%d)\n", file, dlang);
    */
    if (DEBUG_HEADER) printf("trying GDC deps for [%s]...\n", file);
    result = gdcDeps(file);
    if (result != NULL) {
      if (DEBUG_HEADER) {
        printf("GDC deps for [%s]\n", file);
        for (const LIST *l = result; l != NULL; l = l->next) printf("  <%s>\n", l->string);
      }
      return result;
    }
  }
  if (!(f = fopen(file, "r"))) return result;
  //fprintf(stderr, "DBG: processing header [%s] (D=%d)\n", file, dlang);
  /*
  {
    LIST *t = var_get("DLANG");
    dlang = (t != NULL && t->string[0]);
    //fprintf(stderr, "DLANG: %d\n", dlang);
  }
  */
  if (dlang) {
    re[rec++] = regexp_compile("\\bimport\\s+([^;]+)\\s*;", 0); // don't use ".+n?;", it's buggy
    re[rec++] = regexp_compile("\\bimport\\s*\\(([^)]+)\\)", 0);
    re_dlinc = regexp_compile("\\s*([^,;]+)\\s*", 0);
  }
  if (!dlang) {
    while (rec < MAXINC && hdrscan) {
      //printf("header re %d: [%s]\n", rec, hdrscan->string);
      re[rec] = regexp_compile(hdrscan->string, 0);
      ++rec;
      hdrscan = list_next(hdrscan);
    }
  }
  /* the following regexp is used to detect cases where a  */
  /* file is included through a line line "#include MACRO" */
  re_macros = regexp_compile("^\\s*#\\s*include\\s+([A-Za-z_][A-Za-z0-9_]*).*$", 0);
  if (DEBUG_HEADER) printf("header processing: [%s] (%d)\n", file, rec);
  if ((buf = malloc(line_size)) == NULL) { fprintf(stderr, "FATAL: out of memory!\n"); abort(); }
  dstr_init(&buf2);
  dstr_init(&buf3);
  while (fgets(buf, line_size, f)) {
    for (i = 0; i < rec; ++i) {
      mt[0].sp = mt[0].ep = NULL;
      if (regexp_execute(re[i], buf, mt, 2) > 0 && mt[1].sp != NULL) {
        /* copy and terminate extracted string */
        int l = mt[1].ep-mt[1].sp;
        dstr_set_buf(&buf2, mt[1].sp, l);
        if (dlang && i <= 1) {
          //fprintf(stderr, "dlang: [%s]\n", dstr_cstr(&buf2));
          if (i == 0) {
            /* this is DLang 'import' directive */
            const char *s = dstr_cstr(&buf2);
            //fprintf(stderr, "=== %d; [%s]\n", l, dstr_cstr(&buf2));
            while (*s) {
              char *p, *t;
              int againD = 0;
              if (!regexp_execute(re_dlinc, s, mt, 2) || mt[1].sp == NULL) break;
              dstr_set_buf(&buf3, mt[1].sp, mt[1].ep-mt[1].sp);
              dstr_push_cstr(&buf3, ".d");
              p = dstr_cstr(&buf3);
              //fprintf(stderr, "*** [%s]\n", dstr_cstr(&buf3));
              //fprintf(stderr, "+++000 [%s]\n", dstr_cstr(&buf3));
              if ((t = strchr(p, ':')) != NULL) { againD = 1; *t = 0; }
              if ((t = strchr(p, '=')) != NULL) p = t+1;
              //fprintf(stderr, "+++001 [%s]\n", dstr_cstr(&buf3));
              while (*p && isspace(*p)) ++p;
              if (*p) {
                t = p+strlen(p);
                while (t > p && isspace(t[-1])) --t;
                *t = 0;
              }
              // add ".d" again if it was cut
              // note that we have always place for it
              if (*p && againD) strcat(p, ".d");
              //fprintf(stderr, "+++002 [%s]\n", dstr_cstr(&buf3));
              if (*p) {
                for (t = p; *t; ++t) if (*t == '.') *t = '/';
                //fprintf(stderr, "+++ [%s]\n", dstr_cstr(&buf3));
                *(strrchr(p, '/')) = '.'; // restore '.d'
                //fprintf(stderr, "dlang include: [%s]\n", p);
                result = list_new(result, p, 0);
                // add name/package.d too
                char *pd = malloc(strlen(p)+32);
                strcpy(pd, p);
                strcpy(strrchr(pd, '.'), "/package.d");
                //fprintf(stderr, "dlang include pkg: [%s]\n", pd);
                result = list_new(result, pd, 0);
                free(pd);
              }
              s = mt[1].ep;
            }
          } else {
            /* this is DLang import() operator */
            /* TODO: process string escapes */
            //fprintf(stderr, "II: [%s]\n", dstr_cstr(&buf2));
            if (dstr_cstr(&buf2)[0] == '"' || dstr_cstr(&buf2)[0] == '`') {
              char *fn = strdup(dstr_cstr(&buf2)+1);
              if (fn[0] && fn[1] && fn[strlen(fn)-1] == dstr_cstr(&buf2)[dstr_len(&buf2)-1]) {
                fn[strlen(fn)-1] = 0;
                //fprintf(stderr, "import directive: [%s]\n", fn);
                result = list_new(result, fn, 0);
              }
              free(fn);
            }
          }
        } else {
          // check if this is local include
          dstring_t xname;
          resolve_local_include(&xname, file, &buf2);
          //fprintf(stderr, "FILE: <%s>; include: <%s>\n", file, dstr_cstr(&xname));
          result = list_new(result, dstr_cstr(&xname), 0);
          dstr_done(&xname);
        }
        if (DEBUG_HEADER) printf("header found: %s\n", dstr_cstr(&buf2));
      }
    }
    /* special treatment for #include MACRO */
    mt[0].sp = mt[0].ep = NULL;
    if (regexp_execute(re_macros, buf, mt, 2) > 0 && mt[1].sp != NULL) {
      const char *header_filename;
      int l = mt[1].ep-mt[1].sp;
      dstr_set_buf(&buf2, mt[1].sp, l);
      if (DEBUG_HEADER) printf("macro header found: %s", dstr_cstr(&buf2));
      header_filename = macro_header_get(dstr_cstr(&buf2));
      if (header_filename) {
        if (DEBUG_HEADER) printf(" resolved to '%s'\n", header_filename);
        result = list_new(result, header_filename, 0);
      } else {
        if (DEBUG_HEADER) printf(" ignored !!\n");
      }
    }
  }
  dstr_done(&buf3);
  dstr_done(&buf2);
  free(buf);
  fclose(f);
  if (re_dlinc != NULL) regexp_free(re_dlinc);
  regexp_free(re_macros);
  while (--rec >= 0) regexp_free(re[rec]);
  return result;
}
