/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * variable.c - handle jam multi-element variables
 *
 * External routines:
 *
 *  var_defines() - load a bunch of variable=value settings
 *  var_string() - expand a string with variables in it
 *  var_get() - get value of a user defined symbol
 *  var_set() - set a variable in jam's user defined symbol table
 *  var_swap() - swap a variable's value with the given one
 *  var_done() - free variable tables
 *
 * Internal routines:
 *
 *  var_enter() - make new var symbol table entry, returning var ptr
 *  var_dump() - dump a variable to stdout
 */
#include "jam.h"
#include "lists.h"
#include "parse.h"
#include "variable.h"
#include "expand.h"
#include "hash.h"
#include "newstr.h"
#include "matchglob.h"
#include "dstrings.h"


static struct hash *varhash = NULL;


/*
 * VARIABLE - a user defined multi-value variable
 */

typedef struct _variable VARIABLE;
struct _variable {
  const char *symbol;
  LIST *value;
};


/*
 * var_dump() - dump a variable to stdout
 */
static void var_dump (const char *symbol, LIST *value, const char *what) {
  printf("%s %s = ", what, symbol);
  list_print_ex(stdout, value, LPFLAG_NO_TRSPACE);
  printf("\n");
}


/*
 * var_enter() - make new var symbol table entry, returning var ptr
 */
static VARIABLE *var_enter (const char *symbol) {
  VARIABLE var, *v = &var;
  if (!varhash) varhash = hashinit(sizeof(VARIABLE), "variables");
  v->symbol = symbol;
  v->value = 0;
  if (hashenter(varhash, (HASHDATA **)&v)) v->symbol = newstr(symbol); /* never freed */
  return v;
}


/* case-insensitive */
static inline int globhit (const char *list[], const char *str) {
  for (; *list; ++list) {
    //fprintf(stderr, "[%s]:[%s]:%d\n", *list, str, matchglobex(*list, str, 0));
    if (matchglobex(*list, str, 0) == 0) return 1;
  }
  return 0;
}


/*
 * var_defines()
 */
void var_defines (const char **e, int dontignore) {
  static const char *pathsplits[] = {
    "*PATH",
    "INCLUDE",
    "LIBS",
    NULL
  };
  static const char *varignores[] = {
    "ANT_*",
    "BASH*",
    "LS_*", /* various options for ls */
    "PROMPT_*",
    "PS[0-9]",
    "WINDOWPATH",
    "OS",
    "OSPLAT",
    "_",
    NULL
  };
  for (; *e; ++e) {
    const char *val = strchr(*e, '=');
    /* Just say "no": on Unix, variables can contain function
     * definitions. their value begins with "()"
     */
    if (val != NULL && val[1] == '(' && val[2] == ')') continue;
    if (val != NULL) {
      LIST *l = L0;
      const char *pp, *p;
      char split = ' ';
      int op = VAR_SET;
      dstring_t buf, name;
      dstr_init_buf(&buf, *e, val-*e);
      /* get name */
      dstr_init_buf(&name, *e, val-*e);
      if (val > *e) {
        /* is this '+=', '-=' or '?=' operator? */
        switch (val[-1]) {
          case '+': op = VAR_APPEND; break;
          case '-': op = VAR_REMOVE; break;
          case '?': op = VAR_DEFAULT; break;
        }
        if (op != VAR_SET) dstr_pop_char(&name);
      }
      if (!dontignore && globhit(varignores, dstr_cstr(&name))) continue; /* ignore this */
      /* split *PATH at :'s, not spaces */
      if (val-4 >= *e) {
        if (globhit(pathsplits, dstr_cstr(&name))) {
          split = (strncmp(dstr_cstr(&name), "LUA_", 4) == 0 ? ';' : SPLITPATH); /* special for LUA_XXX */
        }
      }
      /* do the split */
      for (pp = val+1; (p = strchr(pp, split)); pp = p+1) {
        int len = p-pp;
        if (len > 0) {
          dstr_clear(&buf);
          dstr_push_buf(&buf, pp, len);
          l = list_new(l, dstr_cstr(&buf), 0);
        }
      }
      /* get name */
      l = list_new(l, pp, 0);
      var_set(dstr_cstr(&name), l, op);
      dstr_done(&buf);
      dstr_done(&name);
    }
  }
}


/*
 * var_string()
 */
int var_string (const char *in, dstring_t *out, LOL *lol, char separator) {
  int osz = dstr_len(out), lwpos;
  const char *st;
  while (*in) {
    int dollar = 0;
    /* copy white space */
    st = in;
    while (isspace(*in)) ++in;
    dstr_push_memrange(out, st, in);
    lwpos = dstr_len(out);
    /* copy non-white space, watching for variables */
    st = in;
    while (*in && !isspace(*in)) {
      if (in[0] == '$' && in[1] == '(') ++dollar;
      ++in;
    }
    dstr_push_memrange(out, st, in);
    /* if a variable encountered, expand it and and embed the space-separated members of the list in the output */
    if (dollar) {
      LIST *l = var_expand(dstr_cstr(out)+lwpos, dstr_cstr(out)+dstr_len(out), lol, 0), *hd = l;
      dstr_chop(out, lwpos);
      while (l != NULL) {
        dstr_push_cstr(out, l->string);
        /* separate with space */
        if ((l = list_next(l)) != NULL) dstr_push_char(out, separator);
      }
      list_free(hd);
    }
  }
  return dstr_len(out)-osz;
}


/*
 * var_get()
 */
LIST *var_get (const char *symbol) {
  VARIABLE var, *v = &var;
  v->symbol = symbol;
  if (varhash && hashcheck(varhash, (HASHDATA **)&v)) {
    if (DEBUG_VARGET) var_dump(v->symbol, v->value, "get");
    return v->value;
  }
  return 0;
}


/*
 * var_set()
 */
void var_set (const char *symbol, LIST *value, int flag) {
  VARIABLE *v = var_enter(symbol);
  if (DEBUG_VARSET) var_dump(symbol, value, "set");
  switch(flag) {
    case VAR_SET:
      /* replace value */
      list_free(v->value);
      v->value = value;
      break;
    case VAR_APPEND:
      /* append value */
      v->value = list_append(v->value, value);
      break;
    case VAR_REMOVE:
      /* remove all values */
      v->value = list_removeall(v->value, value);
      list_free(value);
      break;
    case VAR_DEFAULT:
      /* set only if unset */
      if (!v->value) v->value = value; else list_free(value);
      break;
  }
}


/*
 * var_swap()
 */
LIST *var_swap (const char *symbol, LIST *value) {
  VARIABLE *v = var_enter(symbol);
  LIST *oldvalue = v->value;
  if (DEBUG_VARSET) var_dump(symbol, value, "set");
  v->value = value;
  return oldvalue;
}


/*
 * var_done()
 */
void var_done (void) {
  hashdone(varhash);
}
