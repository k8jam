/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * scan.h - the jam yacc scanner
 *
 * The yymode() function is for the parser to adjust the lexicon of the
 * scanner.  Aside from normal keyword scanning, there is a mode to
 * handle action strings (look only for the closing }) and a mode to
 * ignore most keywords when looking for a punctuation keyword.  This
 * allows non-punctuation keywords to be used in lists without quoting.
 */
#ifndef JAMH_SCAN_H
#define JAMH_SCAN_H


/*
 * token_t - value of a lexical token
 */
typedef struct {
  int type;
  const char *string;
  int strlit; /* !0: don't do variable expansion in string */
  PARSE *parse;
  LIST *list;
  int number;
  int line;
  int pos;
  const char *file; // newstr()
} token_t;

extern token_t yylval;

extern void yyerror (const token_t *tk, const char *s);
extern void yyfparse (const char *s);
extern int yylex (void);


enum {
  SCAN_NORMAL, /* normal parsing */
  SCAN_STRING, /* look only for matching '}' */
  SCAN_PUNCT   /* only punctuation keywords */
};

extern void yymode (int n);


#endif
