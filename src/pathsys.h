/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * pathsys.h - PATHNAME struct
 */
#ifndef JAMH_PATHSYS_H
#define JAMH_PATHSYS_H

/*
 * PATHNAME - a name of a file, broken into <grist>dir/base/suffix(member)
 *
 * <grist> is salt to distinguish between targets that otherwise would
 * have the same name:  it never appears in the bound name of a target.
 * (member) is an archive member name: the syntax is arbitrary, but must
 * agree in path_parse(), path_build() and the Jambase.
 *
 * On VMS, we keep track of whether the original path was a directory
 * (without a file), so that $(VAR:D) can climb to the parent.
 */

typedef struct _pathname PATHNAME;
typedef struct _pathpart PATHPART;

struct _pathpart {
  const char *ptr;
  int len;
};


#define f_grist   part[0]
#define f_root    part[1]
#define f_dir     part[2]
#define f_base    part[3]
#define f_suffix  part[4]
#define f_member  part[5]

struct _pathname {
  PATHPART part[6];
};


extern void path_build (char *file, const PATHNAME *f);
extern void path_parse (const char *file, PATHNAME *f);
extern void path_parent (PATHNAME *f);

/* if 'pwd'is NULL, use getcwd() */
extern char *normalize_path (const char *path, char *buffer, size_t bufferSize, const char *pwd);


#endif
