/*
 * Copyright 1993-2002 Christopher Seiwald and Perforce Software, Inc.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * filesys.h - OS specific file routines
 */
#ifndef JAMH_FILESYS_H
#define JAMH_FILESYS_H

#define ARMAG   "!<arch>\n"
#define SARMAG  8
#define ARFMAG  "`\n"

#if defined(__ppc__) || defined (__ppc64__)
#include <ar.h>
#endif

typedef void (*scanback) (void *closure, const char *file, int found, time_t t);

extern void file_dirscan (const char *dir, scanback func, void *closure);
extern void file_archscan (const char *arch, scanback func, void *closure);

extern int file_time (const char *filename, time_t *time);

// <0: error; 0: regular; 1: directory
extern int file_type (const char *diskname);


#endif
