/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __DSTRINGS_H__
#define __DSTRINGS_H__

#ifdef __cplusplus
extern "C" {
#endif

/* non-GCC compilers sux! */
#if defined(__GNUC__) && __GNUC__ > 2
# define DSTRINGS_PURE         __attribute__((pure))
# define DSTRINGS_CONST        __attribute__((const))
# define DSTRINGS_NORETURN     __attribute__((noreturn))
# define DSTRINGS_PRINTF(m,n)  __attribute__((format(printf,m,n)))
# define DSTRINGS_SENTINEL     __attribute__((sentinel))
# define DSTRINGS_CLEANUP      __attribute__((cleanup(dstr_done)))
# define DSTRINGS_PCLEANUP     __attribute__((cleanup(dstr_donep)))
#else
# define DSTRINGS_PURE
# define DSTRINGS_CONST
# define DSTRINGS_NORETURN
# define DSTRINGS_PRINTF(m,n)
# define DSTRINGS_SENTINEL
# define DSTRINGS_CLEANUP
# define DSTRINGS_PCLEANUP
#endif


/* string is always 0-terminated */
typedef struct {
  char *str;
  int len;
  int size;
  char sbuf[256];
} dstring_t;


extern void dstr_init (dstring_t *s);
extern void dstr_init_cstr (dstring_t *s, const void *cstr);
extern void dstr_init_buf (dstring_t *s, const void *start, int len); /* len<0: use strlen() */
extern void dstr_init_memrange (dstring_t *s, const void *start, const void *finish); /* *finish will not be included */
extern void dstr_done (dstring_t *s);
extern void dstr_clear (dstring_t *s); /* clear allocated string; will not free allocated memory though */
extern void dstr_empty (dstring_t *s); /* clear allocated string; will free allocated memory */
extern void dstr_reserve (dstring_t *s, int size); /* 'size' should take trailing '\0' into account */
extern void dstr_set_cstr (dstring_t *s, const void *cstr); /* does dstr_clear() first */
extern void dstr_set_buf (dstring_t *s, const void *start, int len); /* len<0: use strlen(); does dstr_clear() first */
extern void dstr_set_memrange (dstring_t *s, const void *start, const void *finish); /* *finish will not be included; does dstr_clear() first */
extern void dstr_push_cstr (dstring_t *s, const void *str);
extern void dstr_push_buf (dstring_t *s, const void *start, int len); /* len<0: use strlen() */
extern void dstr_push_memrange (dstring_t *s, const void *start, const void *finish); /* *finish will not be included */
extern void dstr_chop (dstring_t *s, int n); /* set string length to min(n, s->len), but not less than 0 */
extern int dstr_pop_char (dstring_t *s); /* returns 'popped' char (0 if string is empty) */
extern void dstr_push_char (dstring_t *s, int x);
extern char dstr_last_char (dstring_t *s) DSTRINGS_PURE; /* return last char (0 if string is empty) */
extern char *dstr_cstr (dstring_t *s) DSTRINGS_PURE;
extern int dstr_len (dstring_t *s) DSTRINGS_PURE;

static inline void dstr_done_p (dstring_t **sp) { dstr_done(*sp); }


#ifdef __cplusplus
}
#endif
#endif
