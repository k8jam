/*
 * Copyright 1993, 1995 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * execcmd.h - execute a shell script
 */
#ifndef JAMH_EXECCMD_H
#define JAMH_EXECCMD_H


extern void execcmd (const char *string, void (*func)(void *closure, int status), void *closure, LIST *shell);
extern int execwait (void);


#define EXEC_CMD_OK    (0)
#define EXEC_CMD_FAIL  (1)
#define EXEC_CMD_INTR  (2)


#endif
