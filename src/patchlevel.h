/* Keep JAMVERSYM in sync with VERSION. */
/* It can be accessed as $(K8_JAMVERSION) in the Jamfile. */

#define VERSION "2.7.0"
#define JAMVERSYM "K8_JAMVERSION=2.7.0"
#define JAMVERSYM_HI "K8_JAMVERSION_HI=2"
#define JAMVERSYM_MID "K8_JAMVERSION_MID=7"
#define JAMVERSYM_LO "K8_JAMVERSION_LO=0"
