/*
 * Copyright 1993, 2000 Christopher Seiwald.
 * This file is part of Jam - see jam.c for Copyright information.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * hdrmacro.c - handle header files that define macros used in
 *              #include statements.
 *
 *  we look for lines like "#define MACRO  <....>" or '#define MACRO  "    "'
 *  in the target file. When found, we
 *
 *  we then phony up a rule invocation like:
 *
 *  $(HDRRULE) <target> : <resolved included files> ;
 *
 * External routines:
 *    headers1() - scan a target for "#include MACRO" lines and try
 *                 to resolve them when needed
 *
 * Internal routines:
 *    headers1() - using regexp, scan a file and build include LIST
 */
#include "jam.h"
#include "lists.h"
#include "parse.h"
#include "compile.h"
#include "rules.h"
#include "variable.h"
#include "re9.h"
#include "hdrmacro.h"
#include "hash.h"
#include "newstr.h"
#include "dstrings.h"


/* this type is used to store a dictionary of file header macros */
typedef struct header_macro {
  const char *symbol;
  const char *filename; /* we could maybe use a LIST here ?? */
} HEADER_MACRO;

static struct hash *header_macros_hash = 0;


/*
 * headers() - scan a target for include files and call HDRRULE
 */
//#define MAXINC  (10)

void macro_headers (TARGET *t) {
  regexp_t *re;
  re9_sub_t mt[4];
  int line_size = 16384;
  char *buf; /* line_size size */
  dstring_t buf1, buf2;
  FILE *f;
  if (DEBUG_HEADER) printf("macro header scan for %s\n", t->name);
  /* this regexp is used to detect lines of the form */
  /* "#define MACRO <....>" or "#define MACRO "....." */
  /* in the header macro files */
  if ((f = fopen(t->boundname, "r")) == 0) return;
  re = regexp_compile("^\\s*#\\s*define\\s+([A-Za-z_][A-Za-z0-9_]*)\\s+[<\"]([^\">]+)[\">].*$", 0);
  if ((buf = malloc(line_size)) == NULL) { fprintf(stderr, "FATAL: out of memory!\n"); abort(); }
  dstr_init(&buf1);
  dstr_init(&buf2);
  while (fgets(buf, sizeof(buf), f)) {
    HEADER_MACRO var, *v = &var;
    mt[0].sp = mt[0].ep = NULL;
    if (regexp_execute(re, buf, mt, 4) > 0) {
      int l1 = mt[1].ep-mt[1].sp;
      int l2 = mt[2].ep-mt[2].sp;
      if (l1 > 0 && l2 > 0) {
        dstr_set_buf(&buf1, mt[1].sp, l1);
        dstr_set_buf(&buf2, mt[2].sp, l2);
        /* we detected a line that looks like "#define MACRO filename */
        if (DEBUG_HEADER) printf("macro '%s' used to define filename '%s' in '%s'\n", dstr_cstr(&buf1), dstr_cstr(&buf2), t->boundname);
        /* add macro definition to hash table */
        if (!header_macros_hash) header_macros_hash = hashinit(sizeof(HEADER_MACRO), "hdrmacros");
        v->symbol = (const char *)dstr_cstr(&buf1);
        v->filename = 0;
        if (hashenter(header_macros_hash, (HASHDATA **)&v)) {
          v->symbol = newstr(dstr_cstr(&buf1)); /* never freed */
          v->filename = newstr(dstr_cstr(&buf2)); /* never freed */
        }
      }
      /* XXXX: FOR NOW, WE IGNORE MULTIPLE MACRO DEFINITIONS! */
      /*       WE MIGHT AS WELL USE A LIST TO STORE THEM */
    }
  }
  dstr_done(&buf2);
  dstr_done(&buf1);
  free(buf);
  fclose(f);
  regexp_free(re);
}


const char *macro_header_get (const char *macro_name) {
  HEADER_MACRO var, *v = &var;
  v->symbol = (char*)macro_name;
  if (header_macros_hash && hashcheck(header_macros_hash, (HASHDATA **)&v)) {
    if (DEBUG_HEADER) printf("### macro '%s' evaluated to '%s'\n", macro_name, v->filename);
    return (const char *)v->filename;
  }
  return 0;
}
