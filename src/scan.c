/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * scan.c - the jam yacc scanner
 */
#include "jam.h"
#include "lists.h"
#include "parse.h"
#include "scan.h"
#include "jamgram.h"
#include "jambase.h"
#include "newstr.h"
#include "dstrings.h"


token_t yylval;


typedef struct {
  const char *word;
  int type;
} keyword_t;


static const keyword_t keywords[] = {
#include "jamgramtab.h"
  {0,0}
};


typedef struct include_s {
  struct include_s *next; /* next serial include file */
  const char *string; /* pointer into current line */
  char **strings; /* for yyfparse() -- text to parse */
  FILE *file; /* for yyfparse() -- file being read */
  const char *fname; /* for yyfparse() -- file name */
  int line; /* line counter for error messages */
  int pos; /* position for error messages */
  //int back_count; /* # of yyunget()ed chars */
  //char back_chars[2]; /* buffer for yyunget()ed chars */
  char *fcontents; /* for yyfparse() -- file contents */
  int prevwasn; /* !0: increment line and reset to 0 */
} include_t;

static include_t *incp = NULL; /* current file; head of chain */

/* hack to stop segfaulting when last string contains ';' without space before it */
static int s_back_count = 0; /* # of yyunget()ed chars */
static char s_back_chars[2]; /* buffer for yyunget()ed chars */


static int scan_mode = SCAN_NORMAL;
/*static int any_errors = 0;*/

static const char *symdump (const token_t *s);


#ifndef NDEBUG
static const char *mnames[] = {
  "SCAN_NORMAL",
  "SCAN_STRING",
  "SCAN_PUNCT",
};
#endif


/*
 * Set parser mode: normal, string, or keyword
 */
void yymode (int n) {
#ifndef NDEBUG
  if (DEBUG_SCAN && scan_mode != n) printf("**MODE TRANSITION: %s --> %s\n", mnames[scan_mode], mnames[n]);
#endif
  scan_mode = n;
}


void yyerror (const token_t *tk, const char *s) {
  printf("ERROR(%d:%d) '%s': %s\n", tk->line, tk->pos, tk->file, s);
  exit(EXITBAD); /* exit now */
}


static void yywarning_ex (const char *s) {
  printf("WARNING(%d:%d) '%s': %s\n", incp->line, incp->pos, incp->fname, s);
}


void yyfparse (const char *s) {
  include_t *i = (include_t *)malloc(sizeof(*i));
  /* push this onto the incp chain */
  i->string = "";
  i->strings = NULL;
  i->file = NULL;
  //i->fname = strdup(s);
  i->fname = newstr(s);
  i->line = 0;
  i->pos = 0;
  i->next = incp;
  //i->back_count = 0;
  i->fcontents = NULL;
  i->prevwasn = 1;
  incp = i;
  /* if the filename is "::Jambase", it means use the internal jambase */
  if (strcmp(s, "::Jambase") == 0) {
    jambase_unpack();
    i->strings = jambase;
  }
}


/*
 * yychar() - read new line and return first character
 *
 * fabricates a continuous stream of characters across include files, returning EOF at the bitter end
 */
static int yychar (void) {
  include_t *i = incp;
  if (s_back_count) {
    //fprintf(stderr, "GET unget: %d\n", s_back_chars[s_back_count-1]);
    return s_back_chars[--s_back_count];
  }
  if (!incp) return EOF;
  /* once we start reading from the input stream, we reset the
   * include insertion point so that the next include file becomes
   * the head of the list */
  /* if there is more data in this line, return it */
  if (i->prevwasn) { i->prevwasn = 0; ++i->line; i->pos = 0; }
again:
  ++i->pos;
  if (*i->string) {
    #if 0
    if (i->fcontents != NULL) {
      fprintf(stderr, "...: (%d:%d) pos=%u; <%s>\n", i->line, i->pos, (int)(ptrdiff_t)(i->string-i->fcontents), i->fname);
    }
    #endif
    if (*i->string == '\n') i->prevwasn = 1;
    return *i->string++;
  }
  /* if we're reading from an internal string list, go to the next string */
  if (i->strings) {
    if (!*i->strings) goto next;
    i->string = *(i->strings++);
    return *i->string++;
  }
  /* if necessary, open the file and get file contents */
  if (!i->file) {
    FILE *f;
    long fsize;
    #if 0
    fprintf(stderr, "OPENING: <%s>\n", i->fname);
    #endif
    if ((f = fopen(i->fname, "rb")) == NULL) perror(i->fname);
    i->file = f;
    if (fseek(f, 0, SEEK_END) < 0) perror(i->fname);
    if ((fsize = ftell(f)) < 0) perror(i->fname);
    if (fseek(f, 0, SEEK_SET) < 0) perror(i->fname);
    uint32_t xalsz;
    if (fsize > 1024*1024*64) {
      fprintf(stderr, "FATAL: input file (%s) too big!\n", i->fname);
      abort();
    }
    xalsz = (uint32_t)fsize;
    // fuck off, g-shit-cc!
    if (xalsz > 1024*1024*64) {
      fprintf(stderr, "FATAL: input file (%s) too big!\n", i->fname);
      abort();
    }
    i->fcontents = calloc(1, xalsz+2U);
    if (fsize > 0 && fread(i->fcontents, xalsz, 1, f) != 1) perror(i->fname);
    fclose(f); /* don't need to hold it open */
    /*k8: hack, because i don't understand why it doesn't work sometimes */
    i->fcontents[xalsz] = '\n';
    i->fcontents[xalsz+1] = 0;
    i->string = i->fcontents;
    goto again;
  }
next:
  /* this include is done */
  /* free it up and return EOF so yyparse() returns to parse_file() */
  incp = i->next;
  /* close file, free name */
  if (i->fcontents != NULL) {
    #if 0
    fprintf(stderr, "DONE-INC: <%s>\n", i->fname);
    #endif
    free(i->fcontents);
  }
  //if (i->fname != NULL) free(i->fname);
  free(i);
  return EOF;
}


/*
 *  yychar() - back up one character
 */
static inline void yyunget (int c) {
  if (c != EOF) {
    if (s_back_count >= 2) { fprintf(stderr, "yyunget: too much!\n"); abort(); }
    s_back_chars[s_back_count++] = c;
    //fprintf(stderr, "UNGET: %d\n", c);
  }
}


/* eat white space */
static int skip_spaces (int c) {
  for (;;) {
    /* skip past white space */
    while (c != EOF && isspace(c)) {
      yylval.line = incp->line;
      yylval.pos = incp->pos;
      c = yychar();
    }
    /* not a comment? swallow up comment line */
    if (c != '#') break;
    while ((c = yychar()) != EOF && c != '\n') ;
  }
  return c;
}


static int digit (int c, int base) {
  if (c == EOF) return -1;
  if (c >= 'a' && c <= 'z') c -= 32;
  if (c < '0' || (c > '9' && c < 'A') || c > 'Z') return -1;
  if ((c -= '0') > 9) c -= 7;
  if (c >= base) return -1;
  return c;
}


/* textlen includes trailing zero */
static void remove_indent (char *text, int textlen, int indent) {
  if (indent > 0) {
    while (*text) {
      char *eol = strchr(text, '\n');
      if (eol == NULL) eol = text+textlen-1;
      if (eol-text >= indent) {
        textlen -= indent;
        eol -= indent;
        memmove(text, text+indent, textlen);
      }
      if (!eol[0]) break;
      textlen -= eol+1-text;
      text = eol+1;
    }
  }
}


static inline const keyword_t *find_keyword (const char *nbuf, size_t nblen) {
  if (nblen > 0) {
    for (const keyword_t *k = keywords; k->word; ++k) if (strncmp(k->word, nbuf, nblen) == 0 && k->word[nblen] == 0) return k;
  }
  return NULL;
}


/*
 * yylex() - set yylval to current token; return its type
 */

#define PUSH_CHAR(_c)  do { \
  if (sbused+1 >= sbsize) { \
    int newsz = ((sbused+1)|0x7ff)+1; \
    char *nb = realloc(sbuf, newsz); \
    if (nb == NULL) { fprintf(stderr, "FATAL: out of memory!\n"); abort(); } \
    sbuf = nb; \
    sbsize = newsz; \
  } \
  sbuf[sbused++] = (_c); \
} while (0)

static char *sbuf = NULL;
static int sbsize = 0;
static int sbused;


/* "$(" already scanned and pushed */
/* return char after ")" */
int scan_varaccess (void) {
  int c = yychar(), qch = 0, oc;
  if (c == EOF) return c;
  /* scan variable name */
  while (c != EOF && c != '[' && c != ':') {
    PUSH_CHAR(c);
    oc = c;
    c = yychar();
    if (oc == ')') return c;
    if (oc == '$' && c == '(') { PUSH_CHAR(c); c = scan_varaccess(); }
  }
  if (c == EOF) return c;
  /* scan indexing; 'c' is not pushed */
  if (c == '[') {
    while (c != EOF && c != ']') {
      PUSH_CHAR(c);
      oc = c;
      c = yychar();
      if (oc == ')') return c;
      if (oc == '$' && c == '(') { PUSH_CHAR(c); c = scan_varaccess(); }
    }
    /* find either selector or ')' */
    while (c != EOF && c != ':') {
      PUSH_CHAR(c);
      oc = c;
      c = yychar();
      if (oc == ')') return c;
      if (oc == '$' && c == '(') { PUSH_CHAR(c); c = scan_varaccess(); }
    }
    if (c == EOF) return c;
  }
  /* scan selectors; 'c' is not pushed */
  while (c != EOF) {
    if (qch != '\'' && c == '\\') {
      /* screening */
      PUSH_CHAR(c);
      if ((c = yychar()) == EOF) break;
      PUSH_CHAR(c);
      c = yychar();
      continue;
    }
    PUSH_CHAR(c);
    oc = c;
    c = yychar();
    if (!qch && (oc == '"' || oc == '\'')) { qch = oc; continue; }
    if (!qch && oc == ')') return c;
    if (qch != '\'' && oc == '$' && c == '(') {
      PUSH_CHAR(c);
      c = scan_varaccess();
      continue;
    }
    if (qch && oc == qch) {
      if (!(qch == '\'' && c == '\'')) qch = 0;
      continue;
    }
  }
  return c;
}


int yylex (void) {
  const keyword_t *kw;
  int c;
  sbused = 0;
  yylval.strlit = 0;
  if (!incp) goto eof;
  yylval.strlit = 0; /* expand this string */
  yylval.line = incp->line;
  yylval.pos = incp->pos;
  yylval.file = incp->fname;
  /* get first character (whitespace or of token) */
  c = yychar();
  if (scan_mode == SCAN_STRING) {
    /* if scanning for a string (action's {}'s), look for the closing brace */
    /* we handle matching braces, if they match! */
    int nest = 1, indent = -1, cind, bol;
    /* skip spaces and newline */
    while (c != EOF && c != '\n' && isspace(c)) c = yychar();
    if (c == '\n') c = yychar();
    /* collect string, caclucate indent */
    cind = 0;
    bol = 1;
    while (c != EOF) {
      if (c == '{') ++nest;
      else if (c == '}' && !--nest) break;
      /* indent calculation */
      if (c == '\n') {
        cind = 0;
        bol = 1;
      } else if (bol) {
        if (isspace(c)) {
          ++cind;
        } else {
          bol = 0;
          if (indent < 0 || cind < indent) indent = cind;
        }
      }
      PUSH_CHAR(c);
      c = yychar();
    }
    /* we ate the ending brace -- regurgitate it */
    if (c != EOF) yyunget(c);
    /* check obvious errors */
    if (nest) { yyerror(&yylval, "unmatched {} in action block"); goto eof; }
    /* remove trailing newlines and spaces, add one newline */
    while (sbused > 0 && isspace(sbuf[sbused-1])) --sbused;
    PUSH_CHAR('\n');
    PUSH_CHAR(0);
    if (indent > 0) {
      //fprintf(stderr, "=== %d ===\n%s===\n", indent, sbuf);
      remove_indent(sbuf, sbused, indent);
      //fprintf(stderr, "--- %d ---\n%s---\n", indent, sbuf);
    }
    yylval.type = T_STRING;
    yylval.string = newstr(sbuf);
    yymode(SCAN_NORMAL);
  } else {
    int keyword = 0, qch = 0;
    int n;
    c = skip_spaces(c);
    /* c now contains the first character of a token */
    if (c == EOF) goto eof;
    /* special thingy: single-quoted string */
    if (c == '\'') {
      for (c = yychar(); c != EOF; c = yychar()) {
        if (c == '\'') {
          /* check for special case: "''" */
          if ((c = yychar()) != '\'') {
            if (c != EOF && !isspace(c)) yyunget(c);
            break;
          }
        }
        PUSH_CHAR(c);
      }
      PUSH_CHAR(0);
      yylval.type = T_ARG;
      yylval.strlit = 1; /* don't expand this string */
      yylval.string = newstr(sbuf);
      goto lexret;
    }
    /* 'normal' mode */
    keyword = (scan_mode == SCAN_NORMAL && isalpha(c)) || (scan_mode == SCAN_PUNCT && !isalnum(c)); /* maybe */
    //if (DEBUG_SCAN) printf("mode: %d; char: '%c'; keyword: %d\n", scan_mode, c, keyword);
    /* look for white space to delimit word */
    /* \ protects next character */
    for (; c != EOF; c = yychar()) {
      /* check if this is var access */
      if (c == '$') {
        keyword = 0;
        PUSH_CHAR(c);
        if ((c = yychar()) == EOF) break;
        if (c == '(') {
          PUSH_CHAR(c);
          c = scan_varaccess();
          yyunget(c);
          continue;
        }
        if (!qch) {
          if (isalnum(c) || c == '_' || c == '-' || c == '<' || c == '>') yywarning_ex("\"$x\" -- maybe you want \"$(x\" instead?");
        }
      }
      /* check for some common bugs */
      if (!qch && c == '(') {
        int nc = yychar();
        yyunget(nc);
        if (nc == '$') yywarning_ex("\"($\" -- maybe you want \"$(\" instead?");
        if (((sbused > 0 && !isalnum(sbuf[sbused-1])) || (sbused == 0)) &&
            (isalnum(nc) || nc == '_' || nc == '-' || nc == '<' || nc == '>')) yywarning_ex("\"(x\" -- maybe you want \"$(x\" instead?");
      }
      /* 'c' is not pushed yet */
      if (!qch && scan_mode == SCAN_PUNCT) {
        /* we are in list, the only possible keywords follows */
        if (strchr("{}[];", c) != NULL) {
          if (sbused == 0) {
            keyword = 1;
            PUSH_CHAR(c);
            c = ' ';
          }
          break;
        }
      }
      if (!qch && (isspace(c) || c == '\'')) break;
      if (!qch && scan_mode == SCAN_NORMAL && c != '"' && c != '\'' && !isalnum(c)) {
        /* check if this char (and possibly next) forms non-alnum token */
        PUSH_CHAR(c);
        if ((c = yychar()) != EOF) {
          /* try 2-char tokens */
          PUSH_CHAR(c);
          if ((kw = find_keyword(sbuf+sbused-2, 2)) != NULL) {
            if (sbused == 2) {
              /* wow! token! */
              yylval.type = kw->type;
              yylval.string = kw->word; /* used by symdump */
              goto lexret;
            }
            yywarning_ex("non-alpha token without whitespace");
            /* return this 2 chars */
            yyunget(sbuf[--sbused]);
            yyunget(sbuf[--sbused]);
            c = ' ';
            break;
          }
          /* return one char back */
          --sbused;
          yyunget(c);
        }
        /* try 1-char token */
        if (sbused > 1 && sbuf[sbused-1] == '=' && isalnum(sbuf[sbused-2])) goto skipkwone;
        if (sbused == 1 && sbuf[sbused-1] == '!') {
          int nc = yychar();
          yyunget(nc);
          if (isalnum(nc) || nc == '-' || nc == '_') goto skipkwone;
        }
        if ((kw = find_keyword(sbuf+sbused-1, 1)) != NULL) {
          if (sbused == 1) {
            /* wow! token! */
            yylval.type = kw->type;
            yylval.string = kw->word; /* used by symdump */
            goto lexret;
          }
          if (strchr("{}[];", sbuf[sbused-1]) == NULL) yywarning_ex("non-alpha token without whitespace");
          /* return this char */
          yyunget(sbuf[--sbused]);
          c = ' ';
          break;
        }
skipkwone:
        /* pop this char and process it as usual */
        c = sbuf[--sbused];
      }
      /* check for quoting */
      if (qch && c == qch) {
        qch = 0;
        continue;
      }
      if (!qch && c == '"') {
        keyword = 0;
        qch = c;
        continue;
      }
      /* screened char? */
      if (c == '\\') {
        keyword = 0;
        if ((c = yychar()) == EOF) break;
        if (qch) {
          /* in string */
          switch (c) {
            case 'a': PUSH_CHAR('\a'); break;
            case 'b': PUSH_CHAR('\b'); break;
            case 'e': PUSH_CHAR('\x1b'); break;
            case 'f': PUSH_CHAR('\f'); break;
            case 'n': PUSH_CHAR('\n'); break;
            case 'r': PUSH_CHAR('\r'); break;
            case 't': PUSH_CHAR('\t'); break;
            case 'v': PUSH_CHAR('\v'); break;
            case 'x':
              // first digit
              if ((c = yychar()) == EOF) { yyerror(&yylval, "invalid hex escape in quoted string"); goto eof; }
              if ((n = digit(c, 16)) < 0) { yyerror(&yylval, "invalid hex escape in quoted string"); goto eof; }
              // second digit
              if ((c = yychar()) != EOF) {
                int d = digit(c, 16);
                if (d < 0) yyunget(c); else n = (n*16)+d;
              }
              if (n == 0) { yyerror(&yylval, "invalid hex escape in quoted string"); goto eof; }
              PUSH_CHAR(n);
              break;
            //TODO: add '\uXXXX'?
            default:
              if (isalnum(c)) { yyerror(&yylval, "invalid escape in quoted string"); goto eof; }
              PUSH_CHAR(c);
              break;
          }
        } else {
          /* not in string */
          PUSH_CHAR(c);
        }
        continue;
      }
      /* normal char */
      if (scan_mode == SCAN_NORMAL) {
        if (keyword && !isalpha(c)) keyword = 0;
      } else if (scan_mode == SCAN_PUNCT) {
        if (keyword && isalnum(c)) keyword = 0;
      }
      PUSH_CHAR(c);
    }
    /* we looked ahead a character -- back up */
    /* don't return spaces, they will be skipped on next call anyway */
    if (c != EOF && !isspace(c)) yyunget(c);
    /* check obvious errors */
    if (qch) { yyerror(&yylval, "unmatched \" in string"); goto eof; }
    PUSH_CHAR(0);
    /*if (DEBUG_SCAN) printf("keyword: %d; str='%s' (%d)\n", keyword, sbuf, sbused);*/
    /* scan token table */
    yylval.type = T_ARG;
    if (keyword && sbused > 0) {
      /* find token */
      if ((kw = find_keyword(sbuf, sbused-1)) != NULL) {
        yylval.type = kw->type;
        yylval.string = kw->word; /* used by symdump */
      }
    }
    if (yylval.type == T_ARG) yylval.string = newstr(sbuf);
  }
lexret:
  if (DEBUG_SCAN) printf("scan %s\n", symdump(&yylval));
  return yylval.type;
eof:
  yylval.type = 0; /* 0 is EOF for lemon */
  return yylval.type;
}

#undef PUSH_CHAR


static const char *symdump (const token_t *s) {
  static char *buf = NULL;
  static int bufsz = 0;
  int nsz;
  if (s->type == EOF) return "EOF";
  nsz = strlen(s->string)+128;
  if (nsz > bufsz) {
    char *nb = realloc(buf, nsz);
    if (nb == NULL) { fprintf(stderr, "FATAL: out of memory!\n"); abort(); }
    buf = nb;
    bufsz = nsz;
  }
  switch (s->type) {
    case 0: sprintf(buf, "unknown symbol <%s>", s->string); break;
    case T_ARG: sprintf(buf, "argument <%s>", s->string); break;
    case T_STRING: sprintf(buf, "string \"%s\"", s->string); break;
    default: sprintf(buf, "keyword `%s`", s->string); break;
  }
  return buf;
}
