/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "jam.h"
#include "hash.h"
#include "newstr.h"


typedef struct recache_item_s {
  regexp_t re; /* this MUST be here! */
  struct recache_item_s *next; /* for 'same string, different flags' */
} recache_item_t;


static struct hash *recache = NULL;


/* return 0 if not found (created new ci) */
static int find_re (regexp_t **ci, const char *str, int flags) {
  int r = 1;
  recache_item_t fnd, *res = &fnd;
  if (recache == NULL) recache = hashinit(sizeof(recache_item_t), "recache");
  fnd.re.restr = newstr(str);
  if (hashenter(recache, (HASHDATA **)&res)) {
    /* want new one */
    res->re.flags = flags;
    res->re.maxmem = 0;
    res->next = NULL;
    r = 0;
    //fprintf(stderr, "NEW RE: '%s'\n", str);
  } else {
    /* hit; check if we have regexp with this set of flags */
    recache_item_t *c;
    for (c = res; c != NULL; c = c->next) if (c->re.flags == flags) break;
    if (c == NULL) {
      /* not found; create new item */
      recache_item_t *last;
      for (last = res; last->next != NULL; last = last->next) ;
      if ((c = malloc(sizeof(*c))) == NULL) { printf("FATAL: out of memory!\n"); exit(EXITBAD); }
      c->re.restr = res->re.restr;
      c->re.flags = res->re.flags;
      c->re.maxmem = 0;
      c->next = NULL;
      last->next = c;
      res = c;
      r = 0;
      //fprintf(stderr, "NEW RE(1): '%s'\n", str);
    } else {
      res = c;
      //fprintf(stderr, "RE HIT: '%s'\n", str);
    }
  }
  *ci = &res->re;
  return r;
}


/*
 * regexp options:
 *  i: ignore case
 *  u: this is utf-8 string
 *  m: '.' matches newline
 * default mode: non-utf-8 (it can be only reset with /.../u)
 */
regexp_t *regexp_compile (const char *str, int flags) {
  regexp_t *cre;
  const char *s = str, *e = NULL, *errmsg;
  flags |= RE9_FLAG_NONUTF8;
  if (str == NULL) str = "";
  if (str[0] == '/' && (e = strrchr(str+1, '/')) != NULL) {
    /* this must be regexp with options */
    for (const char *t = e+1; *t; ++t) {
      switch (*t) {
        case 'i': flags |= RE9_FLAG_CASEINSENS; break;
        case 'u': flags &= ~RE9_FLAG_NONUTF8; break;
        case 'm': flags |= RE9_FLAG_ANYDOT; break;
        default:
          printf("FATAL: invalid regexp option: '%c'!\n", *t);
          exit(EXITBAD); /* oops */
      }
    }
    ++str;
  }
  if (find_re(&cre, s, flags) == 0) {
    if ((cre->re = re9_compile_ex(str, e, flags, &errmsg)) == NULL) {
      printf("FATAL: regexp error: '%s'!\n", errmsg);
      exit(EXITBAD); /* oops */
    }
    flags &= ~RE9_FLAG_ANYDOT; /* don't need that */
    /* RE9_FLAG_CASEINSENS left only for caller checks; re9_execute() will ignore it */
    cre->flags = flags;
  }
  return cre;
}


void regexp_free (regexp_t *re) {
  /* do nothing, yeah! */
}


int regexp_execute (regexp_t *re, const char *bol, re9_sub_t *mp, int ms) {
  if (re != NULL) {
#ifdef REGEXP9_DEBUG_MEMSIZE
    int res = re9_execute(re->re, re->flags, bol, mp, ms);
    if (re9_memused > re->maxmem) re->maxmem = re9_memused;
    return res;
#else
    return re9_execute(re->re, re->flags, bol, mp, ms);
#endif
  }
  return -1;
}


void regexp_done (void) {
#ifdef REGEXP9_DEBUG_MEMSIZE
  int count = 0;
  regexp_t *rarray;
  hashiterate(recache, ({ int lmb (const void *hdata, void *udata) { ++count; return 0; } lmb; }), NULL);
  rarray = malloc(sizeof(rarray[0])*count);
  count = 0;
  hashiterate(recache, ({ int lmb (const void *hdata, void *udata) { rarray[count++] = *((regexp_t *)hdata); return 0; } lmb; }), NULL);
  printf("regexps, sorted by used memory:\n");
  qsort(rarray, count, sizeof(rarray[0]), ({
    int cmp (const void *p0, const void *p1) {
      regexp_t *r0 = (regexp_t *)p0;
      regexp_t *r1 = (regexp_t *)p1;
      if (r0->maxmem != r1->maxmem) return r1->maxmem-r0->maxmem;
      if (strlen(r0->restr) != strlen(r1->restr)) return strlen(r0->restr)-strlen(r1->restr);
      return strcmp(r0->restr, r1->restr);
    }
    cmp;
  }));
  for (int f = 0; f < count; ++f) printf("%10d: /%s/\n", rarray[f].maxmem, rarray[f].restr);
  free(rarray);
#endif
}
